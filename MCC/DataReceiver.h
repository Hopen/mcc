/************************************************************************/
/* Name     : MCC\DataReceiver.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/
#pragma once
//#include "Patterns/singleton.h"

#include <boost/algorithm/string.hpp>

#include "Timer.h"
#include "Router\router_compatibility.h"
#include "ImapMessageContext.h"


class CSystemLog;
class CMailConnectorImpl;

enum class DoAfterRead
{
	Move = 0,
	Delete = 1
};

inline DoAfterRead DoAfterReadFromString(const std::wstring& aAfterRead)
{
	auto afterRead = boost::algorithm::to_lower_copy(aAfterRead);

	if (afterRead == L"move")
	{
		return DoAfterRead::Move;
	}
	else if (afterRead == L"delete")
	{
		return DoAfterRead::Delete;
	}
	else
	{
		throw std::runtime_error("Unknown 'afterRead' param. Acceptable values are: 'move' or 'delete'");
	}
}

struct TImapParams
{
	std::wstring _email;
	std::wstring _certificate_uri;
	bool _sslEnable = false;
	std::wstring _uri;
	std::wstring _port;
	std::wstring _login;
	std::wstring _pass;
	std::wstring _fail_box_name;
	bool _peek = false;
	std::wstring _afterReadMoveTo;
	DoAfterRead _afterRead{ DoAfterRead::Move };
};


using MessageCollector = std::list<CMessage>;

template <class Impl>
class CDataReceiver
{
public:
	CDataReceiver()
		//: m_router(std::make_unique<CRouterManager>())
	{
		SubscribeOnEvents();

		CRouterManager::ConnectHandler chandler = boost::bind(&Impl::on_routerConnect, static_cast<Impl*>(this));
		//m_router->RegisterConnectHandler(chandler);
		m_router.RegisterConnectHandler(chandler);
		CRouterManager::DeliverErrorHandler ehandler = boost::bind(&Impl::on_routerDeliverError, static_cast<Impl*>(this), _1, _2, _3);
		//m_router->RegisterDeliverErrorHandler(ehandler);
		m_router.RegisterDeliverErrorHandler(ehandler);

		CRouterManager::StatusChangeHandler status_handler = boost::bind(&Impl::on_appStatusChange, static_cast<Impl*>(this), _1, _2);
		//m_router->RegisterStatusHandler(status_handler);
		m_router.RegisterStatusHandler(status_handler);
	}

	int Init(CSystemLog* pLog)
	{
		//m_log = pLog;
		return static_cast<Impl*>(this)->init_impl(pLog);
	}

	void Stop()
	{
		return static_cast<Impl*>(this)->stop();
	}

	void SubscribeOnEvents()
	{
		static_cast<Impl*>(this)->set_Subscribes();
	}

	uint32_t GetClientId()const
	{
		return static_cast<const Impl*>(this)->get_ClientId();
	}

	template <class TMessage>
	void SendToAny(TMessage&& message)const
	{
		static_cast<const Impl*>(this)->sendToAny(std::forward<TMessage>(message));
	}

	template <class TMessage>
	void SendToAll(TMessage&& message)const
	{
		static_cast<const Impl*>(this)->sendToAll(std::forward<TMessage>(message));
	}

	template <class TMessage, class TAddress>
	void SendTo(TMessage&& message, TAddress&& address)const
	{
		static_cast<const Impl*>(this)->sendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	}


protected:
	//std::unique_ptr<CRouterManager>	m_router;
	mutable CRouterManager m_router;
};

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	/*static */CMailConnectorImpl* getConnector();

	void TestReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
	{
		ReplyEventHandler(_msg, _from, _to);
	}

private:
	int init_impl(CSystemLog* pLog);

private:
	void InitTimer();

	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS& , const CLIENT_ADDRESS& );
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	void stop();

	void OnTimerExpired();

	void ReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	uint32_t get_ClientId()const
	{
		return m_router.BoundAddress();
	}

	template <class TMessage>
	void sendToAny(TMessage&& message)const
	{
		m_router.SendToAny(std::forward<TMessage>(message));
	}
	
	template <class TMessage>
	void sendToAll(TMessage&& message)const
	{
		m_router.SendToAll(std::forward<TMessage>(message));
	}

	template <class TMessage, class TAddress>
	void sendTo(TMessage&& message, TAddress&& address)const
	{
		m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	}


public:
	//handler
	void receive_onAccepted( MessagePtr message)const;
	void receive_onCompleted(const std::wstring& _email, MessagePtr message)const;
	void receive_onFailed(const std::string& message)const;

	void send_onAccepted(const CMessage& msg)const;
	void send_onCompleted(const CMessage& msg)const;
	void send_onFailed(const CMessage& msg)const;


private:
	CSystemLog * m_log;

	boost::asio::io_service& r_io;
	//std::unique_ptr<boost::asio::io_service> m_io;

	std::unique_ptr<CMyTimer> m_timer1;
	//std::unique_ptr<CRouterManager>	m_router;
	
	/*static*/ std::unique_ptr<CMailConnectorImpl> m_idapConnector;
	MessageCollector m_incomingMsgList;


};

/******************************* eof *************************************/

