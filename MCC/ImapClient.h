/************************************************************************/
/* Name     : MCC\ImapClient.h                                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/

#pragma once
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "boost/asio/ssl.hpp"
#include "ImapMessageContext.h"

#include "DataReceiver.h"

//#include "Log/SystemLog.h"

class imap_client
{
public:
	virtual void start() = 0;

	virtual MessageList getNewMessages()const = 0;

	virtual ~imap_client() {}
};

using ImapClientPtr = std::shared_ptr<imap_client>;

//template <class TSocketImpl>
//class imap_client_impl;

//template <class TSocketImpl>
//using ImapClientPtr = std::shared_ptr<imap_client_impl <TSocketImpl> >;

//class imap_SSL_client;
//class imap_NOSSL_client;
//
//
//template<typename... TArgs>
//ImapClientPtr makeSSL_ImapClient(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	TArgs&... args)
//{
//	return std::make_unique<imap_client_impl <imap_SSL_client> >(std::make_unique<imap_SSL_client>(std::forward<TArgs>(args)...), pLog, _login, _password);
//}

class CSystemLog;

//ImapClientPtr makeSSL_ImapClient(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	boost::asio::io_service& io_service,
//	boost::asio::ssl::context& context,
//	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
//	statusHandlerImap _accepted,
//	statusHandlerImap _completed,
//	statusFailedHandlerImap _failed,
//	bool peek,
//	const std::wstring& _afterReadMoveTo);

ImapClientPtr makeSSL_ImapClient(boost::asio::io_service& io_service,
	TImapParams params, 
	boost::asio::ssl::context& context,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	statusHandlerImap _accepted,
	statusHandlerImap _completed,
	statusFailedHandlerImap _failed,
	CSystemLog* pLog);
//
//ImapClientPtr makeNOSSL_ImapClient(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	boost::asio::io_service& io_service,
//	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
//	statusHandlerImap _accepted,
//	statusHandlerImap _completed,
//	statusFailedHandlerImap _failed,
//	bool peek,
//	const std::wstring& _afterReadMoveTo);

ImapClientPtr makeNOSSL_ImapClient(boost::asio::io_service& io_service,
	TImapParams params,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	statusHandlerImap _accepted,
	statusHandlerImap _completed,
	statusFailedHandlerImap _failed,
	CSystemLog* pLog);

void makeSSL_ImapClient(CSystemLog* pLog);

//template<typename... TArgs>
//ImapClientPtr<SSL_client> makeSSL_ImapClient
//(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	TArgs&...args)
//{
//	return std::make_shared<imap_client_impl <SSL_client> >(std::make_unique<SSL_client>(std::forward<TArgs>(args)...), pLog, _login, _password);
//}
//
//
//template<typename... TArgs>
//ImapClientPtr<NOSSL_client>  makeNOSSL_ImapClient
//(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	TArgs&...args)
//{
//	return std::make_shared<imap_client_impl <NOSSL_client> >(std::make_unique<NOSSL_client>(std::forward<TArgs>(args)...), pLog, _login, _password);
//}


//class CAbstract
//{
//public:
//	virtual void foo() = 0;
//};

//std::unique_ptr<CAbstract> makeSSL_ImapClient();
//std::unique_ptr<CAbstract> makeNOSSL_ImapClient();

/******************************* eof *************************************/