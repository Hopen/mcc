/************************************************************************/
/* Name     : MCC\ImapFetchBodyContentParser.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/
#include "stdafx.h"
#include "ImapFetchBodyContentParser.h"

#ifdef PARSER_ENABLE
BOOST_FUSION_ADAPT_STRUCT(
	fetchparsercontent::TFetchData,
	(int, num)
	(int, uid)
	(std::string, contentIndex)
	(int, size)
	//(std::string, source)
	)
#endif


namespace fetchparsercontent
{
	using ParserCallBack = std::function <void(const TFetchData& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE
		// rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> name/*, flags*/;
		boost::spirit::qi::rule<std::string::iterator, TFetchData(), boost::spirit::ascii::space_type> fetchBody;

		name = '['
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_(']')]
			>> ']';

		//flags = boost::spirit::lit('(')
		//	>> *(
		//		boost::spirit::qi::string("\\Answered") |
		//		boost::spirit::qi::string("\\Flagged") |
		//		boost::spirit::qi::string("\\Deleted") |
		//		boost::spirit::qi::string("\\Draft") |
		//		boost::spirit::qi::string("\\Seen") |
		//		boost::spirit::qi::string("\\Unanswered") |
		//		boost::spirit::qi::string("\\Unflagged") |
		//		boost::spirit::qi::string("\\Undeleted") |
		//		boost::spirit::qi::string("\\Undraft") |
		//		boost::spirit::qi::string("\\Unseen")
		//		)
		//	>> boost::spirit::lit(')');


		//fetchBody = '*'
		//	>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
		//	>> "FETCH (BODY"
		//	>> (name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
		//	>> '{'
		//	>> (boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])
		//	>> '}'
		//	;

		fetchBody = '*'
			>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> "FETCH ("
			>> *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
				|"BODY" > ((name[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> '{'
			>> (boost::spirit::qi::int_[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> '}'))
			;


		TFetchData output;
		
		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					fetchBody[_callback]
					),
				boost::spirit::ascii::space,
				output
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}


		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

}

bool fetchparsercontent_parse(std::string source, std::function <void(const fetchparsercontent::TFetchData& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString)
{
	bool r = fetchparsercontent::parse(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

		return r;
}

/******************************* eof *************************************/