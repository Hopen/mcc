﻿
#include "stdafx.h"

#define BOOST_APPLICATION_FEATURE_NS_SELECT_BOOST
 //#define BOOST_APPLICATION_FEATURE_NS_SELECT_STD

#include <iostream>
#include <fstream>
#include <future>
#include <boost/program_options.hpp>
#include <boost/application.hpp>

// provide setup example for windows service
#if defined(BOOST_WINDOWS_API)
#   include "setup/windows/setup/service_setup.hpp"
#endif

#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"

#include "DataReceiver.h"

namespace po = boost::program_options;
class CThreadWrapper
{
public:
	CThreadWrapper(std::thread&& _t)
		: t(std::move(_t))
	{

	}
	~CThreadWrapper()
	{
		if (t.joinable())
		{
			t.join();
		}
	}

private:
	std::thread t;
};


void S2MCC_Reply_test(CDataReceiverImpl* pReciever)
{
	CMessage msg(L"S2MCC_Reply");
	msg[L"MCCID"] = 84984517885968;
	msg[L"Customer"] = L"hopen@inbox.ru";
	msg[L"Operator"] = L"seemslikebox@mail.ru";
	msg[L"CC"] = L"aalekseev@expertsolutions.ru";
	//msg[L"CC"] = "";
	msg[L"BCC"] = L"";
	msg[L"Message"] = L"Важная информация для Вас: экономьте время на получении информации.";
	//msg[L"BCC"] = L"aalekseev@forte-it.ru";
	//msg[L"Operator"] = L"testimap@beeline.ru";
	msg[L"MessageTitle"] = L"Заголовок";
	//msg[L"MessageUrl"] = L"http://tv-ocm002/Attachments/1234/outbox/text.plain";
	//msg[L"MessageUrl"] = L"D:\\Attachments\\84984517885968\\inbox\\text.plain";
	//msg[L"AttachCount"] = 0;
	//msg[L"Attach0Name"] = L"Оппозиция_прошлое_—_современность_в_блоках,_характеризующих_структуру.png";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\Оппозиция_прошлое_—_современность_в_блоках,_характеризующих_структуру.png";
	//msg[L"Attach0Name"] = L"text.html";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\text.html";
	//msg[L"Attach0Name"] = L"1894837554.jpg";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\1894837554.jpg";
	//msg[L"Attach0Name"] = L"простой текстовый документ.txt";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\простой текстовый документ.txt";

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pReciever->TestReplyEventHandler(msg, _from, _to);

	msg[L"Customer"] = L"hopen@inbox.ru";
	msg[L"MessageTitle"] = L"Title";
	msg[L"Message"] = L"Сообщение";
	msg[L"Charset"] = L"ascii";

	pReciever->TestReplyEventHandler(msg, _from, _to);

}

class myapp
{

public:

	myapp(boost::application::context& context)
		: context_(context)
	{
	}

	void worker()
	{
		boost::asio::io_service io;

		// insert singleton_auto_pointer HERE
		singleton_auto_pointer<CSystemLog> log;

		//singleton_auto_pointer<CDataReceiverImpl> receiver;
		m_receiver = std::make_unique<CDataReceiverImpl>(io);

		if (!m_receiver->Init(log->GetInstance()))
		{
			singleton_auto_pointer<CSystemLog> log;
			log->LogString(LEVEL_INFO, L"Starting run");

			//unit test
			//S2MCC_Reply_test(m_receiver.get());

			CThreadWrapper t(std::thread(boost::bind(&boost::asio::io_service::run, &io)));

			boost::application::csbl::shared_ptr<boost::application::status> st =
				context_.find<boost::application::status>();

			int count = 0;
			while (st->state() != boost::application::status::stopped)
			{
				boost::this_thread::sleep(boost::posix_time::seconds(1));

				//if (st->state() == application::status::paused)
				//	my_log_file_ << count++ << ", paused..." << std::endl;
				//else
				//	my_log_file_ << count++ << ", running..." << std::endl;
			}

			m_receiver->Stop();
		}


	}

	// param
	int operator()()
	{
		//// launch a work thread
		//boost::application::csbl::thread thread(&myapp::worker, this);

		//context_.find<boost::application::wait_for_termination_request>()->wait();

		////auto handle = std::async(&myapp::worker);
		////handle.get();

		//OR run indirect
		worker();

		return 0;
	}

	// windows/posix

	bool stop()
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Stoping the application...");

		return true; // return true to stop, false to ignore
	}

	// windows specific (ignored on posix)

	bool pause()
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Pause the application...");
		//my_log_file_ << "Pause my application..." << std::endl;
		return true; // return true to pause, false to ignore
	}

	bool resume()
	{
		//my_log_file_ << "Resume my application..." << std::endl;
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Resume the application...");

		return true; // return true to resume, false to ignore
	}

private:

	//std::ofstream my_log_file_;

	boost::application::context& context_;

	std::unique_ptr<CDataReceiverImpl> m_receiver;

};

#include "MailConnector.h"


#include "boost/archive/iterators/base64_from_binary.hpp"
#include "boost/archive/iterators/binary_from_base64.hpp"
#include "boost/archive/iterators/transform_width.hpp"
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>
#include <sstream>
#include <string>


int main(int argc, char *c_argv[])
{
	bool bRunAsConsole = false;

#if defined(BOOST_WINDOWS_API)
#if !defined(__MINGW32__)
	// get our executable path name
	boost::filesystem::path executable_path_name = boost::application::path().location();
	boost::system::error_code ec;

	po::variables_map vm;
	po::options_description install("service options");
	install.add_options()
		("help", "produce a help message")
		("console", "run as console")
		(",i", "install service")
		(",u", "unistall service")
		("name", po::value<std::string>()->default_value(executable_path_name.stem().string()), "service name")
		("display", po::value<std::string>()->default_value(""), "service display name (optional, installation only)")
		("description", po::value<std::string>()->default_value(""), "service description (optional, installation only)")
		;


	po::store(po::parse_command_line(argc, c_argv, install), vm);

	if (vm.count("help"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;
		std::cout << install << std::endl;
		return 0;
	}

	if (vm.count("console"))
	{
		bRunAsConsole = true;
	}

	if (vm.count("-i"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;

		//std::string sName = vm["name"].as<std::string>();
		//std::wstring wName = stow(sName);
		//std::wstring sDescription = vm["description"].as<std::wstring>();

		try
		{
			boost::application::example::install_windows_service(
				boost::application::setup_arg(stow(vm["name"].as<std::string>())),
				boost::application::setup_arg(stow(vm["display"].as<std::string>())),
				boost::application::setup_arg(stow(vm["description"].as<std::string>())),
				boost::application::setup_arg(executable_path_name),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L"auto"),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L"")).install(ec);

			std::cout << ec.message() << std::endl;
		}
		catch (boost::system::system_error& ec)
		{
			std::cout << ec.what() << std::endl;
		}
		return 0;
	}

	if (vm.count("-u"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;

		boost::application::example::uninstall_windows_service(
			boost::application::setup_arg(vm["name"].as<std::string>()),
			boost::application::setup_arg(executable_path_name)).uninstall(ec);

		std::cout << ec.message() << std::endl;
		return 0;
	}
#endif
#endif


	boost::application::context app_context;
	myapp app(app_context);

	// my server aspects

	//insert path aspect
	boost::application::path path;
	path.location();
	app_context.insert<boost::application::path>(
		boost::application::csbl::make_shared<boost::application::path>(path));

	//app_context.insert<application::args_<char> >(
	//	application::csbl::make_shared<application::args_<char> >(argc, c_argv));


	// add termination handler

	boost::application::handler<>::callback termination_callback
		= boost::bind(&myapp::stop, &app);

	app_context.insert<boost::application::termination_handler>(
		boost::application::csbl::make_shared<boost::application::termination_handler_default_behaviour>(termination_callback));

	// To  "pause/resume" works, is required to add the 2 handlers.

#if defined(BOOST_WINDOWS_API)

	// windows only : add pause handler

	boost::application::handler<>::callback pause_callback
		= boost::bind(&myapp::pause, &app);

	app_context.insert<boost::application::pause_handler>(
		boost::application::csbl::make_shared<boost::application::pause_handler_default_behaviour>(pause_callback));

	// windows only : add resume handler

	boost::application::handler<>::callback resume_callback
		= boost::bind(&myapp::resume, &app);

	app_context.insert<boost::application::resume_handler>(
		boost::application::csbl::make_shared<boost::application::resume_handler_default_behaviour>(resume_callback));

#endif

	//// check if we need setup

	//if (setup(app_context))
	//{
	//	std::cout << "[I] Setup changed the current configuration." << std::endl;
	//	return 0;
	//}

	// my server instantiation

	//boost::system::error_code ec;
	int result = 0;
	if (bRunAsConsole)
		result = boost::application::launch< boost::application::common>(app, app_context, ec);
	else
		result = boost::application::launch< boost::application::server>(app, app_context, ec);
	

	if (ec)
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Process terminated with error: <%i> - %s", ec.value(), ec.message().c_str());

		//std::cout << "[E] " << ec.message()
		//	<< " <" << ec.value() << "> " << std::endl;
	}

	return result;
}

