/************************************************************************/
/* Name     : MCC\ImapSearchParser.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/

#include "stdafx.h"
#include "ImapSearchParser.h"

//#define PARSER_ENABLE

//#include <string>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"

#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
#endif

namespace searchparser
{
	struct TIMAPStatus
	{
		int _number;
		std::string _status;
		std::string _extra;
	};

}

#ifdef PARSER_ENABLE

BOOST_FUSION_ADAPT_STRUCT(
	searchparser::TIMAPStatus,
	(int, _number)
	(std::string, _status)
	(std::string, _extra)
	)

#endif

namespace searchparser
{
	using ParserCallBack = std::function <void(const int& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE
		bool r = false;
		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					"* SEARCH" >> *boost::spirit::qi::int_[_callback]
					),
				boost::spirit::ascii::space
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

	template <typename Iterator>
	bool parse2(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		boost::spirit::qi::rule<std::string::iterator, std::string(), boost::spirit::ascii::space_type> type;
		boost::spirit::qi::rule<std::string::iterator, TIMAPStatus(), boost::spirit::ascii::space_type> answer;

		type = +(boost::spirit::qi::string("OK") |
			boost::spirit::qi::string("BAD") |
			boost::spirit::qi::string("NO"));

		answer = boost::spirit::lit('a')
			>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> +(boost::spirit::qi::char_/*[boost::phoenix::at_c<2>(boost::spirit::_val) += boost::spirit::qi::_1]*/)
			;

		bool r = false;
		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					"* SEARCH" >> *boost::spirit::qi::int_[_callback]
					>> answer
					>> -('*' >> boost::spirit::qi::int_ >> "EXISTS")
					>> -('*' >> boost::spirit::qi::int_ >> "RECENT")
					),
				boost::spirit::ascii::space
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}
}

bool searchparser_parse(std::string source, std::function<void(const int&i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = searchparser::parse(
		source.begin(),
		source.end(),
		_callback,
		errString
		);
	return r;
}


bool searchparser_parse2(std::string source, std::function<void(const int&i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = searchparser::parse2(
		source.begin(),
		source.end(),
		_callback,
		errString
		);
	return r;
}


/******************************* eof *************************************/
