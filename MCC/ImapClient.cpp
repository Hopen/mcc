/************************************************************************/
/* Name     : MCC\ImapClient.cpp                                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "ImapClient.h"
#include "Log/SystemLog.h"

#include "ConnectionClasses.h"

//#include <mlang.h>

#include "ImapMessageContext.h"
//#include "ImapFetchHelpParser.h"

//const std::string FAILED_BOX_NAME = "\"FAIL\"";
//const std::string FAILED_BOX_NAME = "FAIL";


template< typename tPair >
struct second_t {
	typename tPair::second_type operator()(const tPair& p) const { return p.second; }
};


template< typename tMap >
second_t< typename tMap::value_type > second(const tMap& m) { return second_t<typename tMap::value_type>(); }

static std::string CutFuckingSubject(std::string src)
{
	std::string result = src;

	int pos1 = src.find('{');
	if (pos1 < 0)
		return result;

	int pos2 = src.find('}', pos1);

	if (pos2 < 0)
		return result;

	std::string size = src.substr(pos1 + 1, pos2 - pos1 - 1);

	int iSize = 0;

	try
	{
		iSize = toNum<int>(size);
	}
	catch (...)
	{

	}

	if (0 == iSize)
		return result;

	++pos2;

	for (int i = 0; i < 3; ++i)
	{
		if (result[pos2] == '\r' || result[pos2] == '\n')
		{
			++pos2;
		}
	}


	std::string subject = src.substr(pos2, iSize);
	std::string left = src.substr(0, pos1);
	std::string right = src.substr(pos2 + iSize, src.size() - (pos2 + iSize));
	int pos = right.find("((");
	if (pos > 0)
	{
		right = right.substr(pos, right.size() - pos);
	}

	//boost::replace_all(subject, "\n", "");
	//boost::replace_all(subject, "\r", "");
	boost::replace_all(subject, "\"", "");

	result = left + "\"" + subject + "\" " + right;

	return result;
}


template <class TSocketImpl>
class imap_client_impl : /*public std::enable_shared_from_this <imap_client_impl <TSocketImpl> >,*/ public imap_client
{
public:
	imap_client_impl(//TSocketImpl* _impl,
		std::unique_ptr<TSocketImpl>&& _impl, 
		CSystemLog* pLog,
		const std::wstring& _login,
		const std::wstring& _password,
		statusHandlerImap _accepted,
		statusHandlerImap _completed,
		statusFailedHandlerImap _failed,
		bool _peek,
		const std::wstring& _afterReadMoveTo,
		const DoAfterRead _afterRead,
		const std::wstring& _fail_box_name)
		: imap_client()
		, m_Log(pLog)
		, m_login(_login)
		, m_password(_password)
		, m_onAcceptedHandle(_accepted)
		, m_onCompletedHandle(_completed)
		, m_onFailedHandle(_failed)
		, socket_(std::move(_impl))
		, m_peek(_peek)
		, m_afterReadMoveTo(_afterReadMoveTo)
		, m_afterRead(_afterRead)
		, m_fail_box_name(_fail_box_name)
	{
	}
	~imap_client_impl() 
	{
		int test = 0;
	}

	void start() override
	{
		socket_->start(boost::bind(&imap_client_impl::verify_certificate, this /*shared_from_this()*/, _1, _2),
			boost::bind(&imap_client_impl::on_connect_ssl_done, this /*shared_from_this()*/,
				boost::asio::placeholders::error/*, boost::asio::placeholders::iterator*/),
			boost::bind(&imap_client_impl::on_connect_nossl_done, this /*shared_from_this()*/,
				boost::asio::placeholders::error, boost::asio::placeholders::iterator));
	}

private:

	bool verify_certificate(bool preverified,
		boost::asio::ssl::verify_context& ctx)
	{
		// The verify callback can be used to check whether the certificate that is
		// being presented is valid for the peer. For example, RFC 2818 describes
		// the steps involved in doing this for HTTPS. Consult the OpenSSL
		// documentation for more details. Note that the callback is called once
		// for each certificate in the certificate chain, starting from the root
		// certificate authority.

		// In this example we will simply print the certificate's subject name.
		char subject_name[256];
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

		std::wstring answer(stow(std::string(subject_name)));

		LogStringModule(LEVEL_INFO, L"IMAP", L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", answer.c_str(), preverified ? L"true" : L"false");


		return preverified;
	}

	void on_connect_ssl_done(const boost::system::error_code& error/*, boost::asio::ip::tcp::resolver::iterator iterator*/)
	{
		if (!error)
		{
			 LogStringModule(LEVEL_INFO, L"IMAP", L"Handshake done, logining...");
			// Read the response status line.
			socket_->readUntil("\r\n");

			make_login();
		}
		else
		{
			 LogStringModule(LEVEL_INFO, L"IMAP", L"Handshake failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	void on_connect_nossl_done(const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator)
	{
		if (!error)
		{
			// Read the response status line.
			socket_->readUntil("\r\n");

			make_login();
		}
		else
		{
			 LogStringModule(LEVEL_INFO, L"IMAP", L"Connect failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	void make_login()
	{
		std::string newCommand = (boost::format("a%i LOGIN %s %s\r\n") % m_iCount %wtos(m_login).c_str() % wtos(m_password).c_str()).str();
		//std::string newCommand = (boost::format("a%i NOOP\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_login, this /*shared_from_this()*/, _1)));
		//static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_noop, shared_from_this(), _1)));


		++m_iResponseQuantity;
		write(newCommand);

		socket_->read(reply_, sizeof(reply_),
			boost::bind(&imap_client_impl::on_read, this /*shared_from_this()*/,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));

	}

	void on_noop(const std::string& _reply)
	{
		int test = 0;
	}


	void write(const std::string &command)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", L"Write: \"%s\"", stow(command).c_str());

		//socket_->read(reply_, sizeof(reply_),
		//	boost::bind(&imap_client_impl::on_read, this /*shared_from_this()*/,
		//		boost::asio::placeholders::error,
		//		boost::asio::placeholders::bytes_transferred));


		socket_->write(command,
			boost::bind(&imap_client_impl::on_write, this /*shared_from_this()*/,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));
	}

	void on_read(const boost::system::error_code& error,
		size_t bytes_transferred)
	{
		if (!error)
		{
			if (bytes_transferred)
			{
				std::string in_data, status;
				in_data.append(reply_, bytes_transferred);

				LogStringModule(LEVEL_FINEST, L"IMAP", L"Reply: %s", in_data.c_str());

				std::string lastString;
				//looking for last string
				if (!readBuffer.empty())
				{
					int pos = readBuffer.rfind("\r\n");
					if (pos >= 0)
					{
						lastString = readBuffer.substr(pos + strlen("\r\n"), readBuffer.size() - pos - strlen("\r\n"));
					}
					else
					{
						lastString = readBuffer;
					}
				}
				readBuffer += in_data;
				// LogStringModule(LEVEL_FINEST, L"IMAP", L"readBuffer: %s", readBuffer.c_str());

				in_data = lastString + in_data;

				// looking for end of command
				std::istringstream response_stream(in_data);

				while (std::getline(response_stream, status))
				{

					if (status.empty() || status[0] != 'a')
						continue;

					if (status.find(" ") == std::string::npos)
						continue;

					if (status[status.size() - 1] == '\r')
						status.erase(status.size() - 1);


					auto lambda = [this, status = status](const readstatusparser::TIMAPStatus& command_status, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
					{
						THandler handler = _getHandlerByCommandIndex(command_status._number);
						--m_iResponseQuantity;

						std::size_t pos = readBuffer.find(status);
						std::string answer = readBuffer.substr(0, pos - strlen("\r\n"));
						std::size_t right = pos + status.size() + strlen("\r\n");
						if (right < readBuffer.size())
							readBuffer = readBuffer.substr(right, readBuffer.size() - right);
						else
							readBuffer.clear();

						// LogStringModule(LEVEL_FINEST, L"IMAP", L"answer: %s", answer.c_str());

						if (command_status._status == "OK")
						{
							if (handler._handler)
								handler._handler(answer);
						}
						else
						{
							 LogStringModule(LEVEL_INFO, L"IMAP", L"Execution command #[%i](%s) failed: \"%s\", status = \"%s\"",
								command_status._number,
								stow(handler._command).c_str(),
								stow(command_status._completed).c_str(),
								stow(command_status._status).c_str()
								);

							make_logout();
						}
					};

					bool r = readstatusparser_parse(
						status,
						lambda);

					if (!r)
					{
						ParsingFailed(status, std::string{});
						make_logout();
					}
				}
			}
			else
			{
				//!! error here !!
				 LogStringModule(LEVEL_INFO, L"IMAP", L"Read failed: no bytes transfered");
			}

			std::memset(reply_, 0, sizeof(reply_));

			 LogStringModule(LEVEL_INFO, L"IMAP", L"async_read_some");
			socket_->read(reply_, sizeof(reply_),
				boost::bind(&imap_client_impl::on_read, this /*shared_from_this()*/,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred));
		}
		else
		{
			 LogStringModule(LEVEL_INFO, L"IMAP", L"Read failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	void on_write(const boost::system::error_code& error,
		size_t bytes_transferred)
	{
		//nothing to do
	}

	void on_login(const std::string& _reply)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", L"Login OK");
		make_list();
		//make_select();

	}

	void make_list()
	{
		std::string newCommand = (boost::format("a%i LIST \"\" \"*\"\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_list, this /*shared_from_this()*/, _1)));

		++m_iResponseQuantity;

		write(newCommand);

	}

	void on_list(const std::string& _reply)
	{
		//bool bMailBoxExist = false;
		//parsing

		std::string errString;

		LogStringModule(LEVEL_INFO, L"on list", stow(_reply));

		bool r = listparser_parse(
			_reply,
			[this](const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
			{
				std::string _box_tmp = box_name;
				if (_box_tmp[_box_tmp.size() - 1] = '\r')
				{
					_box_tmp.erase(_box_tmp.size() - 1);
				}
				if (_box_tmp[0] = ' ')
					_box_tmp.erase(0, 1);

				if (_box_tmp == wtos(m_fail_box_name)/*FAILED_BOX_NAME*/)
				{
					//bMailBoxExist = true;
					m_relocate = true;
				}

				//int pos = box_name.find(FAILED_BOX_NAME);

				//if (box_name.find(FAILED_BOX_NAME) == 0)
				//{
				//	bMailBoxExist = true;
				//}
			},
			errString
			);

		if (!r)
		{
			ParsingFailed(_reply, errString);
		}
		//else
		//{
		//	if (bMailBoxExist)
		//	{
				LogStringModule(LEVEL_INFO, L"IMAP", L"List OK, select INBOX..., relocate: %s", m_relocate ? L"true" : L"false");
				make_select();
		//	}
		//	else
		//	{
		//		 LogStringModule(LEVEL_INFO, L"IMAP", L"List OK, create %s...", stow(FAILED_BOX_NAME).c_str());
		//		make_create();
		//	}
		//}

	}

	void make_create()
	{
		std::string newCommand = (boost::format("a%i CREATE %s\r\n") % m_iCount %wtos(m_fail_box_name)/*FAILED_BOX_NAME*/.c_str()).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_create, this /*shared_from_this()*/, _1)));

		++m_iResponseQuantity;

		write(newCommand);
	}

	void on_create(const std::string& _reply)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", L"Create OK, select INBOX...");
		make_select();
	}

	void make_select()
	{
		std::string newCommand = (boost::format("a%i SELECT INBOX\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_select, this /*shared_from_this()*/, _1)));

		++m_iResponseQuantity;

		write(newCommand);
	}

	void on_select(const std::string& _reply)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", L"Select OK, search UNSEEN messages...");
		std::string newCommand = (boost::format("a%i UID SEARCH UNSEEN\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_search, this /*shared_from_this()*/, _1)));

		++m_iResponseQuantity;

		write(newCommand);
	}

	void on_search(const std::string& _reply)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", L"Search OK, fetch messages...");
		std::string fullCommand;
		auto lambda = [this, &fullCommand](const int& i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			//std::string newCommand = (boost::format("a%i FETCH %i full\r\n") % m_iCount % i).str();
			std::string newCommand = (boost::format("a%i UID FETCH %i all\r\n") % m_iCount % i).str();
			fullCommand += newCommand;

			m_response_list[m_iCount++] = THandler(std::move(newCommand),
				static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_fetch_all, this /*shared_from_this()*/, _1)));

			++m_iResponseQuantity;
			++m_iFetchAnswersQuantity;

		};

		std::string errString;

		bool r = searchparser_parse(
			_reply,
			lambda,
			errString
			);

		if (!r)
		{
			ParsingFailed(_reply, errString);
		}

		if (fullCommand.empty())
		{
			 LogStringModule(LEVEL_INFO, L"IMAP", L"There are no new messages");
			make_logout();
		}
		else
		{
			write(fullCommand);
		}
	}

	//void on_fetch(const std::string& _reply)
	//{
	//	--m_iFetchAnswersQuantity;
	//	 LogStringModule(LEVEL_FINEST, L"IMAP", L"Fetch done, parsing...: %s,\r\nwait next: %i", _reply.c_str(), m_iFetchAnswersQuantity);
	//	auto lambda = [this](const fetchparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
	//	{
	//		 LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse OK");
	//		std::string newCommand;

	//		SectionPtr section = doc.header.body.GetSections();

	//		MessageId msgId = doc.num;

	//		std::string subject1 = doc.header.envelope.getSubject();
	//		std::wstring subject2;
	//		if (subject1.find("=?") == 0)
	//			subject2 = DecodeSubject(doc.header.envelope.getSubject());
	//		else
	//			subject2 = stow(subject1);

	//		auto utc_time = doc.header.envelope.send_date.getUtc();

	//		MessagePtr newMsg = std::make_shared<CIMAPMessage>(msgId,
	//			subject2,
	//			doc.header.receive_date.makeDateTime(utc_time),
	//			doc.header.envelope.send_date.makeDateTime(),
	//			doc.header.envelope.from.makeAddress(),
	//			doc.header.envelope.to.makeAddress(),
	//			doc.header.envelope.cc.makeAddress(),
	//			std::move(section));

	//		m_messages[msgId] = newMsg;

	//		newMsg->getSectionsToDownload([this, newMsg, msgId](const std::wstring& index)
	//		{
	//			//int test = 0;
	//			//if (test)
	//			//	return;

	//			newMsg->increaseWaitingSectionsCount();

	//			++m_iResponseQuantity;

	//			std::string newCommand = (boost::format("a%i FETCH %i %s[%s]\r\n") % m_iCount % msgId %(m_peek?"body.peek":"body") % wtos(index).c_str()).str();
	//			writeBuffer += newCommand;

	//			 LogStringModule(LEVEL_FINEST, L"IMAP", L"writeBuffer = %s", writeBuffer.c_str());

	//			m_response_list[m_iCount++] = THandler(newCommand,
	//				static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_fetch_body_content, this /*shared_from_this()*/, newMsg, _1)));

	//		});

	//		if (!newMsg->getWaitingSessionCount())
	//		{
	//			 LogStringModule(LEVEL_FINEST, L"IMAP", L"There are an empty message");
	//		}
	//	};

	//	std::string error_msg;

	//	std::string temp = _reply;
	//	boost::replace_all(temp, "\\\"", "");

	//	bool r = fetchparser::parse(
	//		temp.begin(),
	//		temp.end(),
	//		lambda,
	//		error_msg
	//		);

	//	 LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse Done");

	//	if (!r)
	//	{
	//		ParsingFailed(_reply, error_msg);

	//		if (m_relocate)
	//		{
	//			int pos = _reply.find("FETCH");
	//			if (pos > 0)
	//			{
	//				std::string errorString = _reply.substr(0, pos + sizeof("FETCH"));

	//				bool r = fetchparser::parse2(
	//					errorString.begin(),
	//					errorString.end(),
	//					[this, _reply = _reply](const int& msgNum, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
	//				{
	//					this->m_onFailedHandle(_reply);
	//					this->make_copy_and_delete(msgNum);
	//				}
	//					,
	//					error_msg
	//					);

	//			}
	//		}




	//	}

	//	if (m_iFetchAnswersQuantity == 0)
	//	{
	//		if (!writeBuffer.empty())
	//		{
	//			write(writeBuffer);

	//			writeBuffer.clear();
	//		}
	//		else
	//		{
	//			make_logout();
	//		}
	//	}

	//}

	void on_fetch_all(const std::string& _reply)
	{
		--m_iFetchAnswersQuantity;
		LogStringModule(LEVEL_FINEST, L"IMAP", L"Fetch all done, parsing...: %s,\r\nwait next: %i", _reply.c_str(), m_iFetchAnswersQuantity);
		auto lambda = [this](const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse OK");

			//SectionPtr section = doc.header.body.GetSections();

			//MessageId msgId = doc.num;
			MessageId msgId = doc.header.uid;

			std::string subject1 = doc.header.envelope.getSubject();
			std::wstring subject2;

			int pos = subject1.find("=?");
			if (pos != -1 && subject1.rfind("?=") != -1)
			{
				//hot fix for spirit
				subject1 = subject1.substr(pos, subject1.size() - pos);
				subject2 = DecodeSubject(subject1);
			}
				
			else
				subject2 = stow(subject1);

			//cutting subject when lenght more than 255
			//if (subject2.length() > 255)
			//	subject2 = subject2.substr(0, 254);

			auto utc_time = doc.header.envelope.send_date.getUtc();

			MessagePtr newMsg = std::make_shared<CIMAPMessage>(msgId,
				subject2,
				doc.header.receive_date.makeDateTime(utc_time),
				doc.header.envelope.send_date.makeDateTime(),
				doc.header.envelope.from.makeAddress(),
				doc.header.envelope.reply_to.makeAddress(),
				doc.header.envelope.to.makeAddress(),
				doc.header.envelope.cc.makeAddress()/*,
				std::move(section)*/);

			m_messages[msgId] = newMsg;

			std::string newCommand = (boost::format("a%i UID FETCH %i bodystructure\r\n") % m_iCount % msgId).str();
			writeBuffer += newCommand;

			m_response_list[m_iCount++] = THandler(std::move(newCommand),
				static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_fetch_bodystructure, this /*shared_from_this()*/, _1)));

			++m_iResponseQuantity;
			++m_iFetchBodyAnswersQuantity;
		};

		std::string error_msg;

		std::string temp = _reply;
		boost::replace_all(temp, "\\\"", "");

		//temp = "* 13 FETCH (ENVELOPE (\"Wed, 3 Aug 2016 10:16:54 +0300 (MSK)\" {158}\r\nDELIVERY FAILURE: 554 <ktskg@yadex.ru>: Relay access denied (maybe you need to check mail via POP3 first?). PROVERTE POCHTU V YASHIKE, A ZATEM OTPRAVTE PISMA! ((\"MAILER-DAEMON\" NIL \"MAILER-DAEMON%beekas-10\" \"beeline.ru\")) ((\"MAILER-DAEMON\" NIL \"MAILER-DAEMON%beekas-10\" \"beeline.ru\")) ((\"MAILER-DAEMON\" NIL \"MAILER-DAEMON%beekas-10\" \"beeline.ru\")) ((NIL NIL \"otvet\" \"beeline.ru\")) NIL NIL NIL \"<195818385.47049.1470208614808.JavaMail.SYSTEM@mailhost>\") INTERNALDATE \"03-Aug-2016 07:16:57 +0000\" RFC822.SIZE 14544 FLAGS () UID 272083)";

		std::string tmp = CutFuckingSubject(std::move(temp));

		bool r = fetchallparser_parse(
			tmp,
			lambda,
			error_msg
			);

		LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse Done");

		if (!r)
		{
			ParsingFailed(_reply, error_msg);

			if (m_relocate)
			{
				LogStringModule(LEVEL_FINEST, L"IMAP", L"Start relocation");
				int pos = _reply.find("UID");
				if (pos > 0)
				{
					std::string errorString = _reply.substr(pos, _reply.size() - sizeof("UID"));

					bool r = fetchparser_parse2(
						errorString,
						[this, _reply = _reply](const int& msgNum, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
					{
						this->m_onFailedHandle(_reply);
						this->make_copy_and_delete(msgNum, wtos(m_fail_box_name)/*FAILED_BOX_NAME*/);
					}
						,
						error_msg
						);

				}
			}




		}


		if (m_iFetchAnswersQuantity == 0)
		{
			if (!writeBuffer.empty())
			{
				write(writeBuffer);

				writeBuffer.clear();
			}
			else
			{
				make_logout();
			}
		}

	}

	void on_fetch_bodystructure(const std::string& _reply)
	{
		--m_iFetchBodyAnswersQuantity;
		LogStringModule(LEVEL_FINEST, L"IMAP", L"Fetch bodystructure done, parsing...: %s,\r\nwait next: %i", _reply.c_str(), m_iFetchAnswersQuantity);
		auto lambda = [this](const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse OK");

			SectionPtr section = doc.structure.body.GetSections();

			//MessageId msgId = doc.num;
			MessageId msgId = doc.structure.uid;

			auto newMsg = m_messages.find(msgId);
			if (newMsg != m_messages.end())
			{
				newMsg->second->setSections(std::move(section));

				newMsg->second->getSectionsToDownload([this, newMsg, msgId](const std::wstring& index)
				{
					newMsg->second->increaseWaitingSectionsCount();

					++m_iResponseQuantity;

					std::string newCommand = (boost::format("a%i UID FETCH %i %s[%s]\r\n") % m_iCount % msgId % (m_peek ? "body.peek" : "body") % wtos(index).c_str()).str();
					writeBuffer += newCommand;

					LogStringModule(LEVEL_FINEST, L"IMAP", L"writeBuffer = %s", writeBuffer.c_str());

					m_response_list[m_iCount++] = THandler(newCommand,
						static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_fetch_body_content, this /*shared_from_this()*/, newMsg->second, _1)));

				});

				if (!newMsg->second->getWaitingSessionCount())
				{
					LogStringModule(LEVEL_FINEST, L"IMAP", L"There are an empty message");
					make_seen(msgId);
					//make_deleted(msgId);
				}
			}


		};

		std::string error_msg;

		//std::string temp = "* 13 FETCH (BODYSTRUCTURE ((((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 588 10 NIL NIL NIL)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 8170 137 NIL NIL NIL) \"alternative\" (\"boundary\" \"_000_8ec6e39e4e864f758cf4308f90662f5emskex03bellmainbellinte_\") NIL NIL)(\"image\" \"gif\"  (\"name\" \"image001.gif\") \"<image001.gif@01D1C271.9BF4F4F0>\" \"image001.gif\" \"base64\" 3966 NIL (\"inline\" (\"filename\" \"image001.gif\")) NIL) \"related\" (\"boundary\" \"_005_8ec6e39e4e864f758cf4308f90662f5emskex03bellmainbellinte_\" \"type\" \"multipart/alternative\") NIL NIL)(\"text\" \"html\"  (\"name\" \"=?koi8-r?B?8c7ExcvTLmh0bWw=?=\") NIL \"=?koi8-r?B?8c7ExcvTLmh0bWw=?=\" \"base64\" 108220 1804 NIL (\"attachment\" (\"filename\" \"=?koi8-r?B?8c7ExcvTLmh0bWw=?=\")) NIL) \"mixed\" (\"boundary\" \"_006_8ec6e39e4e864f758cf4308f90662f5emskex03bellmainbellinte_\") NIL (\"ru-RU\")))";// _reply;
		std::string temp = _reply;
		boost::replace_all(temp, "\\\"", "");

		bool r = fetchbodystructureparser_parse(
			temp,
			lambda,
			error_msg
			);

		LogStringModule(LEVEL_FINEST, L"IMAP", L"Parse Done");

		if (!r)
		{
			ParsingFailed(_reply, temp);

			if (m_relocate)
			{
				LogStringModule(LEVEL_FINEST, L"IMAP", L"Start relocation");
				int pos = _reply.find("UID");
				if (pos > 0)
				{
					std::string errorString = _reply.substr(pos, _reply.size() - sizeof("UID"));

					bool r = fetchparser_parse2(
						errorString,
						[this, _reply = _reply](const int& msgNum, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
					{
						this->m_onFailedHandle(_reply);
						this->make_copy_and_delete(msgNum, wtos(m_fail_box_name)/*FAILED_BOX_NAME*/);
					}
						,
						error_msg
						);

				}

			}




		}

		if (m_iFetchBodyAnswersQuantity == 0)
		{
			if (!writeBuffer.empty())
			{
				write(writeBuffer);

				writeBuffer.clear();
			}
			else
			{
				make_logout();
			}
		}

	}


	void on_fetch_body_content(MessagePtr message, const std::string& _reply)
	{
		 LogStringModule(LEVEL_FINEST, L"IMAP", L"Fetch body content done: %s", _reply.c_str());

		std::string sSource, header;
		int pos = _reply.find("\r\n");
		if (pos >= 0)
		{
			header = _reply.substr(0, pos);
			sSource = _reply.substr(pos + 2, _reply.size() - (pos + 2));

			//if (sSource[sSource.size() - 1] == ')')
			//	sSource.erase(sSource.size() - 1);

			pos = sSource.rfind(")");

			sSource = sSource.substr(0, pos - 1);

			pos = sSource.rfind("\r\n");

			if (pos == -1)
				pos = 0;

			int pos1 = sSource.find("FLAGS", pos);
			if (pos1 >= 0)
				sSource = sSource.substr(0, pos1 - 1); //including space

			int pos2 = sSource.find("UID", pos);
			if (pos2 >= 0)
				sSource = sSource.substr(0, pos2 - 1); //including space

		}

		auto lambda = [this, sSource = sSource, message = message](const fetchparsercontent::TFetchData& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			message->setSectionSource(doc.contentIndex, sSource);
		};

		std::string errString;

		bool r = fetchparsercontent_parse(
			header,
			lambda,
			errString
			);

		if (!r)
		{
			ParsingFailed(header, errString);
		}

		message->decreaseWaitingSectionsCount();

		if (message->getWaitingSessionCount() == 0)
		{
			// nothing to download
			m_onCompletedHandle(message);
			//make_deleted(message->getIMAPMessageId());

			MoveOrDelete(message->getIMAPMessageId());
		}

		//if (m_iResponseQuantity == 0)
		//{
		//	//make_logout();
		//	make_expunge();
		//}

	}

	void make_expunge()
	{
		++m_iResponseQuantity;

		std::string newCommand = (boost::format("a%i EXPUNGE\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_expunge, this /*shared_from_this()*/, _1)));

		write(newCommand);
	}

	void on_expunge(const std::string& _reply)
	{
		make_logout();
	}


	void make_seen(int messageNum)
	{
		++m_iResponseQuantity;

		std::string newCommand = (boost::format("a%i UID STORE %i +FLAGS (\\seen)\r\n") % m_iCount % messageNum).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_make_seen, this /*shared_from_this()*/, messageNum, _1)));

		write(newCommand);

	}

	void make_deleted(int messageNum)
	{
		++m_iResponseQuantity;

		std::string newCommand = (boost::format("a%i UID STORE %i +FLAGS (\\deleted)\r\n") % m_iCount % messageNum).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_delete, this /*shared_from_this()*/, _1)));

		write(newCommand);

	}

	void on_make_seen(int messageNum, const std::string& _reply)
	{
		//nothing to do
		MoveOrDelete(messageNum);
	}

	void make_copy_and_delete(int messageNum, const std::string& _folder)
	{
		//m_onFailedHandle();

		++m_iResponseQuantity;

		std::string newCommand = (boost::format("a%i UID COPY %i %s\r\n") % m_iCount % messageNum % /*FAILED_BOX_NAME*/_folder.c_str()).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_make_copy, this /*shared_from_this()*/, messageNum, _1)));

		write(newCommand);

	}

	void on_make_copy(int messageNum, const std::string& /*_reply*/)
	{
		make_deleted(messageNum);
	}

	void on_delete(const std::string& /*_reply*/)
	{
		if (m_iResponseQuantity == 0)
		{
			make_expunge();
		}

	}

	void make_logout()
	{
		++m_iResponseQuantity;

		std::string newCommand = (boost::format("a%i LOGOUT\r\n") % m_iCount).str();
		m_response_list[m_iCount++] = THandler(newCommand,
			static_cast<boost::function<void(const std::string &)>>(boost::bind(&imap_client_impl::on_logout, this /*shared_from_this()*/, _1)));

		write(newCommand);
	}

	void on_logout(const std::string& _reply)
	{
		 LogStringModule(LEVEL_INFO, L"IMAP", stow(_reply).c_str());
	}

	void ParsingFailed(const std::string & source, const std::string& exception_what)
	{
		 LogStringModule(LEVEL_INFO, L"SPIRIT", L"Parsing failed: \"%s\"",
			stow(source).c_str()
			);

		if (!exception_what.empty())
		{
			 LogStringModule(LEVEL_INFO, L"SPIRIT", L"Exception: \"%s\"",
				stow(/*context*/exception_what).c_str()
				);
		}
	}

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring&_lpszModule, const std::wstring& formatMessage, TArgs&&... args)
	{
		m_Log->LogStringModule(level, std::wstring(m_login + L" " + _lpszModule), formatMessage, std::forward<TArgs>(args)...);
	}

	void MoveOrDelete(int messageNum)
	{
		switch (m_afterRead)
		{
		case DoAfterRead::Move:
			make_copy_and_delete(messageNum, wtos(m_afterReadMoveTo));
			break;
		case DoAfterRead::Delete:
			make_deleted(messageNum);
			break;
		default:
			make_copy_and_delete(messageNum, wtos(m_afterReadMoveTo));
			break;
		}
	}

public:

	MessageList getNewMessages()const override
	{
		MessageList list; //using RVO
		std::transform(m_messages.begin(), m_messages.end(), std::back_inserter(list), second(m_messages));

		return list;
	}


private:

	using ResponseHandler = std::function <void(const std::string&)>;

	struct THandler
	{
		std::string _command;
		ResponseHandler _handler;

		THandler() = default;

		template <class T>
		THandler(T&& command, const ResponseHandler& handler)
			: _command(std::forward<T>(command)), _handler(handler)
		{}

	};

	using ResponseHandlersCollector = std::map<int, THandler>;

	THandler _getHandlerByCommandIndex(const int& index)
	{
		imap_client_impl::THandler response;
		auto handler = m_response_list.find(index);
		if (handler != m_response_list.end())
		{
			response = handler->second;
		}
		return response;
	}
	MessagePtr _getMessageByNum(const int& num)const
	{
		auto cit = m_messages.find(num);
		if (cit != m_messages.end())
			return cit->second;

		return nullptr;
	}

private:
	//TSocketImpl* socket_;
	std::unique_ptr<TSocketImpl> socket_;
	CSystemLog* m_Log;
	//
	char reply_[131072];
	//
	std::wstring m_login;
	std::wstring m_password;
	std::wstring m_afterReadMoveTo;
	DoAfterRead m_afterRead;
	std::wstring m_fail_box_name;
	std::atomic<int> m_iCount{ 1 };
	std::atomic<int> m_iResponseQuantity{ 0 };
	std::atomic<int> m_iFetchAnswersQuantity{ 0 };
	std::atomic<int> m_iFetchBodyAnswersQuantity{ 0 };
	//
	//
	ResponseHandlersCollector m_response_list;

	MessageMap m_messages;

	std::string readBuffer;
	std::string writeBuffer;
	//
	statusHandlerImap m_onAcceptedHandle;
	statusHandlerImap m_onCompletedHandle;
	statusFailedHandlerImap m_onFailedHandle;

	bool m_peek{false};
	bool m_relocate{ false };
};

//ImapClientPtr makeSSL_ImapClient(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	boost::asio::io_service& io_service,
//	boost::asio::ssl::context& context,
//	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
//	statusHandlerImap _accepted,
//	statusHandlerImap _completed,
//	statusFailedHandlerImap _failed,
//	bool peek,
//	const std::wstring& _afterReadMoveTo)
ImapClientPtr makeSSL_ImapClient(boost::asio::io_service& io_service,
	TImapParams params,
	boost::asio::ssl::context& context,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	statusHandlerImap _accepted,
	statusHandlerImap _completed,
	statusFailedHandlerImap _failed,
	CSystemLog* pLog)
{
	return std::make_shared<imap_client_impl <SSL_client> >
		(std::make_unique<SSL_client>(io_service, context, endpoint_iterator), 
			pLog, params._login, params._pass, _accepted, _completed, _failed, params._peek, params._afterReadMoveTo, params._afterRead, params._fail_box_name);
}


//ImapClientPtr makeNOSSL_ImapClient(CSystemLog* pLog,
//	const std::wstring& _login,
//	const std::wstring& _password,
//	boost::asio::io_service& io_service,
//	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
//	statusHandlerImap _accepted,
//	statusHandlerImap _completed,
//	statusFailedHandlerImap _failed,
//	bool peek,
//	const std::wstring& _afterReadMoveTo)
ImapClientPtr makeNOSSL_ImapClient(boost::asio::io_service& io_service,
	TImapParams params,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	statusHandlerImap _accepted,
	statusHandlerImap _completed,
	statusFailedHandlerImap _failed,
	CSystemLog* pLog)
{
	return std::make_shared<imap_client_impl <NOSSL_client> >
		(std::make_unique<NOSSL_client>(io_service, endpoint_iterator), 
			pLog, params._login, params._pass, _accepted, _completed, _failed, params._peek, params._afterReadMoveTo, params._afterRead, params._fail_box_name);
}
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
//#include <boost/coroutine/asymmetric_coroutine.hpp>
//#include <boost/coroutine/symmetric_coroutine.hpp>
//
//char reply_[131072];
////char reply_[10];
//
//struct TImapParams2
//{
//	std::wstring _email = L"seemslikebox@mail.ru";
//	std::wstring _certificate_uri = L"../Obj/PEM/cacert.pem";
//	std::wstring _uri = L"imap.mail.ru";
//	std::wstring _port = L"993";
//	std::wstring _login = L"seemslikebox@mail.ru";
//	std::wstring _pass = L"febrary17";
//	bool _peek = false;
//};
//
//class MySocket
//{
//public:
//
//	using read_until_predicate = std::function<bool(const std::string&)>;
//
//	MySocket(CSystemLog* pLog,
//		const TImapParams2& params,
//		boost::asio::io_service &io_service,
//		boost::coroutines::symmetric_coroutine<void>::call_type& call,
//		boost::coroutines::symmetric_coroutine<void>::yield_type& yield)
//		: _call(&call), _yield(&yield),
//		_email(params._email), _certificate_uri(params._certificate_uri),
//		_uri(params._uri), _port(params._port), _log(pLog)
//	{
//		boost::asio::ip::tcp::resolver resolver(io_service);
//		boost::asio::ip::tcp::resolver::query query(wtos(_uri), wtos(_port));
//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//
//		boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
//		ctx.load_verify_file(wtos(_certificate_uri));
//
//		//boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket_(io_service, ctx);
//		_socket = std::make_unique<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>(io_service, ctx);
//
//		boost::system::error_code errorCode;
//
//		boost::asio::async_connect(_socket->lowest_layer(), iterator,
//			[&](const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator)
//		{
//			errorCode = error;
//
//			(*_call)();
//		}
//		);
//
//		(*_yield)();
//
//		if (errorCode)
//			throw errorCode;
//	}
//
//	template<typename... TArgs>
//	void LogStringModule(LogLevel level, const std::wstring&_lpszModule, const std::wstring& formatMessage, TArgs&&... args)
//	{
//		_log->LogStringModule(level, std::wstring(_email + L" " + _lpszModule), formatMessage, std::forward<TArgs>(args)...);
//	}
//
//	void Handshake()
//	{
//		_socket->set_verify_mode(boost::asio::ssl::verify_none);
//
//		_socket->set_verify_callback([&](bool preverified,
//			boost::asio::ssl::verify_context& ctx)
//		{
//			// The verify callback can be used to check whether the certificate that is
//			// being presented is valid for the peer. For example, RFC 2818 describes
//			// the steps involved in doing this for HTTPS. Consult the OpenSSL
//			// documentation for more details. Note that the callback is called once
//			// for each certificate in the certificate chain, starting from the root
//			// certificate authority.
//
//			// In this example we will simply print the certificate's subject name.
//			char subject_name[256];
//			X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//			X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//			std::cout << subject_name << std::endl;
//
//			LogStringModule(LEVEL_INFO, L"IMAP", L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//			return preverified;
//		});
//
//		boost::system::error_code errorCode;
//
//		_socket->async_handshake(boost::asio::ssl::stream_base::client,
//			[&](const boost::system::error_code& error/*, boost::asio::ip::tcp::resolver::iterator*/)
//		{
//			errorCode = error;
//
//			(*_call)();
//		}
//		);
//
//		(*_yield)();
//
//		if (errorCode)
//			throw errorCode;
//
//	}
//
//	std::string readUntil_if(read_until_predicate call_back)
//	{
//		std::string in_data;
//
//		do
//		{
//			in_data += readSome();
//
//		} while (!call_back(in_data));
//
//		return in_data;
//	}
//
//	//std::string read()
//	//{
//	//	std::string in_data;
//
//	//	while (true)
//	//	{
//	//		in_data += readSome();
//
//	//		int pos = in_data.find('\n');
//
//	//		if (pos >= 0)
//	//			break;
//	//	}
//
//	//	return in_data;
//	//}
//
//	std::string readSome()
//	{
//		std::string in_data;
//
//		boost::system::error_code errorCode;
//
//		_socket->async_read_some(boost::asio::buffer(reply_, sizeof(reply_)),
//			[&](const boost::system::error_code& error,
//				size_t bytes_transferred)
//		{
//			if (!error && bytes_transferred)
//			{
//
//				in_data.append(reply_, bytes_transferred);
//			}
//
//			errorCode = error;
//			(*_call)();
//		}
//		);
//
//		(*_yield)();
//
//		if (errorCode && errorCode != boost::asio::error::eof)
//			throw errorCode;
//
//		LogStringModule(LEVEL_FINEST, L"IMAP", L"Read: %s", stow(in_data).c_str());
//
//		return in_data;
//	}
//
//	template <class T>
//	void write(T&& command)
//	{
//		//std::cout << "write:" << command << std::endl;
//
//		LogStringModule(LEVEL_FINEST, L"IMAP", L"Write: %s", stow(command).c_str());
//
//		size_t size = command.size();
//
//		boost::system::error_code errorCode;
//
//		_socket->async_write_some(boost::asio::buffer(std::forward<T>(command), size),
//			[&](const boost::system::error_code& error,
//				size_t bytes_transferred)
//		{
//			errorCode = error;
//
//			(*_call)();
//		}
//		);
//
//		(*_yield)();
//
//		if (errorCode)
//			throw errorCode;
//
//	}
//
//private:
//
//	boost::coroutines::symmetric_coroutine<void>::call_type* _call{ 0 };
//	boost::coroutines::symmetric_coroutine<void>::yield_type* _yield{ 0 };
//
//	std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>> _socket;
//
//	std::wstring _email; 
//	std::wstring _certificate_uri; 
//	std::wstring _uri;
//	std::wstring _port;
//
//	CSystemLog* _log;
//};
//
//
//void NewImapClient(CSystemLog* pLog, TImapParams2 params)
//{
//	// some log functions 
//
//	auto LogStringModule = [pLog, _email = params._email](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//	{
//		pLog->LogStringModule(level, _email + L" IMAP", formatMessage, std::forward<decltype(args)>(args)...);
//	};
//
//	auto ParsingFailed = [&](const std::string & source, const std::string& exception_what)
//	{
//		LogStringModule(LEVEL_INFO, L"Parsing failed: \"%s\"",
//			stow(source).c_str()
//			);
//
//		if (!exception_what.empty())
//		{
//			LogStringModule(LEVEL_INFO, L"Exception: \"%s\"",
//				stow(/*context*/exception_what).c_str()
//				);
//		}
//	};
//
//
//	//let's go!
//
//	try
//	{
//		boost::asio::io_service io_service;
//		boost::coroutines::symmetric_coroutine<void>::call_type* pCoroMain = 0;
//
//		boost::coroutines::symmetric_coroutine<void>::call_type core_main(
//			[&](boost::coroutines::symmetric_coroutine<void>::yield_type& yield)
//		{
//			MySocket socket(pLog, params, io_service, *pCoroMain, yield);
//			socket.Handshake();
//
//			std::string socket_answer = socket.readSome();
//
//			auto read_complete_predicate = [&](const std::string& data, const std::string& finished_sequence)
//			{
//				int pos = static_cast<int>(data.rfind(finished_sequence));
//				if (pos >= 0)
//				{
//					std::string status = data.substr(pos, data.size()),
//						command_status_extra, command_status_status;
//					bool bStatus = true;
//					
//					bool r = readstatusparser_parse(
//						status,
//						[&bStatus, &command_status_extra, &command_status_status](const readstatusparser::TIMAPStatus& command_status, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//					{
//						if (command_status._status != "OK")
//						{
//							command_status_extra = command_status._extra;
//							command_status_status = command_status._status;
//							bStatus = false;
//						}
//					});
//
//					if (!bStatus)
//					{
//						LogStringModule(LEVEL_INFO, L"IMAP", L"Execution command failed: \"%s\", status = \"%s\"",
//							stow(command_status_extra).c_str(),
//							stow(command_status_status).c_str()
//							);
//					}
//
//					return r;
//				}
//				return false;
//			};
//
//
//			int iCount = 0;
//			
//			//logining
//
//			socket.write((boost::format("a%i LOGIN %s %s\r\n") % ++iCount %wtos(params._login).c_str() % wtos(params._pass).c_str()).str());
//			socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//			// get folders
//			auto can_relocate = false;
//			socket.write((boost::format("a%i LIST \"\" \"*\" \r\n") % ++iCount).str());
//			socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//			std::string errString;
//
//			bool r = listparser_parse2(
//				socket_answer,
//				[&](const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//			{
//				std::string _box_tmp = box_name;
//				if (_box_tmp[_box_tmp.size() - 1] = '\r')
//				{
//					_box_tmp.erase(_box_tmp.size() - 1);
//				}
//				if (_box_tmp[0] = ' ')
//					_box_tmp.erase(0, 1);
//
//				if (_box_tmp == FAILED_BOX_NAME)
//				{
//					can_relocate = true;
//				}
//
//			},
//				errString
//				);
//
//			if (!r)
//			{
//				ParsingFailed(socket_answer, errString);
//			}
//
//			//select inbox and get list of unseen messages
//			using MessageNumCollector = std::list <int>;
//
//			MessageNumCollector  messageList;
//			if (r)
//			{
//				socket.write((boost::format("a%i SELECT INBOX\r\n") % ++iCount).str());
//				socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//				socket.write((boost::format("a%i SEARCH UNSEEN\r\n") % ++iCount).str());
//				socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//				r = searchparser_parse2(
//					socket_answer,
//					[&messageList](const int& i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//					{
//						messageList.emplace_back(i);
//					},
//					errString
//					);
//
//				if (!r)
//				{
//					ParsingFailed(socket_answer, errString);
//				}
//			}
//
//			//get unseen messages
//			MessageMap messages;
//
//			if (!messageList.empty())
//			{
//				for each(auto& num in messageList)
//				{
//					// get header
//					/*socket.write((boost::format("a%i FETCH %i all\r\n") % ++iCount %num).str());
//					socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//					boost::replace_all(socket_answer, "\\\"", "");
//
//					auto lambda1 = [&messages](const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//					{
//						MessageId msgId = doc.num;
//
//						std::string subject1 = doc.header.envelope.getSubject();
//						std::wstring subject2;
//						if (subject1.find("=?") == 0)
//							subject2 = DecodeSubject(doc.header.envelope.getSubject());
//						else
//							subject2 = stow(subject1);
//
//						auto utc_time = doc.header.envelope.send_date.getUtc();
//
//						MessagePtr newMsg = std::make_shared<CIMAPMessage>(msgId,
//							subject2,
//							doc.header.receive_date.makeDateTime(utc_time),
//							doc.header.envelope.send_date.makeDateTime(),
//							doc.header.envelope.from.makeAddress(),
//							doc.header.envelope.to.makeAddress(),
//							doc.header.envelope.cc.makeAddress());
//
//						messages[msgId] = newMsg;
//					};
//
//
//					bool r = fetchallparser_parse2(
//						socket_answer,
//						lambda1,
//						errString
//						);
//
//
//					if (!r)
//					{
//						ParsingFailed(socket_answer, errString);
//						continue;
//					}*/
//
//					// get bodystructure
//					socket.write((boost::format("a%i FETCH %i bodystructure\r\n") % ++iCount %num).str());
//					socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//					boost::replace_all(socket_answer, "\\\"", "");
//
//					auto lambda2 = [/*&messages*/](const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//					{
//						//SectionPtr section = doc.structure.body.GetSections();
//
//						//auto newMsg = messages.find(doc.num);
//						//if (newMsg != messages.end())
//						//{
//						//	newMsg->second->setSections(std::move(section));
//						//}
//					};
//
//					r = fetchbodystructureparser_parse2(
//						socket_answer,
//						lambda2,
//						errString
//						);
//
//					if (!r)
//					{
//						//ParsingFailed(socket_answer, errString);
//					}
//				}
//			}
//
//
//			//for each (auto message in messages)
//			//{
//
//			//	std::list<std::wstring> sections2download;
//			//	message.second->getSectionsToDownload([&sections2download](const std::wstring& index)
//			//	{
//			//		sections2download.push_back(index);
//			//	});
//
//			//	for each (auto section in sections2download)
//			//	{
//			//		//socket.write((boost::format("a%i FETCH %i %s[%s]\r\n") % ++iCount).str());
//			//		socket.write((boost::format("a%i FETCH %i %s[%s]\r\n") % ++iCount % message.first % (params._peek ? "body.peek" : "body") % wtos(section).c_str()).str());
//			//		socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//			//		std::string header, sSource;
//			//		int pos = socket_answer.find("\r\n");
//			//		if (pos >= 0)
//			//		{
//			//			header = socket_answer.substr(0, pos);
//			//			sSource = socket_answer.substr(pos + 2, socket_answer.size() - (pos + 2));
//
//			//			pos = sSource.rfind((boost::format("\r\na%i") % (iCount)).str());
//			//			if (pos)
//			//			{
//			//				sSource.erase(pos);
//			//			}
//
//			//			if (sSource[sSource.size() - 1] == ')')
//			//				sSource.erase(sSource.size() - 1);
//
//			//			pos = sSource.rfind("\r\n");
//
//			//			if (pos == -1)
//			//				pos = 0;
//
//			//			pos = sSource.find("FLAGS", pos);
//			//			if (pos >= 0)
//			//				sSource = sSource.substr(0, pos - 1); //including space
//			//		}
//
//
//			//		auto lambda = [&message,sSource](const fetchparsercontent::TFetchData& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
//			//		{
//			//			message.second->setSectionSource(doc.contentIndex, sSource);
//			//		};
//
//
//			//		bool r = fetchparsercontent_parse(
//			//			header,
//			//			lambda,
//			//			errString
//			//			);
//
//			//		if (!r)
//			//		{
//			//			ParsingFailed(socket_answer, errString);
//			//		}
//			//	}
//
//			//	//m_onCompletedHandle(message);
//			//}
//
//
//
//			socket.write((boost::format("a%i LOGOUT\r\n") % ++iCount).str());
//			socket_answer = socket.readUntil_if(std::bind(read_complete_predicate, std::placeholders::_1, (boost::format("\r\na%i") % (iCount)).str()));
//
//			int test = 0;
//		}
//		);
//
//		pCoroMain = &core_main;
//
//		io_service.post([&]()
//		{
//			core_main();
//		});
//
//		io_service.run();
//
//		int test = 0;
//	}
//	catch (boost::system::error_code& error)
//	{
//		//std::cout << error.message() << std::endl;
//		LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//	}
//	catch (...)
//	{
//		LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//	}
//
//	int test = 0;
//}

void makeSSL_ImapClient(CSystemLog * pLog)
{
	//NewImapClient(pLog, TImapParams2());
}


/******************************* eof *************************************/