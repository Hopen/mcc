/************************************************************************/
/* Name     : CDRLog\Timer.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Feb 2015                                               */
/************************************************************************/
#pragma once

#include <boost/thread.hpp>
#include <boost/asio.hpp>

//class CTimer
//{
//public:
//	using IOPtr = std::unique_ptr<boost::asio::io_service>;
//	using SimpleCallbackFunc = std::function <void(void)>;// void(*)();
//
//	CTimer(const IOPtr& _io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, int _checkTime);
//	~CTimer();
//	void CancelTimer();
//private:
//	void Loop();
//
//private:
//	boost::asio::strand m_strand;
//	SimpleCallbackFunc m_callBackFunc;
//	boost::posix_time::minutes m_checkTime;
//	boost::asio::deadline_timer m_timer;
//};


class CMyTimer
{
	const int FIRST_CHECK_TIME = 3;

public:
	using IOPtr = std::unique_ptr<boost::asio::io_service>;
	using SimpleCallbackFunc = std::function <void(void)>;

	CMyTimer(boost::asio::io_service& _io, SimpleCallbackFunc _callback, int _checkTime)
		: m_strand(_io),
		m_checkTime(_checkTime),
		m_timer(_io, boost::posix_time::seconds(CMyTimer::FIRST_CHECK_TIME)),
		m_callBackFunc(_callback)
	{
		m_timer.async_wait(m_strand.wrap(boost::bind(&CMyTimer::Loop, this)));
	}
	CMyTimer(const CMyTimer&) = delete;
	//~CMyTimer() {};
	void CancelTimer();
private:
	void Loop();

private:
	boost::asio::strand m_strand;
	SimpleCallbackFunc m_callBackFunc;
	//boost::posix_time::minutes m_checkTime;
	boost::posix_time::seconds m_checkTime;
	boost::asio::deadline_timer m_timer;

	bool m_bTimerStoped{ false };
};
/******************************* eof *************************************/