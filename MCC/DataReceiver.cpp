/************************************************************************/
/* Name     : MCC\DataReceiver.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <future>
#include <type_traits>
#include "DataReceiver.h"
#include "Configuration\SystemConfiguration.h"
#include "Log\SystemLog.h"
//#include "Router\router_connection.h"
#include "MailConnector.h"
#include "api/ProcessHelper.h"
//


const int EXPIREDTIME = 30;

//std::unique_ptr<CMailConnectorImpl> CDataReceiverImpl::m_idapConnector;

//static void OnTimer1Expired()
//{
//	//singleton_auto_pointer<CMailConnectorImpl> connector;
//	//connector->ConnectTo(L"imap.mail.ru", L"993");
//
//	if (CMailConnectorImpl* connector = CDataReceiverImpl::getConnector())
//	{
//		connector->GetMail(L"imap.mail.ru", L"993", L"seemslikebox@mail.ru", L"febrary17");
//	}
//}
//template <class F, class... Ts>
//using function_type = std::result_of_t<F(Ts...)>;
//
//template <class F, class... Ts>
//using tasks = std::vector <function_type<F, Ts>>;


template <class F, class... Ts>
auto asyncFunc(F&& f, Ts&&...params)
{
	return std::async(std::launch::async, std::forward<F>(f), std::forward<Ts>(params)...);
}

void CDataReceiverImpl::OnTimerExpired()
{
	if (CMailConnectorImpl* connector = CDataReceiverImpl::getConnector())
	{
		SystemConfig settings;
		TImapParams params;

		//std::wstring sImapServer, sImapPort;
		//bool bSSLEnable = false;

		try
		{
			params._uri = settings->get_config_value<std::wstring>(_T("IMAPServerName"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"IMAPServerName\" config key");
		}

		try
		{
			params._port = settings->get_config_value<std::wstring>(_T("IMAPPort"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"IMAPPort\" config key");
		}

		try
		{
			params._sslEnable = settings->safe_get_config_bool(L"SSLEnable", false);
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SSLEnable\" config key");
		}

		params._fail_box_name = settings->safe_get_config_value<std::wstring>(_T("FailBoxName"), L"FAIL");

		std::wstring pem_location = extra_api::CProcessHelper::GetCurrentFolderName();

		if (!params._uri.empty() && !params._port.empty())
		{
			std::vector <std::future <bool>> tasks;

			//auto imap_node = settings->get_config_node(std::wstring(L"configuration.IMAP"));
			auto imap_node = settings->get_config_node(std::wstring(L"IMAP"));

			for (const auto& child : imap_node)
			{
				if (child.first == L"<xmlcomment>")
					continue;

				try
				{
					std::wstring sertificate = params._sslEnable ? settings->get_config_attr<std::wstring>(child.second, L"sslcertificate"):L"";
					if (!sertificate.empty())
						sertificate = pem_location + sertificate;

					params._certificate_uri = sertificate;
					params._email = settings->get_config_attr<std::wstring>(child.second, L"name");
					params._login = settings->get_config_attr<std::wstring>(child.second, L"login");
					params._pass = settings->get_config_attr<std::wstring>(child.second, L"password");
					params._peek = settings->get_config_bool(child.second, L"peek");
					params._afterReadMoveTo = settings->safe_get_config_attr<std::wstring>(child.second, L"afterReadMoveTo", L"");

					std::wstring afterRead = settings->safe_get_config_attr<std::wstring>(child.second, L"afterRead", L"move");
					params._afterRead = DoAfterReadFromString(afterRead);

					if (params._afterRead == DoAfterRead::Move && params._afterReadMoveTo.empty())
					{
						throw std::runtime_error("'afterReadMoveTo' param is mandatory when 'afterRead' == move");
					}

					tasks.emplace_back(std::async(std::launch::async, boost::bind(&CMailConnectorImpl::GetMail, connector, 
						this,
						/*settings->get_config_attr(child.second, L"name"),
						sertificate,
						sImapServer,
						sImapPort,
						settings->get_config_attr(child.second, L"login"),
						settings->get_config_attr(child.second, L"password"),
						settings->get_config_bool(child.second, L"peek")*/
						params)));

				}
				catch (const core::configuration_error& error)
				{
					m_log->LogString(LEVEL_WARNING, L"Exception reading config key: %s", stow(error.what()));
				}


			}
		}

		//if (!sImapServer.empty() && !sImapPort.empty() && !sLogin.empty()&& !sPassword.empty() && (bSSLEnable && !sCertificate.empty() || !bSSLEnable))
		//{
		//	connector->GetMail(this, sMail, sCertificate, sImapServer, sImapPort, sLogin, sPassword, bSSLEnable);
		//}

		if (m_incomingMsgList.empty())
			return;

		std::wstring sSmtpServer, sSmtpPort, sLogin, sPassword, sCertificate, sSmtpClientAddress;

		try
		{
			sSmtpServer = settings->get_config_value<std::wstring>(_T("SMTPServerName"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SMTPServerName\" config key");
		}

		try
		{
			sSmtpPort = settings->get_config_value<std::wstring>(_T("SMTPPort"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SMTPPort\" config key");
		}

		try
		{
			sLogin = settings->get_config_value<std::wstring>(_T("SMTPLogin"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SMTPLogin\" config key");
		}

		try
		{
			sPassword = settings->get_config_value<std::wstring>(_T("SMTPPassword"));
		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SMTPPassword\" config key");
		}

		try
		{
			sCertificate = settings->get_config_value<std::wstring>(_T("SMTPSSLSertificate"));
			std::wstring pem_location = extra_api::CProcessHelper::GetCurrentFolderName();//fpath.parent_path().c_str();
			sCertificate = pem_location + sCertificate;

		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SMTPSSLSertificate\" config key");
		}

		try
		{
			sSmtpClientAddress = settings->get_config_value<std::wstring>(_T("SmtpClientName"));

		}
		catch (...)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception reading \"SmtpClientName\" config key");
		}

		if (!sSmtpServer.empty() && !sSmtpPort.empty() && !sLogin.empty() && !sPassword.empty() && (params._sslEnable && !sCertificate.empty() || !params._sslEnable))
		{
			connector->SendMail(this, m_incomingMsgList, sSmtpClientAddress, sCertificate, sSmtpServer, sSmtpPort, sLogin, sPassword, params._sslEnable);
		}
	}
}

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) :
	//m_router(std::make_unique<CRouterManager>()),
	m_log(NULL), 
	//m_io(std::make_unique<boost::asio::io_service>())
	r_io(io)//,
	//m_timer1(std::make_unique<CMyTimer>(io, /*&CDataReceiverImpl::*/OnTimer1Expired, 30))
{
}


CDataReceiverImpl::~CDataReceiverImpl()
{

}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	InitTimer();

	m_idapConnector = std::make_unique<CMailConnectorImpl>(r_io);

	//m_log->LogString(LEVEL_INFO, L"Subscribe on events...");
	//SubscribeOnEvents();

	//m_log->LogString(LEVEL_INFO, L"Connecting to router...");
	//CRouterManager::ConnectHandler chandler = boost::bind(&CDataReceiverImpl::RouterConnectHandler, this);
	//m_router->RegisterConnectHandler(chandler);
	//CRouterManager::DeliverErrorHandler ehandler = boost::bind(&CDataReceiverImpl::RouterDeliverErrorHandler, this, _1, _2, _3);
	//m_router->RegisterDeliverErrorHandler(ehandler);

	//CRouterManager::StatusChangeHandler status_handler = boost::bind(&CDataReceiverImpl::AppStatusChange, this, _1, _2);
	//m_router->RegisterStatusHandler(status_handler);


	return 0;
}

CMailConnectorImpl * CDataReceiverImpl::getConnector()
{
	if (m_idapConnector.get())
		return m_idapConnector.get();
	return nullptr;
}


void CDataReceiverImpl::InitTimer(/*const unsigned int& msec*/)
{
	SystemConfig settings;

	int iFileRecorderTimer = EXPIREDTIME;
	try
	{
		iFileRecorderTimer = settings->safe_get_config_int(_T("ExpiredTime"), EXPIREDTIME);
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"ExpiredTime\" config key");
	}

	//m_timer1 = std::make_unique<CMyTimer>(r_io, OnTimer1Expired, iFileRecorderTimer);
	m_timer1 = std::make_unique<CMyTimer>(r_io, boost::bind(&CDataReceiverImpl::OnTimerExpired, this), iFileRecorderTimer);
	//boost::thread t(boost::bind(&boost::asio::io_service::run, m_io));
}

void CDataReceiverImpl::set_Subscribes()
{
	//SUBSCRIBE_MESSAGE(m_router, L"S2MCC_Reply", CDataReceiverImpl::ReplyEventHandler);

}

void CDataReceiverImpl::stop()
{
	if (m_timer1)
		m_timer1->CancelTimer();
}

void CDataReceiverImpl::on_routerConnect()
{
	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& msg, const CLIENT_ADDRESS &, const CLIENT_ADDRESS &)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == /*static_cast<int>*/(core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}


//void CDataReceiverImpl::ReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
//{
//	SystemConfig settings;
//	std::vector<std::wstring> listOfMails;
//
//	try
//	{
//		auto imap_node = settings->get_config_node(std::wstring(L"configuration.IMAP"));
//		for (const auto& child : imap_node)
//		{
//			if (child.first == L"<xmlcomment>")
//				continue;
//			listOfMails.emplace_back(settings->get_config_attr(child.second, L"name"));
//		}
//	}
//	catch (...)
//	{
//		m_log->LogString(LEVEL_WARNING, L"Exception reading \"configuration.IMAP\" config key");
//	}
//
//	std::wstring sTo = _msg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr();
//	if (!listOfMails.empty() && !sTo.empty())
//	{
//		if (std::find(listOfMails.begin(), listOfMails.end(), sTo) == listOfMails.end())
//			return;
//	}
//
//
//	m_log->LogStringModule(LEVEL_INFO, L"S2MCC_Reply", L"Incoming message: %s", _msg.Dump().c_str());
//
//	__int64 iMCCID = 0;
//	try
//	{
//		iMCCID = _msg.SafeReadParam(L"MCCID", core::checked_type::Int64, 0).AsInt64();
//		if (iMCCID == 0)
//		{
//			m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Incoming message has no MCCID and has skipped");
//			return;
//		}
//	}
//	catch (core::message_parameter_invalid_type_exception& error)
//	{
//		m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Exception when try to read \"MCCID\":", stow(error.what()));
//		return;
//	}
//	 
//	 std::wstring sMessageFolderMCCID = toStr<wchar_t>(iMCCID);
//
//	 //std::wstring sMessageFolderMCCID = (boost::wformat(L"%I64d") % iMCCID).str();
//
//	 if (1)
//	 {
//		 std::wstring sSubject = _msg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr();
//		 if (!sSubject.empty())
//		 {
//			 _msg[L"MessageTitle"] = stow(boost::to_utf8(sSubject));
//		 }
//	 }
//
//
//	
//	
//
//
//	std::wstring sAttachmentFolder;
//	try
//	{
//		sAttachmentFolder = settings->get_config_value(_T("AttachmentFolder"));
//	}
//	catch (...)
//	{
//		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentFolder\" config key");
//		sAttachmentFolder = L"c:\\Attachments\\inbox\\";
//	}
//
//	//std::wstring sImapMessageType;
//	//try
//	//{
//	//	sImapMessageType = settings->get_config_value(_T("PreferedMessageType"));
//	//}
//	//catch (...)
//	//{
//	//	sImapMessageType = L"text.plain";
//	//	m_log->LogString(LEVEL_WARNING, L"Exception reading \"PreferedMessageType\" config key");
//	//}
//
//	//_msg[L"ImapMessageType"] = sImapMessageType;
//
//
//	struct TAttachmentsParam
//	{
//		std::wstring Url;
//		std::wstring SourceName;
//		bool dDecode{ false };
//
//		TAttachmentsParam(const std::wstring& _url, const std::wstring& _sourceName, bool _decode = false)
//			:Url(_url), SourceName(_sourceName), dDecode(_decode)
//		{}
//	};
//
//	std::vector <TAttachmentsParam> attachments;
//
//	auto attachCount = _msg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();
//	if (attachCount == 0)
//	{
//		m_log->LogString(LEVEL_FINEST, L"No attachments");
//	}
//	else
//	{
//		
//		for (int i = 0; i < attachCount; ++i)
//		{
//			auto attach = _msg.SafeReadParam(L"Attach" + toStr<wchar_t>(i) + L"Url", core::checked_type::String, L"");
//			if (!attach.IsEmpty() && attach.AsWideStr().length())
//			{
//				attachments.emplace_back(attach.AsWideStr(), std::wstring(L"Attach") + toStr<wchar_t>(i) + L"Source");
//			}
//
//			std::wstring attachName = L"Attach" + toStr<wchar_t>(i) + L"Name";
//
//			auto attachNameSource = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
//			_msg[attachName] = L"=?UTF-8?B?" + stow(encodeBase64(boost::to_utf8(attachNameSource))) + L"?=";
//
//			auto attachNameSourceSafe = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
//			int test = 0;
//		}
//	}
//
//	if (1)
//	{
//		auto mainMassage = _msg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr();
//
//		if (!mainMassage.empty())
//		{
//			std::string sSource = boost::to_utf8(mainMassage);
//			_msg[L"Message"] = /*encodeBase64*/(sSource);
//		}
//		else
//		{
//			auto mainMassage = _msg.SafeReadParam(L"MessageUrl", core::checked_type::String, L"").AsWideStr();
//
//			m_log->LogString(LEVEL_FINEST, L"MessageUrl: %s", mainMassage.c_str());
//
//			if (!mainMassage.empty())
//			{
//				attachments.emplace_back(mainMassage, L"Message", true);
//			}
//		}
//
//	}
//
//
//	for each (auto attachmentParam in attachments)
//	{
//		std::wstring attachPath = attachmentParam.Url;
//		std::wstring attachSourceName = attachmentParam.SourceName;
//
//		m_log->LogString(LEVEL_FINEST, L"attachPath: %s", attachPath.c_str());
//		m_log->LogString(LEVEL_FINEST, L"attachSourceName: %s", attachSourceName.c_str());
//		m_log->LogString(LEVEL_FINEST, L"sMessageFolderMCCID: %s", sMessageFolderMCCID.c_str());
//
//		std::wstring localPath;
//		int pos = attachPath.find(sMessageFolderMCCID);
//		if (pos > 0)
//		{
//			localPath = attachPath.substr(pos, attachPath.length());
//		}
//
//		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());
//
//		if (localPath.empty())
//		{
//			break;
//		}
//
//		if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
//		{
//			localPath = sAttachmentFolder + localPath;
//		}
//		else
//		{
//			localPath = sAttachmentFolder + L"\\" + localPath;
//		}
//
//		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());
//
//		std::string sSource;
//
//		std::ifstream _in(localPath.c_str(), std::ios::binary);
//		// read whole file
//		DWORD   dwDataSize = 0;
//		if (_in.seekg(0, std::ios::end))
//		{
//			dwDataSize = static_cast<DWORD>(_in.tellg());
//		}
//		std::string buff(dwDataSize, 0);
//		if (dwDataSize && _in.seekg(0, std::ios::beg))
//		{
//			std::copy(std::istreambuf_iterator< char>(_in),
//				std::istreambuf_iterator< char >(),
//				buff.begin());
//
//			sSource = buff;
//		}
//		else
//		{
//			m_log->LogString(LEVEL_WARNING, L"Attachment file: \"%s\" is empty", localPath.c_str());
//		}
//		_in.close();
//
//		m_log->LogString(LEVEL_FINEST, L"Source: %s", stow(sSource).c_str());
//
//		//std::wstring attachname = std::wstring(L"Attach") + toStr<wchar_t>(count) + L"Source";
//		if (!sSource.empty())
//		{
//			if (attachmentParam.dDecode)
//			{
//				sSource = boost::to_utf8(stow(sSource));
//				_msg[attachSourceName] = sSource;
//			}
//			else
//			{
//				_msg[attachSourceName] = encodeBase64(sSource);
//			}
//
//			//std::wstring sSourceNew = _msg.SafeReadParam(attachSourceName, core::checked_type::String, L"").AsWideStr();
//
//			//if (attachmentParam.dDecode)
//			//{
//			//	sSource = boost::to_utf8(stow(sSource));
//			//}
//
//			//_msg[attachSourceName] = encodeBase64(sSource);
//
//		}
//		else
//		{
//			m_log->LogString(LEVEL_FINEST, L"Source empty");
//			_msg[attachSourceName] = "";
//		}
//
//	}
//
//	//m_log->LogStringModule(LEVEL_INFO, L"S2MCC_Reply", L"Incoming message: %s", _msg.Dump().c_str());
//
//	m_incomingMsgList.push_back(_msg);
//}

void CDataReceiverImpl::ReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	SystemConfig settings;
	std::vector<std::wstring> listOfMails;

	try
	{
		//auto imap_node = settings->get_config_node(std::wstring(L"configuration.IMAP"));
		auto imap_node = settings->get_config_node(std::wstring(L"IMAP"));
		for (const auto& child : imap_node)
		{
			if (child.first == L"<xmlcomment>")
				continue;
			listOfMails.emplace_back(settings->get_config_attr<std::wstring>(child.second, L"name"));
		}
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"configuration.IMAP\" config key");
	}

	try
	{
		std::wstring sDoNoReplyAddress = settings->get_config_value<std::wstring>(_T("DoNotReplayAddress"));
		std::vector <std::wstring> toList;
		boost::split(toList, sDoNoReplyAddress, std::bind2nd(std::equal_to<wchar_t>(), ';'));
		for each (auto address in toList)
		{
			if (!address.empty())
			{
				listOfMails.push_back(address);
			}
		}
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"DoNotReplayAddress\" config key");
	}


	std::wstring sTo = _msg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr();
	if (!listOfMails.empty() && !sTo.empty())
	{
		if (std::find(listOfMails.begin(), listOfMails.end(), sTo) == listOfMails.end())
			return;
	}


	m_log->LogStringModule(LEVEL_INFO, L"S2MCC_Reply", L"Incoming message: %s", _msg.Dump().c_str());

	if (1)
	{
		std::wstring sSubject = _msg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr();
		if (!sSubject.empty())
		{
			_msg[L"MessageTitle"] = stow(boost::to_utf8(sSubject));
		}
	}

	__int64 iMCCID = 0;
	try
	{
		iMCCID = _msg.SafeReadParam(L"MCCID", core::checked_type::Int64, 0).AsInt64();
		if (iMCCID == 0)
		{
			m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Incoming message has no MCCID and has skipped");
			return;
		}
	}
	catch (core::message_parameter_invalid_type_exception& error)
	{
		m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Exception when try to read \"MCCID\":", stow(error.what()));
		return;
	}

	std::wstring sMessageFolderMCCID = toStr<wchar_t>(iMCCID);


	std::wstring sAttachmentFolder;
	try
	{
		sAttachmentFolder = settings->get_config_value<std::wstring>(_T("AttachmentFolder"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentFolder\" config key");
		sAttachmentFolder = L"c:\\Attachments\\outbox\\";
	}


	struct TAttachmentsParam
	{
		std::wstring Url;
		std::wstring SourceName;
		bool dDecode{ false };

		TAttachmentsParam(const std::wstring& _url, const std::wstring& _sourceName, bool _decode = false)
			:Url(_url), SourceName(_sourceName), dDecode(_decode)
		{}
	};

	std::vector <TAttachmentsParam> attachments;

	auto attachCount = _msg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();
	if (attachCount == 0)
	{
		m_log->LogString(LEVEL_FINEST, L"No attachments");
	}
	else
	{

		for (int i = 0; i < attachCount; ++i)
		{
			auto attach = _msg.SafeReadParam(L"Attach" + toStr<wchar_t>(i) + L"Url", core::checked_type::String, L"");
			if (!attach.IsEmpty() && attach.AsWideStr().length())
			{
				attachments.emplace_back(attach.AsWideStr(), std::wstring(L"Attach") + toStr<wchar_t>(i) + L"Source");
			}

			std::wstring attachName = L"Attach" + toStr<wchar_t>(i) + L"Name";

			auto attachNameSource = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
			_msg[attachName] = L"=?UTF-8?B?" + stow(encodeBase64(boost::to_utf8(attachNameSource))) + L"?=";

			//auto attachNameSourceSafe = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
			//int test = 0;
		}
	}

	if (1)
	{
		auto mainMassage = _msg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr();

		if (!mainMassage.empty())
		{
			std::string sSource = boost::to_utf8(mainMassage);
			_msg[L"Message"] = /*encodeBase64*/stow(sSource);
		}
		else
		{
			auto mainMassage = _msg.SafeReadParam(L"MessageUrl", core::checked_type::String, L"").AsWideStr();

			m_log->LogString(LEVEL_FINEST, L"MessageUrl: %s", mainMassage.c_str());

			if (!mainMassage.empty())
			{
				attachments.emplace_back(mainMassage, L"Message", true);
			}
		}

	}


	for each (auto attachmentParam in attachments)
	{
		std::wstring attachPath = attachmentParam.Url;
		std::wstring attachSourceName = attachmentParam.SourceName;

		m_log->LogString(LEVEL_FINEST, L"attachPath: %s", attachPath.c_str());
		m_log->LogString(LEVEL_FINEST, L"attachSourceName: %s", attachSourceName.c_str());
		m_log->LogString(LEVEL_FINEST, L"sMessageFolderMCCID: %s", sMessageFolderMCCID.c_str());

		std::wstring localPath;
		int pos = attachPath.find(sMessageFolderMCCID);
		if (pos > 0)
		{
			localPath = attachPath.substr(pos, attachPath.length());
		}

		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());

		if (localPath.empty())
		{
			break;
		}

		if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
		{
			localPath = sAttachmentFolder + localPath;
		}
		else
		{
			localPath = sAttachmentFolder + L"\\" + localPath;
		}

		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());

		std::string sSource;

		std::ifstream _in(localPath/*attachPath*/.c_str(), std::ios::binary);
		// read whole file
		DWORD   dwDataSize = 0;
		if (_in.seekg(0, std::ios::end))
		{
			dwDataSize = static_cast<DWORD>(_in.tellg());
		}
		std::string buff(dwDataSize, 0);
		if (dwDataSize && _in.seekg(0, std::ios::beg))
		{
			std::copy(std::istreambuf_iterator< char>(_in),
				std::istreambuf_iterator< char >(),
				buff.begin());

			sSource = buff;
		}
		else
		{
			m_log->LogString(LEVEL_WARNING, L"Attachment file: \"%s\" is empty", /*attachPath*/localPath.c_str());
		}
		_in.close();

		m_log->LogString(LEVEL_FINEST, L"Source: %s", stow(sSource).c_str());

		if (!sSource.empty())
		{
			if (attachmentParam.dDecode)
			{
				sSource = boost::to_utf8(stow(sSource));
				_msg[attachSourceName] = stow(sSource);
			}
			else
			{
				_msg[attachSourceName] = stow(encodeBase64(sSource));
			}

		}
		else
		{
			m_log->LogString(LEVEL_FINEST, L"Source empty");
			_msg[attachSourceName] = L"";
		}

	}

	m_incomingMsgList.push_back(_msg);
}



static __int64 CreateMCCID(int num_message, uint32_t _client_id)
{
	__int64 mccid = _client_id;
	mccid = (mccid << 32) | num_message;
	return mccid;
}


void CDataReceiverImpl::receive_onAccepted(MessagePtr message)const
{

}


void CDataReceiverImpl::receive_onCompleted(const std::wstring& _email, MessagePtr msg)const
{
	SystemConfig settings;

	std::wstring sImapMessageType;
	std::wstring sFtpHostname, sFtpUsername, sFtpPassword;
	bool bAlterTextTypeEnable = true, bHtmlContentTypeEnable = true;
	try
	{
		sImapMessageType = settings->get_config_value<std::wstring>(_T("PreferedMessageType"));
	}
	catch (...)
	{
		sImapMessageType = L"text.plain";
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"PreferedMessageType\" config key");
	}

	try
	{
		bAlterTextTypeEnable = settings->safe_get_config_bool(L"AlterTextTypeEnable", true);
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AlterTextTypeEnable\" config key");
	}

	try
	{
		bHtmlContentTypeEnable = settings->safe_get_config_bool(L"HtmlContentTypeEnable", true);
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"HtmlContentTypeEnable\" config key");
	}
	
	try
	{
		sFtpHostname = settings->get_config_value<std::wstring>(_T("FtpHostname"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"FtpHostname\" config key");
	}

	try
	{
		sFtpUsername = settings->get_config_value<std::wstring>(_T("FtpUsername"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"FtpUsername\" config key");
	}

	try
	{
		sFtpPassword = settings->get_config_value<std::wstring>(_T("FtpPassword"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"FtpPassword\" config key");
	}



	auto mccid = CreateMCCID(msg->getIMAPMessageId(), GetClientId());
	std::wstring sMessageFolderMCCID = toStr<wchar_t>(mccid);

	CMessage newMail(L"MCC2TA_NEWEMAIL");
	newMail[L"Type"] = L"Email";
	newMail[L"MCCID"] = mccid;
	newMail[L"From"] = msg->getFrom();
	newMail[L"ReplyTo"] = msg->getReplyTo();
	newMail[L"To"] = _email;//msg->getTo().c_str(); 
	newMail[L"ToList"] = msg->getTo(); 
	newMail[L"CC"] = msg->getCC();
	newMail[L"Subject"] = msg->getSubject();
	newMail[L"Senddate"] = msg->getSendDate();
	newMail[L"ReceiveDate"] = msg->getReceiveDate();
	newMail[L"Host"] = stow(boost::asio::ip::host_name());
	newMail[L"FtpHostname"] = sFtpHostname;
	newMail[L"FtpUsername"] = sFtpUsername;
	newMail[L"FtpPassword"] = sFtpPassword;

	std::wstring sAttachmentFolder, sAttachmentString;
	try
	{
		sAttachmentFolder = settings->get_config_value<std::wstring>(_T("AttachmentFolder"));
		if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
		{
			sAttachmentFolder += sMessageFolderMCCID;
		}
		else
		{
			sAttachmentFolder += L"\\" + sMessageFolderMCCID;
		}
		sAttachmentFolder += L"\\inbox";

	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentFolder\" config key");
		sAttachmentFolder = L"c:\\Attachments\\inbox\\" + sMessageFolderMCCID;
	}

	try
	{
		sAttachmentString = settings->get_config_value<std::wstring>(_T("AttachmentString"));

		if (sAttachmentString[sAttachmentString.length() - 1] == ('/'))
		{
			sAttachmentString += sMessageFolderMCCID;
		}
		else
		{
			sAttachmentString += L"/" + sMessageFolderMCCID;
		}

		sAttachmentString+= L"/inbox";
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentString\" config key");
	}


	int iSectionCount = 0;
	bool bGotPlain = false;

	if (!boost::filesystem::exists(sAttachmentFolder) && !boost::filesystem::create_directories(sAttachmentFolder))
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_WARNING, L"Cannot create \"%s\" folder", sAttachmentFolder.c_str());
	}
	else
	{
		msg->getSectionsSource([&newMail, 
			sAttachmentFolder = sAttachmentFolder, 
			sAttachmentString = sAttachmentString, 
			sMessageFolderMCCID = sMessageFolderMCCID,
			&bGotPlain,
			&iSectionCount,
			sImapMessageType = sImapMessageType,
			bAlterTextTypeEnable = bAlterTextTypeEnable,
			bHtmlContentTypeEnable= bHtmlContentTypeEnable]
			(const std::wstring& _name, 
				const std::wstring& _content, 
				const std::wstring& _type, 
				const std::wstring& source)
		{
			if (!bHtmlContentTypeEnable && _type == L"html" && _content == L"text")
			{
				return;
			}

			std::wstring filename = /*_content + L"_" + _type + L"_" +*/ _name;// sAttachmentFolder + L"\\" + _name;

			std::wstring outputFileName = sAttachmentFolder + L"\\" + filename;
			std::wstring attachFileName = sAttachmentString + L"/" + filename;

			//std::wofstream ofs(sAttachmentFolder + L"\\" + filename, std::wofstream::ios_base::binary);
			//ofs.write(reinterpret_cast<const wchar_t*>(source.c_str()), source.size());
			////ofs << source;
			//ofs.close();

			std::fstream ofs(outputFileName, std::fstream::ios_base::out | std::fstream::ios_base::binary);
			ofs.write(wtos(source).c_str(), source.size());
			ofs.close();


			
			//std::wstring attachName = 

			//newMail[L"attach"]
			if (_content == L"text" && (_name == L"text.plain" || _name == L"text.html"))
			{
				std::wstring messageUrl = newMail.SafeReadParam(L"MessageUrl", core::checked_type::String, L"").AsWideStr();
				if (!bGotPlain)
				{
					newMail[L"MessageUrl"] = attachFileName;

					if (_name == sImapMessageType)
					{
						bGotPlain = true;
					}

					if (messageUrl.empty())
						return;

					attachFileName = messageUrl;
				}

				if (!bAlterTextTypeEnable)
					return;
				
				//if (messageUrl.empty())
				//	return;

				//attachFileName = messageUrl;
			}

			
			//if (_content == L"text")
			//{
			//	////if (_name == L"text.plain")
			//	////{
			//	////	bGotPlain = true;
			//	////	//newMail[L"Source"] = source;
			//	////	newMail[L"MessageUrl"] = attachFileName;

			//	////	return;
			//	////}
			//	////else if (_name == L"text.html" && !bGotPlain)
			//	////{
			//	////	//bGotPlain = true;
			//	////	//newMail[L"Source"] = source;
			//	////	newMail[L"MessageUrl"] = attachFileName;
			//	////}

			//	//if (_name == L"text.html")
			//	//{
			//	//	bGotPlain = true;
			//	//	//newMail[L"Source"] = source;
			//	//	newMail[L"MessageUrl"] = attachFileName;

			//	//	return;
			//	//}
			//	//else if (_name == L"text.plain" && !bGotPlain)
			//	//{
			//	//	//bGotPlain = true;
			//	//	//newMail[L"Source"] = source;
			//	//	newMail[L"MessageUrl"] = attachFileName;
			//	//	return;
			//	//}

			//	if (_name == sImapMessageType)
			//	{
			//		bGotPlain = true;
			//		newMail[L"MessageUrl"] = attachFileName;
			//		return;
			//	}
			//	else if (!bGotPlain && (_name == L"text.plain" | _name == L"text.html"))
			//	{
			//		newMail[L"MessageUrl"] = attachFileName;
			//		return;
			//	}

			//}
			//else
			//{
			int iAttachmentCount = iSectionCount++;

				newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Url"     ] = attachFileName;
				//newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Location"] = outputFileName;
				newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Type"    ] = 1;
				newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Name"    ] = filename;
				newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Size"    ] = source.size();
			//}
		});

	}

	newMail[L"AttachCount"] = iSectionCount;



	//std::wstring sSource, sCharset;
	//msg->getPlainTextAndCharset(sSource, sCharset);

	//newMail[L"Source"] = sSource;
	//newMail[L"Charset"] = sCharset;

	this->SendToAny(newMail);

	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = mccid;
	postmsg[L"From"] = msg->getFrom();
	postmsg[L"To"] = msg->getTo();
	postmsg[L"Dir"] = L"I";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"INBOX";

	this->SendToAll(postmsg);
}

void CDataReceiverImpl::receive_onFailed(const std::string& message)const
{
	int test = 0;
	// ToDo

	m_log->LogString(LEVEL_FINEST, L"Failed to read Message: %s", stow(message).c_str());
}


void CDataReceiverImpl::send_onAccepted(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"SENDING";

	SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));
}

void CDataReceiverImpl::send_onCompleted(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"SENT";

	this->SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));
}

void CDataReceiverImpl::send_onFailed(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Error"] = msg.SafeReadParam(L"Error", core::checked_type::Int, 0);
	postmsg[L"ErrorDescription"] = msg.SafeReadParam(L"ErrorDescription", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"FAILED";

	SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));

}



/******************************* eof *************************************/