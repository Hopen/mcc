/************************************************************************/
/* Name     : MCC\ImapFetchBodyStructureParser.h                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 27 May 2016                                               */
/************************************************************************/

#pragma once

#include <boost/algorithm/string/case_conv.hpp>
#include "ImapMessageContext.h"

namespace fetchbodystructureparser
{

	struct TBodyParam2
	{
		std::string name;
		std::string value;
	};


	struct TBodyParamList2
	{
		std::vector <TBodyParam2> params;

		template <class T>
		std::wstring getParamByName(T &&name)const
		{
			std::wstring value;
			auto cit = std::find_if(params.begin(), params.end(), [name = std::forward<T>(name)](const TBodyParam2& param)
			{
				if (param.name == name)
				{
					return true;
				}

				return false;
			});

			if (cit != params.end())
			{
				value = stow((*cit).value);
			}

			return value;
		}

		template <class T>
		std::wstring getParamByName_Decode(T &&name)const
		{
			std::wstring value = getParamByName(std::forward<T>(name));
			if (value.find(L"=?") == 0)
				value = DecodeSubject(wtos(value));

			return value;
		}

	};

	struct TAttacnmentParams
	{
		std::string type;
		TBodyParamList2 params;
	};

	struct TEncoding
	{
		std::string name;
	};


	struct TSection
	{
		std::string contentType;
		std::string subType;
		TBodyParamList2 body_param;
		TEncoding encoding;
		int size;
		TAttacnmentParams attach_param;

	public:
		std::wstring getEncoding()const { return boost::algorithm::to_lower_copy(stow(encoding.name)); }
		std::wstring getContentType()const { return boost::algorithm::to_lower_copy(stow(contentType)); }
		std::wstring getSubType()const { return boost::algorithm::to_lower_copy(stow(subType)); }
		int getSize()const noexcept { return size; }
	};

	struct TBodySubtype
	{
		std::string subType;
	};

	struct TBody;

	typedef
		boost::variant<
		boost::recursive_wrapper<TBody>
		, TSection
		>
		body_node;

	using sectionList = std::vector<TSection>;

	struct TBody
	{

	private:



	public:
		//sectionList GetSections()const
		//{
		//	sectionList list;

		//	_lineanConverter converter(&list);
		//	converter(*this);

		//	return list;
		//}

		SectionPtr GetSections()const;
		//{
		//	SectionPtr section = std::make_shared<CSection>();

		//	_lineanConverter converter(section, 0);
		//	converter(*this);

		//	_nodeLineanConverter::reset();

		//	return section;
		//}

	public:
		std::vector <body_node> sections;
		TBodySubtype subType;
	};

	struct TBodyStructure
	{
		int uid;
		TBody body; 
	};

	struct TDocument
	{
		int num;
		TBodyStructure structure;
	};



	struct TSource
	{
		std::string ss;
	};


	struct TContainer;

	typedef
		boost::variant<
		boost::recursive_wrapper<TContainer>
		, TSource
		>
		container_node;

	struct TContainer
	{
	public:
		std::vector <container_node> sections;
	};
}

bool fetchbodystructureparser_parse(std::string source, std::function <void(const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);
bool fetchbodystructureparser_parse2(std::string source, std::function <void(const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

/******************************* eof *************************************/