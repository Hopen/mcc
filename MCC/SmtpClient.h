/************************************************************************/
/* Name     : MCC\SmtpClient.h                                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/

#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "boost/asio/ssl.hpp"
#include "DataReceiver.h"

#include "ImapMessageContext.h"

class smtp_client
{
public:
	virtual void start() = 0;

	virtual ~smtp_client() {}
};

using SmtpClientPtr = std::shared_ptr<smtp_client>;

class CSystemLog;

SmtpClientPtr makeSSL_SmtpClient(
	boost::asio::io_service& io_service,
	boost::asio::ssl::context& context,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	MessageCollector& msg_list,
	CSystemLog* pLog,
	const std::wstring& _login,
	const std::wstring& _password,
	const std::wstring& _email,
	statusHandlerSmtp _accepted,
	statusHandlerSmtp _completed,
	statusHandlerSmtp _failed);

SmtpClientPtr makeNOSSL_SmtpClient(
	boost::asio::io_service& io_service,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	MessageCollector& msg_list,
	CSystemLog* pLog,
	const std::wstring& _login,
	const std::wstring& _password,
	const std::wstring& _email,
	statusHandlerSmtp _accepted,
	statusHandlerSmtp _completed,
	statusHandlerSmtp _failed);




/******************************* eof *************************************/

