/************************************************************************/
/* Name     : MCC\MailConnector.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/

#pragma once

#include <string>
#include <future>

#include "Router\router_compatibility.h"
#include "DataReceiver.h"
#include "ImapMessageContext.h"

class CDataReceiverImpl;
//class MessageCollector;

template <class Impl>
class CMailConnector
{
public:

	//bool GetMail2(CDataReceiverImpl *_routerInterface,
	//	const std::wstring& _email,
	//	const std::wstring& _certificate_uri,
	//	const std::wstring& _uri,
	//	const std::wstring& _port,
	//	const std::wstring& _login,
	//	const std::wstring& _pass,
	//	//bool bSSLEnable,
	//	bool bPeek)const
	//{
	//	//return static_cast<const Impl*>(this)->_getMail(_routerInterface, _email, certificate_uri, _uri, _port, _login, _pass, bSSLEnable, bPeek);
	//	return false;
	//}


	auto GetMail(CDataReceiverImpl *_routerInterface, 
//		const std::wstring& _email,
//		const std::wstring& _certificate_uri, 
//		const std::wstring& _uri, 
//		const std::wstring& _port,
//		const std::wstring& _login, 
//		const std::wstring& _pass, 
////		bool bSSLEnable, 
//		bool bPeek
		const TImapParams& params
	)const
	{
		return static_cast<const Impl*>(this)->_getMail(_routerInterface, params/*_email, _certificate_uri, _uri, _port, _login, _pass, false*/);
	}

	auto SendMail(CDataReceiverImpl *_routerInterface, MessageCollector& msg_list, const std::wstring& _email, const std::wstring& certificate_uri, const std::wstring& _uri, const std::wstring& _port,
		const std::wstring& _login, const std::wstring& _pass, bool bSSLEnable)const
	{
		return static_cast<const Impl*>(this)->_sendMail(_routerInterface, msg_list, _email, certificate_uri, _uri, _port, _login, _pass, bSSLEnable);
	}
};

//using ReturnFunctionType = std::future <unsigned int>;

//class imap_client;
class smtp_client;
class CMailConnectorImpl : public CMailConnector < CMailConnectorImpl >
{
	friend class CMailConnector < CMailConnectorImpl >;
public:
	CMailConnectorImpl(boost::asio::io_service& io);
	~CMailConnectorImpl();

private:
	bool _getMail(CDataReceiverImpl *_routerInterface, 
		/*const std::wstring& _email, 
		const std::wstring& certificate_uri, 
		const std::wstring& _uri, 
		const std::wstring& _port,
		const std::wstring& _login, 
		const std::wstring& _pass, 
		bool bPeek*/
		const TImapParams& params)const;

	bool _sendMail(CDataReceiverImpl *_routerInterface, MessageCollector& msg_list, const std::wstring& _email, const std::wstring& certificate_uri, const std::wstring& _uri, const std::wstring& _port,
		const std::wstring& _login, const std::wstring& _pass, bool bSSLEnable)const;

//private:
//	//handler
//	void _imap_accept(CDataReceiverImpl *, MessagePtr message)const;
//	void _imap_completed(CDataReceiverImpl *, const std::wstring& _email, MessagePtr message)const;
//	void _imap_failed(CDataReceiverImpl *, MessagePtr message)const;
//
//	void _smtp_accept(CDataReceiverImpl *, const CMessage& msg)const;
//	void _smtp_completed(CDataReceiverImpl *, const CMessage& msg)const;
//	void _smtp_failed(CDataReceiverImpl *, const CMessage& msg)const;

private:
	boost::asio::io_service& r_io;

	//using ImapClientPtr = std::shared_ptr<imap_client>;
	using SmtpClientPtr = std::shared_ptr<smtp_client>;
	//using ClientCollector = std::list <ImapClientPtr>;

	CDataReceiverImpl *m_pReceiver;
};


/******************************* eof *************************************/
