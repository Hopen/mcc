/************************************************************************/
/* Name     : MCC\SmtpClient.cpp                                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/program_options/detail/convert.hpp>

#include "SmtpClient.h"
#include "Log/SystemLog.h"
#include "SmtpReadStatusParser.h"
#include "ConnectionClasses.h"

//#include <boost/archive/iterators/base64_from_binary.hpp>
//#include <boost/archive/iterators/transform_width.hpp>

const int SMTP_MAX_STRING_LEN = 998;
const std::string END_OF_MESSAGE_TEXT = ".\r\n";

//using base64_text = boost::archive::iterators::base64_from_binary<boost::archive::iterators::transform_width<const char *, 6, 8> > ;

template <class TSocketImpl>
class smtp_client_impl : /*public std::enable_shared_from_this <smtp_client_impl <TSocketImpl> >,*/ public smtp_client
{
public:
	smtp_client_impl(
		std::unique_ptr<TSocketImpl>&& _impl,
		//boost::asio::io_service& io_service,
		//boost::asio::ssl::context& context,
		//boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
		MessageCollector& msg_list,
		CSystemLog* pLog,
		const std::wstring& _login,
		const std::wstring& _password,
		const std::wstring& _sSmtpClientAddress,
		statusHandlerSmtp _accepted,
		statusHandlerSmtp _completed,
		statusHandlerSmtp _failed)
		: socket_(std::move(_impl)), m_Log(pLog), m_login(_login), m_password(_password), m_sSmtpClientAddress(_sSmtpClientAddress),
		m_msg_list(msg_list),
		m_onAcceptedHandle(_accepted),
		m_onCompletedHandle(_completed),
		m_onFailedHandle(_failed)
		/*endpoint_iterator_(endpoint_iterator)*/
	{
	}

	~smtp_client_impl() {}

	void start()
	{
		//socket_.set_verify_mode(/*boost::asio::ssl::verify_peer*/boost::asio::ssl::verify_none);
		////socket_.set_verify_mode(boost::asio::ssl::verify_peer);
		//socket_.set_verify_callback(
		//	boost::bind(&smtp_client_impl::verify_certificate, this /*shared_from_this()*/, _1, _2));

		//boost::asio::async_connect(socket_.lowest_layer(), endpoint_iterator_,
		//	boost::bind(&smtp_client_impl::on_connect_done, this /*shared_from_this()*/,
		//		boost::asio::placeholders::error));
		socket_->start(boost::bind(&smtp_client_impl::verify_certificate, this /*shared_from_this()*/, _1, _2),
			boost::bind(&smtp_client_impl::on_connect_ssl_done, this /*shared_from_this()*/,
				boost::asio::placeholders::error/*, boost::asio::placeholders::iterator*/),
			boost::bind(&smtp_client_impl::on_connect_nossl_done, this /*shared_from_this()*/,
				boost::asio::placeholders::error, boost::asio::placeholders::iterator));


	}

private:
	bool verify_certificate(bool preverified,
		boost::asio::ssl::verify_context& ctx)
	{
		// The verify callback can be used to check whether the certificate that is
		// being presented is valid for the peer. For example, RFC 2818 describes
		// the steps involved in doing this for HTTPS. Consult the OpenSSL
		// documentation for more details. Note that the callback is called once
		// for each certificate in the certificate chain, starting from the root
		// certificate authority.

		// In this example we will simply print the certificate's subject name.
		char subject_name[256];
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

		std::wstring answer(stow(std::string(subject_name)));

		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", answer.c_str(), preverified ? L"true" : L"false");


		return preverified;
	}

	//void on_connect_done(const boost::system::error_code& error)
	//{
	//	if (!error)
	//	{
	//		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Connection done");
	//		socket_.lowest_layer().set_option(boost::asio::ip::tcp::no_delay(true));
	//		socket_.async_handshake(boost::asio::ssl::stream_base::client,
	//			boost::bind(&smtp_client_impl::on_handshake_done, this /*shared_from_this()*/,
	//				boost::asio::placeholders::error));
	//	}
	//	else
	//	{
	//		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Connect failed: \"%s\"", stow(error.message()).c_str());
	//	}
	//}


	void on_connect_ssl_done(const boost::system::error_code& error/*, boost::asio::ip::tcp::resolver::iterator iterator*/)
	{
		if (!error)
		{
			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Handshake done, logining...");
			// Read the response status line.
			socket_->readUntil("\r\n");

			make_ehlo();
			write();

		}
		else
		{
			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Handshake failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	void on_connect_nossl_done(const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator)
	{
		if (!error)
		{
			// Read the response status line.
			socket_->readUntil("\r\n");

			make_ehlo();
			write();

		}
		else
		{
			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Connect failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	//void on_handshake_done(const boost::system::error_code& error)
	//{
	//	if (!error)
	//	{
	//		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Handshake done");
	//		// Read the response status line.
	//		boost::asio::streambuf response;
	//		boost::asio::read_until(socket_, response, "\r\n");

	//		make_ehlo("");

	//	}
	//	else
	//	{
	//		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Handshake failed: \"%s\"", stow(error.message()).c_str());
	//	}
	//}

	void make_ehlo()
	{
		push_command((boost::format("EHLO %s\r\n") % wtos(m_sSmtpClientAddress).c_str()).str(), boost::bind(&smtp_client_impl::on_ehlo, this /*shared_from_this()*/, _1, _2), nullptr);
	}

	void make_reset()
	{
		push_command("rset\r\n", boost::bind(&smtp_client_impl::on_ready_to_send, this /*shared_from_this()*/, _1, _2), nullptr);
	}

	void on_noop(int, const std::string& _reply)
	{
		int test = 0;
	}


	//std::string encodeBase64(std::string pData)
	//{
	//	std::stringstream os;
	//	size_t sz = pData.size();
	//	std::copy(base64_text(pData.c_str()), base64_text(pData.c_str() + sz), std::ostream_iterator<char>(os));
	//	return os.str();
	//}

	void on_ehlo(int, const std::string& _reply)
	{
		if (bAUTHLOGIN)
		{
			push_command("auth login\r\n", nullptr, nullptr);
			push_command((boost::format("%s\r\n") % encodeBase64(wtos(m_login)).c_str()).str(), nullptr, nullptr);
			push_command((boost::format("%s\r\n") % encodeBase64(wtos(m_password)).c_str()).str(), boost::bind(&smtp_client_impl::on_ready_to_send, this /*shared_from_this()*/, _1, _2), nullptr);
		}
		else
		{
			on_ready_to_send(int(), _reply);
		}


		//m_response_list.push_back(THandler(newCommand,
		//	static_cast<boost::function<void(const std::string &)>>(boost::bind(&smtp_client_impl::on_login, this /*shared_from_this()*/, _1))));

		//write();
	}

	//void on_login(const std::string& _reply)
	//{
	//	std::string newCommand = (boost::format("%s\r\n") %encodeBase64(wtos(m_login)).c_str()).str();

	//	m_response_list.push_back(THandler(newCommand,
	//		static_cast<boost::function<void(const std::string &)>>(boost::bind(&smtp_client_impl::on_password, this /*shared_from_this()*/, _1))));

	//	write(newCommand);
	//}

	//void on_password(const std::string& _reply)
	//{
	//	std::string newCommand = (boost::format("%s\r\n") % encodeBase64(wtos(m_password)).c_str()).str();

	//	m_response_list.push_back(THandler(newCommand,
	//		static_cast<boost::function<void(const std::string &)>>(boost::bind(&smtp_client_impl::on_auth_plain, this /*shared_from_this()*/, _1))));

	//	write(newCommand);

	//}


	void make_message()
	{
		while (true)
		{
			auto cit = m_msg_list.begin();
			if (cit == m_msg_list.end())
			{
				make_quit();
				break;
			}

			auto newMsg = *cit;
			m_msg_list.erase(cit);

			m_onAcceptedHandle(newMsg);

			std::wstring sTo = newMsg.SafeReadParam(L"Customer", core::checked_type::String, L"").AsWideStr(),
				sFrom = newMsg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr(),
				sSubject = newMsg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr(),
				sMessage = newMsg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr(),
				sCC = newMsg.SafeReadParam(L"CC", core::checked_type::String, L"").AsWideStr(),
				sBCC = newMsg.SafeReadParam(L"BCC", core::checked_type::String, L"").AsWideStr()/*,
				sImapMessageType = newMsg.SafeReadParam(L"ImapMessageType", core::checked_type::String, L"text.plain").AsWideStr()*/;

			

			std::vector <std::wstring> toList, ccList, bccList;

			std::set<std::wstring> allList;

			boost::split(toList, sTo, std::bind2nd(std::equal_to<wchar_t>(), ';'));
			boost::split(ccList, sCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));
			boost::split(bccList, sBCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));

			allList.insert(toList.begin(), toList.end());
			allList.insert(ccList.begin(), ccList.end());
			allList.insert(bccList.begin(), bccList.end());

			//std::wstring sCharset = newMsg.SafeReadParam(L"Charset", core::checked_type::String, L"").AsWideStr();

			int attachmentsCount = newMsg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();

			//if (sCharset != L"ascii" && !bBITMIME)
			//{
			//	m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Send mail failed: no 8bit MIME Transport available, cannot send message in \"%s\" encoding", sCharset.c_str());
			//	m_Log->LogStringModule(LEVEL_INFO, L"DUMP Message", L"%s", newMsg.Dump().c_str());
			//	continue;
			//}

			push_command((boost::format("mail from:<%s>\r\n") % wtos(sFrom).c_str()).str(), nullptr, boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));
			for each (auto address in allList)
			{
				if (!address.empty()) //? how we get here empty address?
					push_command((boost::format("rcpt to:<%s>\r\n") % wtos(address).c_str()).str(), nullptr, boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));
			}
			push_command("data\r\n", /*boost::bind(&smtp_client_impl::on_writeData, this, _1, _2)*/nullptr, boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));
			std::string outString;
			if (1)
			{
				std::wstring sTemp;
				if (!sSubject.empty())
				{
					sTemp = (boost::wformat(L"subject:%s\r\n") % sSubject.c_str()).str();
				}

				sTemp += (boost::wformat(L"from:%s\r\nTo:%s\r\n")
					% sFrom.c_str()
					% sTo.c_str()).str();

				if (!sCC.empty())
				{
					sTemp += (boost::wformat(L"CC:%s\r\n")
						% sCC.c_str()).str();
				}

				if (!sBCC.empty())
				{
					sTemp += (boost::wformat(L"BCC:%s\r\n")
						% sBCC.c_str()).str();
				}

				sTemp += L"MIME-Version: 1.0\r\n";
				if (!attachmentsCount)
				{
					//if (!sImapMessageType.compare(L"text.html"))
						sTemp += L"Content-type: text/html; charset=utf-8\r\n";
					//else
					//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

					//sTemp += L"Content-Transfer-Encoding: base64\r\n";
					sTemp += L"\r\n";
					sTemp += sMessage;
				}
				else
				{
					sTemp += L"Content-type: multipart/mixed; boundary=\"frontier\"\r\n";
					sTemp += L"\r\n";

					if (!sMessage.empty())
					{
						sTemp += L"--frontier\r\n";
						
						//if (!sImapMessageType.compare(L"text.html"))
							sTemp += L"Content-type: text/html; charset=utf-8\r\n";
						//else
						//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

						//sTemp += L"Content-Transfer-Encoding: base64\r\n";
						sTemp += L"\r\n";
						sTemp += sMessage;
						sTemp += L"\r\n\r\n";
					}
				}

				//push message into commands
				//push_command(wtos(sTemp), nullptr, boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));

				outString += wtos(sTemp);
			}


			if (attachmentsCount)
			{
				for (int i = 0; i < attachmentsCount; ++i)
				{
					std::string sSource, sFileName;
					std::wstring attachUrlName = L"Attach" + toStr<wchar_t>(i) + L"Name",
						attachSourceName = L"Attach" + toStr<wchar_t>(i) + L"Source";

					auto attach = newMsg.SafeReadParam(attachSourceName, core::checked_type::String, L"");
					if (!attach.IsEmpty() && attach.AsStr().length())
					{
						sSource = attach.AsStr();
					}
					else
					{
						continue;
					}

					auto fileName = newMsg.SafeReadParam(attachUrlName, core::checked_type::String, L"");
					if (!fileName.IsEmpty() && fileName.AsStr().length())
					{
						sFileName = fileName.AsStr();
					}

					if (sSource.empty() || sFileName.empty())
						continue;

					std::string sTemp = "--frontier\r\n";
					sTemp += (boost::format("Content-Disposition: attachment; filename=\"%s\"\r\n")
						% sFileName.c_str()).str();
					sTemp += "Content-Transfer-Encoding: base64\r\n";
					sTemp += "\r\n";
					//push_command(sTemp, nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));

					sTemp += sSource;
					sTemp += "\r\n";

					//std::size_t pos1 = 0;
					//std::size_t strLen = sSource.length();


					//while (pos1 < strLen)
					//{
					//	decltype(sSource) ssTemp;

					//	std::size_t msgSize = SMTP_MAX_STRING_LEN;
					//	if (msgSize > strLen - pos1)
					//		msgSize = strLen - pos1;

					//	std::copy(sSource.data() + pos1, sSource.data() + pos1 + msgSize, std::back_inserter(ssTemp));
					//	ssTemp += "\r\n";

					//	push_command(ssTemp, nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));

					//	pos1 += msgSize;
					//}

					//push_command("\r\n\r\n", nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));

					outString += sTemp;
				}

				//push_command("--frontier\r\n", nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));
				outString += "--frontier\r\n";
			}

			////push end of data to commands
			//push_command(std::string("\r\n"), nullptr, boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));
			//push_command(END_OF_MESSAGE_TEXT, boost::bind(&smtp_client_impl::on_successed, this /*shared_from_this()*/, newMsg, _1, _2), boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));
			outString += "\r\n" + END_OF_MESSAGE_TEXT;

			push_command(outString, boost::bind(&smtp_client_impl::on_successed, this /*shared_from_this()*/, newMsg, _1, _2), boost::bind(&smtp_client_impl::on_failed, this /*shared_from_this()*/, newMsg, _1, _2));

			break;
		}

	}

	
	void on_successed(const CMessage& msg, int, const std::string& _reply)
	{
		m_onCompletedHandle(msg);

		on_ready_to_send(int(), _reply);
	}

	void on_failed(const CMessage& msg, int errorCode, const std::string& _reply)
	{
		CMessage error(msg);
		error[L"ErrorCode"] = errorCode;
		error[L"ErrorDescription"] = stow(_reply);

		m_onFailedHandle(error);

		on_ready_to_send(int(), _reply);
	}

	void on_ready_to_send(int, const std::string& _reply)
	{
		make_message();
	}

	void make_quit()
	{
		//m_response_list.push_back(THandler("QUIT",
		//	static_cast<boost::function<void(const std::string &)>>(boost::bind(&smtp_client_impl::on_quit, this /*shared_from_this()*/, _1))));
		push_command("quit\r\n", static_cast<ResponseHandler>(boost::bind(&smtp_client_impl::on_quit, this /*shared_from_this()*/, _1, _2)), nullptr);

		//write();
	}


	//template <class TString>
	//void write(TString &&command/*, boost::function <void(const std::string&)> handler*/)
	////write()
	//{


	//	m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Write: \"%s\"", stow(command).c_str());

	//	socket_->read(reply_, sizeof(reply_),
	//		boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
	//			boost::asio::placeholders::error,
	//			boost::asio::placeholders::bytes_transferred));


	//	socket_->write(std::forward<TString>(command),
	//		boost::bind(&smtp_client_impl::on_write, this /*shared_from_this()*/,
	//			boost::asio::placeholders::error,
	//			boost::asio::placeholders::bytes_transferred));


	//	//socket_.async_read_some(
	//	//	boost::asio::buffer(reply_, sizeof(reply_)),
	//	//	boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
	//	//		boost::asio::placeholders::error,
	//	//		boost::asio::placeholders::bytes_transferred));

	//	//socket_.async_write_some(
	//	//	boost::asio::buffer(command.c_str(), command.size()),
	//	//	boost::bind(&smtp_client_impl::on_write, this /*shared_from_this()*/,
	//	//		boost::asio::placeholders::error,
	//	//		boost::asio::placeholders::bytes_transferred));
	//}

	void on_writeData(int, const std::string& _reply)
	{
		auto cit = this->m_response_list.begin();
		if (cit == m_response_list.end())
			return;

		while (cit!= m_response_list.end())
		{
			auto response = *cit;
			if (response._command == END_OF_MESSAGE_TEXT)
				break;
			
			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Write: \"%s\"", stow(response._command).c_str());
			socket_->write(response._command,
				boost::bind(&smtp_client_impl::on_write, this /*shared_from_this()*/,
					boost::asio::placeholders::error,
					boost::asio::placeholders::bytes_transferred));

			m_response_list.erase(cit);
			cit = this->m_response_list.begin();
		}

		//write();
		//socket_->read(reply_, sizeof(reply_),
		//	boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
		//		boost::asio::placeholders::error,
		//		boost::asio::placeholders::bytes_transferred));
	}

	void write()
	{
		auto cit = this->m_response_list.begin();
		if (cit == m_response_list.end())
			return;

		auto response = *cit;

		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Write: \"%s\"", stow(response._command).c_str());

		socket_->read(reply_, sizeof(reply_),
			boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));


		socket_->write(response._command,
			boost::bind(&smtp_client_impl::on_write, this /*shared_from_this()*/,
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));


		//socket_.async_read_some(
		//	boost::asio::buffer(reply_, sizeof(reply_)),
		//	boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
		//		boost::asio::placeholders::error,
		//		boost::asio::placeholders::bytes_transferred));

		//socket_.async_write_some(
		//	boost::asio::buffer(command.c_str(), command.size()),
		//	boost::bind(&smtp_client_impl::on_write, this /*shared_from_this()*/,
		//		boost::asio::placeholders::error,
		//		boost::asio::placeholders::bytes_transferred));
	}

	void on_read(const boost::system::error_code& error,
		size_t bytes_transferred)
	{
		if (!error)
		{
			if (bytes_transferred)
			{
				std::string in_data, status;
				in_data.append(reply_, bytes_transferred);

				m_Log->LogStringModule(LEVEL_FINEST, L"SMTP", L"Reply: %s", in_data.c_str());

				readBuffer += in_data;
				std::string lastString;
				////looking for last string
				if (!readBuffer.empty())
				{
					int pos = readBuffer.rfind("\r\n");
					if (pos >= 0)
					{
						lastString = readBuffer.substr(0, pos + strlen("\r\n"));
						std::size_t right = pos + strlen("\r\n");
						if (right < readBuffer.size())
							readBuffer = readBuffer.substr(right, readBuffer.size() - right);
						else
							readBuffer.clear();

					}
					else
					{
						lastString = readBuffer;
					}
				}


				// looking for end of command
				//std::istringstream response_stream(readBuffer);

				//while (std::getline(response_stream, status))
				//{
				//if (status[status.size() - 1] == '\r')
				//status.erase(status.size() - 1);

				//if (status.empty())
				//continue;

				auto cit = m_response_list.begin();
				const THandler handler = *cit;;
				m_response_list.erase(cit);

				int codeStatus= 0;

				bool sOK = true;

				auto lambda = [this, lastString = lastString, &codeStatus, &sOK](const smtpreadstatusparser::TCommand& command_status, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
				{
					switch (command_status.code)
					{
					case -8:case 211:case 214:case 220:case 221:case 250:case 251:case 252:case 354:
					{
						if (command_status.data == "PIPELINING")
							bPIPELINING = true;
						else if (command_status.data == "BITMIME")
							bBITMIME = true;
						else if ((command_status.data.find("AUTH") != std::string::npos) && (command_status.data.find("LOGIN") != std::string::npos))
							bAUTHLOGIN = true;

						////OK
						//if (handler._on_completed)
						//{
						//	handler._on_completed(lastString);
						//}

						break;
					}
					case 421:case 450:case 451:case 452:case 500:case 501:case 502:case 503:
					case 504:case 550:case 551:case 552:case 553:case 554:
					{
						sOK = false;
						codeStatus = command_status.code;
						//failed
						m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Failed execution command, error code= \"%i\": %s",
							command_status.code,
							stow(command_status.data).c_str()
							);

						//if (handler._on_failed)
						//{
						//	handler._on_failed(lastString);
						//}

						//m_response_list.clear();
						//make_reset();

						break;
					}
					default:
					{
						//unknown
						break;
					}
					};

				};

				bool r = smtpreadstatusparser::parse(
					lastString.begin(),
					lastString.end(),
					lambda);

				

				if (!r)
				{
					ParsingFailed(lastString);
					
					m_response_list.clear();
					make_quit();
				}
				else
				{
					if (sOK)
					{
						if (handler._on_completed)
						{
							handler._on_completed(codeStatus, lastString);
						}
					}
					else
					{
						if (handler._on_failed)
						{
							//std::wstring sError = (boost::wformat(L"SMTP Failed execution command, error code= \"%i\": %s")
							//	% codeStatus % lastString.c_str()).str();
							handler._on_failed(codeStatus, lastString);
							m_response_list.clear();
							make_reset();
						}

					}
				}
				//else
				//{
				//	if (handler._handler)
				//	{
				//		handler._handler(lastString);
				//	}
				//}

				write();
			}
			else
			{
				//!! error here !!
				m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Read failed: no bytes transfered");
			}

			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"async_read_some");
			////read next part
			//socket_->read(reply_, sizeof(reply_),
			//	boost::bind(&smtp_client_impl::on_read, this /*shared_from_this()*/,
			//		boost::asio::placeholders::error,
			//		boost::asio::placeholders::bytes_transferred));
		}
		else
		{
			m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Read failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	void smtp_client_impl::on_quit(int, const std::string & source)
	{
		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"Finished");
	}

	void on_write(const boost::system::error_code& error,
		size_t bytes_transferred)
	{
		//nothing to do
		m_Log->LogStringModule(LEVEL_INFO, L"SMTP", L"%i bytes has wrote", bytes_transferred);
	}

	void ParsingFailed(const std::string & source)
	{
		m_Log->LogStringModule(LEVEL_INFO, L"SPIRIT", L"Parsing failed: \"%s\"",
			stow(source).c_str()
			);
	}

private:
	template <class T, class H1, class H2>
	void push_command(T&& _command, H1&& _handler1, H2&& _handler2)
	{
		m_response_list.emplace_back(std::forward<T>(_command),
			std::forward<H1>(_handler1), std::forward<H2>(_handler2));
	}

private:

	//boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket_;
	std::unique_ptr<TSocketImpl> socket_;

	//boost::asio::ip::tcp::resolver::iterator endpoint_iterator_;
	CSystemLog* m_Log;

	char reply_[131072];

	std::wstring m_login;
	std::wstring m_password;
	//std::wstring m_email;
	std::wstring m_sSmtpClientAddress;

	std::atomic<int> m_iCount{ 1 };
	std::atomic<int> m_iResponseQuantity{ 0 };
	std::atomic<int> m_iFetchAnswersQuantity{ 0 };

	//std::list <std::string> m_commands;

	bool bBITMIME{ false };
	bool bPIPELINING{ false };
	bool bAUTHLOGIN{ false };

	using ResponseHandler = std::function <void(int, const std::string&)>;

	struct THandler
	{
		std::string _command;
		ResponseHandler _on_completed;
		ResponseHandler _on_failed;

		THandler() = default;

		template <class T>
		THandler(T&& command, const ResponseHandler& on_completed, const ResponseHandler& on_failed)
			: _command(std::forward<T>(command)), _on_completed(on_completed), _on_failed(on_failed)
		{}

	};

	//using ResponseHandlersCollector = std::map<int, THandler>;
	using ResponseHandlersCollector = std::list<THandler>;

	ResponseHandlersCollector m_response_list;

	//MessageMap m_messages;

	std::string readBuffer;
	std::string writeBuffer;

	MessageCollector m_msg_list;

	statusHandlerSmtp m_onAcceptedHandle;
	statusHandlerSmtp m_onCompletedHandle;
	statusHandlerSmtp m_onFailedHandle;

};

SmtpClientPtr makeSSL_SmtpClient(
	boost::asio::io_service& io_service,
	boost::asio::ssl::context& context,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	MessageCollector& msg_list,
	CSystemLog* pLog,
	const std::wstring& _login,
	const std::wstring& _password,
	const std::wstring& _sSmtpClientAddress,
	statusHandlerSmtp _accepted,
	statusHandlerSmtp _completed,
	statusHandlerSmtp _failed)
{
	return std::make_shared<smtp_client_impl <SSL_client> >(std::make_unique<SSL_client>(io_service, context, endpoint_iterator), msg_list, pLog, _login, _password, _sSmtpClientAddress, _accepted, _completed, _failed);
}


SmtpClientPtr makeNOSSL_SmtpClient(
	boost::asio::io_service& io_service,
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
	MessageCollector& msg_list,
	CSystemLog* pLog,
	const std::wstring& _login,
	const std::wstring& _password,
	const std::wstring& _sSmtpClientAddress,
	statusHandlerSmtp _accepted,
	statusHandlerSmtp _completed,
	statusHandlerSmtp _failed)
{
	return std::make_shared<smtp_client_impl <NOSSL_client> >(std::make_unique<NOSSL_client>(io_service, endpoint_iterator), msg_list, pLog, _login, _password, _sSmtpClientAddress, _accepted, _completed, _failed);
}


/******************************* eof *************************************/