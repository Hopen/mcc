/************************************************************************/
/* Name     : MCC\ConnectionClasses.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/

#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "boost/asio/ssl.hpp"
#include <boost/function.hpp>

class abstract_socket_vrapper
{
public:
	abstract_socket_vrapper(boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
		:endpoint_iterator_(endpoint_iterator) {}

	virtual void start(boost::function<bool(bool, boost::asio::ssl::verify_context&)> verify_func,
		boost::function<void(const boost::system::error_code&)> connection_func1,
		boost::function<void(const boost::system::error_code&, boost::asio::ip::tcp::resolver::iterator iterator)>) = 0;

	virtual ~abstract_socket_vrapper() = default;

	abstract_socket_vrapper(const abstract_socket_vrapper&) = default;
	abstract_socket_vrapper(abstract_socket_vrapper&&) = default;

	abstract_socket_vrapper& operator=(const abstract_socket_vrapper&) = default;
	abstract_socket_vrapper& operator=(abstract_socket_vrapper&&) = default;

protected:
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator_;
	std::string m_buffer;

};

class SSL_client :public abstract_socket_vrapper
{
public:
	SSL_client(boost::asio::io_service& io_service,
		boost::asio::ssl::context& context,
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator
		)
		: abstract_socket_vrapper(endpoint_iterator),
		socket_(io_service, context)
	{
	}


	SSL_client(const SSL_client&) = default;
	SSL_client(SSL_client&&) = default;

	SSL_client& operator=(const SSL_client&) = default;
	SSL_client& operator=(SSL_client&&) = default;

	SSL_client::~SSL_client()
	{
		int test = 0;
	}

	void start(boost::function<bool(bool, boost::asio::ssl::verify_context&)> verify_func,
		boost::function<void(const boost::system::error_code&)> connection_func1,
		boost::function<void(const boost::system::error_code&, boost::asio::ip::tcp::resolver::iterator iterator)> /*connection_func2*/
		)
	{
		socket_.set_verify_mode(/*boost::asio::ssl::verify_peer*/boost::asio::ssl::verify_none);
		//socket_.set_verify_mode(boost::asio::ssl::verify_peer);
		socket_.set_verify_callback(verify_func);

		handshake_func = connection_func1;
		boost::asio::async_connect(socket_.lowest_layer(), endpoint_iterator_,
			boost::bind(&SSL_client::on_connect_done, this,
				boost::asio::placeholders::error, boost::asio::placeholders::iterator));
	}


	void on_connect_done(const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator iterator)
	{
		if (!error)
		{
			socket_.lowest_layer().set_option(boost::asio::ip::tcp::no_delay(true));
			socket_.async_handshake(boost::asio::ssl::stream_base::client,
				handshake_func);
		}
		else
		{
			//m_Log->LogStringModule(LEVEL_INFO, L"IMAP", L"Connect failed: \"%s\"", stow(error.message()).c_str());
		}
	}

	template <class MatchCondition>
	std::string readUntil(MatchCondition&& matchCondition)
	{
		boost::asio::streambuf response;
		boost::asio::read_until(socket_, response, std::forward<MatchCondition>(matchCondition));

		std::istream is(&response);
		std::string s;
		is >> s;

		return s;
	}

	void read(char*buffer, std::size_t size, boost::function <void(const boost::system::error_code&, size_t)> handler)
	{
		socket_.async_read_some(
			boost::asio::buffer(buffer, size),
			handler);

	}
	void write(std::string command, boost::function <void(const boost::system::error_code&, size_t)> handler)
	{
		//socket_.async_write_some(
		//	boost::asio::buffer(command.c_str(), command.size()),
		//	handler);

		m_buffer = command;

		boost::asio::streambuf request;
		std::ostream request_stream(&request);
		request_stream << m_buffer;

		boost::asio::write(socket_, request);

	}
private:
	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket_;
	boost::function<void(const boost::system::error_code&)> handshake_func;
};

class NOSSL_client : public abstract_socket_vrapper
{
public:
	NOSSL_client(boost::asio::io_service& io_service,
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator
		)
		:abstract_socket_vrapper(endpoint_iterator),
		socket_(io_service)
	{
	}

	NOSSL_client(const NOSSL_client&) = default;
	NOSSL_client(NOSSL_client&&) = default;

	NOSSL_client& operator=(const NOSSL_client&) = default;
	NOSSL_client& operator=(NOSSL_client&&) = default;

	NOSSL_client::~NOSSL_client()
	{
		int test = 0;
	}

	void start(boost::function<bool(bool, boost::asio::ssl::verify_context&)>,
		boost::function<void(const boost::system::error_code&)>,
		boost::function<void(const boost::system::error_code&, boost::asio::ip::tcp::resolver::iterator iterator)> connection_func
		)
	{
		boost::asio::async_connect(socket_.lowest_layer(), endpoint_iterator_,
			connection_func);

	}

	template <class MatchCondition>
	std::string readUntil(MatchCondition&& matchCondition)
	{
		boost::asio::streambuf response;
		boost::asio::read_until(socket_, response, std::forward<MatchCondition>(matchCondition));

		std::istream is(&response);
		std::string s;
		is >> s;

		return s;
	}

	void read(char*buffer, std::size_t size, boost::function <void(const boost::system::error_code&, size_t)> handler)
	{
		socket_.async_read_some(
			boost::asio::buffer(buffer, size),
			handler);

	}


	void write(std::string command, boost::function <void(const boost::system::error_code&, size_t)> handler)
	{
		//socket_.async_write_some(
		//	boost::asio::buffer(command.c_str(), command.size()),
		//	handler);

		//boost::asio::async_write(socket_, boost::asio::buffer(command.c_str(), command.size()), handler);

		m_buffer = command;

		boost::asio::streambuf request;
		std::ostream request_stream(&request);
		request_stream << m_buffer;

		boost::asio::write(socket_, request);

	}


private:
	boost::asio::ip::tcp::socket socket_;
};


/******************************* eof *************************************/