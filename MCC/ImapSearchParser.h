/************************************************************************/
/* Name     : MCC\ImapSearchParser.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Mar 2016                                               */
/************************************************************************/

#pragma once

bool searchparser_parse(std::string source, std::function <void(const int& i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

bool searchparser_parse2(std::string source, std::function <void(const int& i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

/******************************* eof *************************************/