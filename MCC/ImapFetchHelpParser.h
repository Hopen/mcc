/************************************************************************/
/* Name     : MCC\ImapFetchHelpParser.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/

#pragma once
#include "stdafx.h"

//bool readstatusparser_parse(std::string source, std::function <void(const readstatusparser::TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback);

//bool listparser_parse(std::string source, std::function <void(const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

//bool searchparser_parse(std::string source, std::function <void(const int& i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

//bool fetchallparser_parse(std::string source, std::function <void(const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

//bool fetchbodystructureparser_parse(std::string source, std::function <void(const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

///******************************* eof *************************************/