﻿// FetchBodyTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cctype>

#include "MCC/ImapListParser.h"
#include "MCC/ImapReadStatusParser.h"

std::string CutFuckingSubject(std::string src)
{
	std::string result = src;

	int pos1 = src.find('{');
	if (pos1 < 0)
		return result;

	int pos2 = src.find('}', pos1);

	if (pos2 < 0)
		return result;

	std::string size = src.substr(pos1 + 1, pos2 - pos1 - 1);

	int iSize = 0;

	try
	{
		iSize = toNum<int>(size);
	}
	catch (...)
	{

	}

	if (0 == iSize)
		return result;

	++pos2;

	for (int i = 0; i < 3; ++i)
	{
		if (result[pos2] == '\r' || result[pos2] == '\n')
		{
			++pos2;
		}
	}


	std::string subject = src.substr(pos2, iSize);
	std::string left = src.substr(0, pos1);
	std::string right = src.substr(pos2 + iSize, src.size() - (pos2 + iSize));
	int pos = right.find("((");
	if (pos > 0)
	{
		right = right.substr(pos, right.size() - pos);
	}

	//boost::replace_all(subject, "\n", "");
	//boost::replace_all(subject, "\r", "");
	boost::replace_all(subject, "\"", "");

	result = left + "\"" + subject + "\" " + right;

	return result;
}

void TestImapList()
{
	std::vector<std::string> list;

	list.emplace_back("* LIST (\\HasChildren) \"/\" Calendar");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" Calendar/Birthdays");
	list.emplace_back("* LIST (\\HasChildren) \"/\" Contacts");
	list.emplace_back("* LIST (\\HasNoChildren \\Trash) \"/\" \"Deleted Items\"");
	list.emplace_back("* LIST (\\HasNoChildren \\Drafts) \"/\" Drafts");
	list.emplace_back("* LIST (\\Marked \\HasNoChildren) \"/\" INBOX");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" Journal");
	list.emplace_back("* LIST (\\HasNoChildren \\Junk) \"/\" \"Junk Email\"");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" Notes");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" Outbox");
	list.emplace_back("* LIST (\\HasNoChildren \\Sent) \"/\" \"Sent Items\"");
	list.emplace_back("* LIST (\\HasChildren) \"/\" \"Sync Issues\"");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" \"Sync Issues/Conflicts\"");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" \"Sync Issues/Local Failures\"");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" \"Sync Issues/Server Failures\"");
	list.emplace_back("* LIST (\\HasNoChildren) \"/\" Tasks");


	std::string errString;

	for (const auto& source : list)
	{
		std::cout << source << std::endl;

		bool r = listparser_parse(
			source,
			[](const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			int test = 123;
		},
			errString);


		assert(r);
	}

}

void TestImapStatus()
{
	std::vector<std::string> list;
	list.emplace_back("a3 OK [READ-WRITE] SELECT completed");
	list.emplace_back("a2 OK LIST completed");
	list.emplace_back("a10 BAD Command Argument Error. 12");

	for (const auto& source : list)
	{
		std::cout << source << std::endl;

		bool r = readstatusparser_parse(
			source,
			[](const readstatusparser::TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
			{
				int test = 123;
			});

		assert(r);
	}
}

int main()
{

	//TestImapList();
	//TestImapStatus();

	//std::string _reply = "* 19 FETCH (BODY[1] {9402}\r\n0L/RgNCw0LLQu9C10L3QviDRgSBpUGhvbmUNCg==\r\nFLAGS (\\Seen) UID 268229)\r\n* 20 EXISTS\r\n* 0 RECENT";

	//std::string sSource, header;
	//int pos = _reply.find("\r\n");
	//if (pos >= 0)
	//{
	//	header = _reply.substr(0, pos);
	//	sSource = _reply.substr(pos + 2, _reply.size() - (pos + 2));

	//	pos = sSource.rfind(")");
	//	
	//	sSource = sSource.substr(0, pos - 1);

	//	//if (sSource[sSource.size() - 1] == ')')
	//	//	sSource.erase(sSource.size() - 1);

	//	pos = sSource.rfind("\r\n");

	//	if (pos == -1)
	//		pos = 0;

	//	int pos1 = sSource.find("FLAGS", pos);
	//	if (pos1 >= 0)
	//		sSource = sSource.substr(0, pos1 - 1); //including space

	//	int pos2 = sSource.find("UID", pos);
	//	if (pos2 >= 0)
	//		sSource = sSource.substr(0, pos2 - 1); //including space

	//}

	std::vector <std::string> tests;

	//tests.emplace_back("* 1 FETCH (FLAGS (\\Unseen\\Unanswered) INTERNALDATE \"Wed, 17-Feb-2016 12:22 : 54 +0300\" RFC822.SIZE 161099 ENVELOPE (\"Wed, 17 Feb 2016 13:17:59 +0300\" \"=?utf-8?B?0JTQvtCx0YDQviDQv9C+0LbQsNC70L7QstCw0YLRjCDQsiBNYWlsLlJ1?=\" ((\"=?utf-8?B?0JrQvtC80LDQvdC00LAgTWFpbC5ydQ==?=\" NIL \"welcome\" \"corp.mail.ru\")) NIL NIL ((NIL NIL \"seemslikebox\" \"mail.ru\")) NIL NIL NIL NIL) BODY ( (\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"8bit\" 24695 0) ( (\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"8bit\" 24695 0) (\"image\" \"png\" (\"name\" \"wlm_sep1.png\") \"part1.01060408.00060705@mail.ru\" NIL \"base64\" 414) \"related\") \"alternative\"))");
	//tests.emplace_back("* 4 FETCH (FLAGS (\\Unseen) INTERNALDATE \"17-Feb-2016 12:22:54 +0000\" RFC822.SIZE 14173 ENVELOPE (\"Wed, 17 Feb 2016 15:22:48 +0300\" \"attachment test\" ((NIL NIL \"aalekseev\" \"forte-it.ru\")) NIL NIL ((NIL NIL \"seemslikebox\" \"mail.ru\")) NIL NIL NIL \"<427331455711768@web19j.yandex.ru>\") BODY ((\"text\" \"html\" (\"charset\" \"ascii\") NIL NIL \"8bit\" 23 0)(\"image\" \"png\" (\"name\" \"advanced.png\") NIL NIL \"base64\" 11826) \"mixed\"))");
	//tests.emplace_back("* 4 FETCH (FLAGS () INTERNALDATE \"30-Jul-2015 15:03:35 +0300\" RFC822.SIZE 9654 ENVELOPE (\"Thu, 30 Jul 2015 15:03:34 +0300\" \"1\" ((\"ConferenceRoom2\" NIL \"ConferenceRoom2\" \"vimpel.tst\")) NIL NIL ((\"User_MSK1\" NIL \"User_MSK1\" \"vimpel.tst\")) NIL NIL NIL \"<688f2f7556c44ca49c9afa946b26c85a@MS-EXMBXTST001.bee.vimpel.tst>\") BODY ((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 0 0)(\"text\" \"calendar\" (\"charset\" \"utf-8\" \"method\" \"REQUEST\") NIL NIL \"base64\" 1786 23) \"alternative\"))");
	//tests.emplace_back("* 7 FETCH (FLAGS () INTERNALDATE \"03-Sep-2015 14:41:42 +0300\" RFC822.SIZE 9991 ENVELOPE (\"Thu, 3 Sep 2015 14:41:41 +0300\" \"=?utf-8?B?0J7RgtC60LvQvtC90LXQvdC+OiBzZGZzZWZ3ZWZ3ZndlZndmd2Y=?=\" ((\"ConferenceRoom3\" NIL \"ConferenceRoom3\" \"vimpel.tst\")) NIL NIL ((\"User_MSK1\" NIL \"User_MSK1\" \"vimpel.tst\")) NIL NIL NIL \"<d1e23351dc3b47dc9b9054b22a397fb6@MS-EXMBXTST001.bee.vimpel.tst>\") BODY ((\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 1762 23)(\"text\" \"calendar\" (\"charset\" \"utf-8\" \"method\" \"REPLY\") NIL NIL \"base64\" 2658 35) \"alternative\"))");
	//tests.emplace_back("* 9 FETCH (FLAGS () INTERNALDATE \"16-Mar-2016 12:49:36 +0300\" RFC822.SIZE 6155 ENVELOPE (\"Wed,16 Mar 2016 12:49:47 +0300\" \"Test message\" ((NIL NIL \"User_MSK1\" \"vimpel.tst\")) NIL NIL ((NIL NIL \"User_MSK1\" \"vimpel.tst\")) NIL NIL NIL \"<CB7614007ADB4434AE06D20DC7EAE439@bee.vimpelcom.ru>\") BODY (\"text\" \"plain\" (\"format\" \"flowed\" \"charset\" \"koi8-r\" \"reply-type\" \"original\") NIL NIL \"7bit\" 49 1))");
	//tests.emplace_back("* 1 FETCH (FLAGS () INTERNALDATE \"24-Jul-2015 11:31:04 +0300\" RFC822.SIZE 11319 ENVELOPE (\"Fri, 24 Jul 2015 11:31:02 +0300\" \"=?utf-8?B?0J7RgtC80LXQvdC10L3Qvjog0YHQvtCx0YvRgtGM0LU=?=\" ((\"=?utf-8?B?0JDQsdGA0LDQvNC+0LIg0J/QsNCy0LXQuyDQkNC90LDRgtC+0LvRjNC10LI=?= =?utf-8?B?0LjRhw==?=\" NIL \"PAbramov\" \"vimpel.tst\")) NIL NIL ((\"User_MSK2\" NIL \"User_MSK2\" \"vimpel.tst\") (\"User_MSK1\" NIL \"User_MSK1\" \"vimpel.tst\")) NIL NIL NIL \"<e96c624d096749009c9070524a99ad00@MS-EXMBXTST001.bee.vimpel.tst>\") BODY ((\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 486 7)(\"text\" \"calendar\" (\"charset\" \"utf-8\" \"method\" \"CANCEL\") NIL NIL \"base64\" 1882 25) \"alternative\"))");
	//tests.emplace_back("* 3 FETCH (ENVELOPE (\"Tue, 22 Mar 2016 11:23:36 +0300\" \"=?UTF-8?B?dGVzdA==?=\" ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((NIL NIL \"testimap\" \"beeline.ru\")) NIL NIL NIL \"<1458635016.966584973@f397.i.mail.ru>\") INTERNALDATE \"22-Mar-2016 08:23:37 +0000\" RFC822.SIZE 4553 BODY ((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base65\" 74 2)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 194 4) \"alternative\") FLAGS ())");
	//tests.emplace_back("* 3 FETCH (ENVELOPE (\"Tue, 22 Mar 2016 11:23:36 +0300\" \"=?UTF-8?B?dGVzdA==?=\" ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((NIL NIL \"testimap\" \"beeline.ru\")) NIL NIL NIL \"<1458635016.966584973@f397.i.mail.ru>\") INTERNALDATE \"22-Mar-2016 08:23:37 +0000\" RFC822.SIZE 4553 BODY (\"text\" \"plain\"  NIL NIL NIL \"7BIT\" 5 1) FLAGS ())");
	//tests.emplace_back("* 39 FETCH (ENVELOPE (\"Wed, 27 Apr 2016 14:26:55 +0000\" \"1234\" ((\"Vyacheslav V Dyumaev\" NIL \"VDyumaev\" \"beeline.ru\")) ((\"Vyacheslav V Dyumaev\" NIL \"VDyumaev\" \"beeline.ru\")) ((\"Vyacheslav V Dyumaev\" NIL \"VDyumaev\" \"beeline.ru\")) ((\"testimapuatmobile@beeline.ru\" NIL \"testimapuatmobile\" \"beeline.ru\")) NIL NIL NIL \"<4f49812737434e9a8effd846fefa1cff@DR-MBX007.bee.vimpelcom.ru>\") INTERNALDATE \"27-Apr-2016 14:26:58 +0000\" RFC822.SIZE 271448 BODY (((\"text\" \"plain\"  (\"charset\" \"koi8-r\") NIL NIL \"quoted-printable\" 6 1)(\"text\" \"html\"  (\"charset\" \"koi8-r\") NIL NIL \"quoted-printable\" 1764 30) \"alternative\")(\"application\" \"vnd.openxmlformats-officedocument.spreadsheetml.sheet\"  (\"name\" \"BUG_10067058_-transactions.xlsx\") NIL \"BUG_10067058_-transactions.xlsx\" \"base64\" 40326)(\"text\" \"plain\"  (\"name\" \"clarify.exe_16332.txt\") NIL \"clarify.exe_16332.txt\" \"base64\" 22678 378)(\"text\" \"plain\"  (\"name\" \"clarify.exe_44788.txt\") NIL \"clarify.exe_44788.txt\" \"base64\" 15778 263)(\"application\" \"msword\"  (\"name\" \"OCM.doc\") NIL \"OCM.doc\" \"base64\" 112806)(\"application\" \"msword\"  (\"name\" \"=?koi8-r?B?9NLBztrBy8PJ0SBDQUNfQ1NDLmRvYw==?=\") NIL \"=?koi8-r?B?9NLBztrBy8PJ0SBDQUNfQ1NDLmRvYw==?=\" \"base64\" 65862)(\"application\" \"msonenote\"  (\"name\" \"=?koi8-r?B?+sHNxdTLySDOwSDQz8zRyC5vbmU=?=\") NIL \"=?koi8-r?B?+sHNxdTLySDOwSDQz8zRyC5vbmU=?=\" \"base64\" 7218) \"mixed\") FLAGS ())");


	////tests.emplace_back("* 97 FETCH (ENVELOPE (\"Sat, 30 Apr 2016 19:53:41 +0000\" \"vl\" ((\"APerevalov@bellintegrator.ru\" NIL \"APerevalov\" \"bellintegrator.ru\")) ((\"APerevalov@bellintegrator.ru\" NIL \"APerevalov\" \"bellintegrator.ru\")) ((\"APerevalov@bellintegrator.ru\" NIL \"APerevalov\" \"bellintegrator.ru\")) ((\"testimapuatbb@beeline.ru\" NIL \"testimapuatbb\" \"beeline.ru\")) NIL NIL NIL \"<c3905f918f1744e381eb3e76b587cb36@msk-ex03.bell-main.bellintegrator.ru>\") INTERNALDATE \"30-Apr-2016 19:53:51 +0000\" RFC822.SIZE 4996611 BODY ((((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 296 5)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 9214 154) \"alternative\")(\"image\" \"gif\"  (\"name\" \"image001.gif\") \"<image001.gif@01D1A333.2186D880>\" \"image001.gif\" \"base64\" 4062) \"related\")(\"image\" \"tiff\"  (\"name\" \"+pl,okmijnuhb.tif\") NIL \"+pl,okmijnuhb.tif\" \"base64\" 189138)(\"image\" \"jpeg\"  (\"name\" \"1.jpg\") NIL \"1.jpg\" \"base64\" 48070)(\"image\" \"bmp\"  (\"name\" \"1234567890+.bmp\") NIL \"1234567890+.bmp\" \"base64\" 4191286)(\"image\" \"png\"  (\"name\" \"QWERTYUIOPASDFGHJKLZXCVBNM.png\") NIL \"QWERTYUIOPASDFGHJKLZXCVBNM.png\" \"base64\" 76810)(\"image\" \"gif\"  (\"name\" \"=?koi8-r?B?enhjdmJubS3G2dfB0NLPzMTW3C5naWY=?=\") NIL \"=?koi8-r?B?enhjdmJubS3G2dfB0NLPzMTW3C5naWY=?=\" \"base64\" 170362)(\"image\" \"jpeg\"  (\"name\" \"=?koi8-r?B?ysPVy8XOx9vd2sjfxtnXwdDSz8zE1tzR3tPNydTYwsAuanBn?=\") NIL {61}\r\n=?koi8-r?B?ysPVy8XOx9vd2sjfxtnXwdDSz8zE1tzR3tPNydTYwsAuanBn?= \"base64\" 299660) \"mixed\") FLAGS ())");
	//
	////tests.emplace_back("* 120 FETCH (ENVELOPE (\"Tue, 3 May 2016 07:19:09 +0000 (UTC)\" \"yahoo\" ((NIL NIL \"fortestbee2016\" \"yahoo.com\")) ((NIL NIL \"fortestbee2016\" \"yahoo.com\")) ((NIL NIL \"fortestbee2016\" \"yahoo.com\")) ((\"testimapuatbb@beeline.ru\" NIL \"testimapuatbb\" \"beeline.ru\")) NIL NIL NIL \"<2046206903.9927388.1462259949109.JavaMail.yahoo@mail.yahoo.com>\") INTERNALDATE \"03-May-2016 07:22:01 +0000\" RFC822.SIZE 40793 BODY (((\"text\" \"plain\"  (\"charset\" \"UTF-8\") NIL NIL \"base64\" 8952 150)(\"text\" \"html\"  (\"charset\" \"UTF-8\") NIL NIL \"quoted-printable\" 25600 427) \"alternative\")(\"text\" \"plain\"  NIL \"<a5dd9c59-1bb0-6578-3267-2e9caa694c55@yahoo.com>\" NIL \"base64\" 46 1) \"mixed\") FLAGS ())");

	////tests.emplace_back("* 516 FETCH (ENVELOPE (\"Fri, 06 May 2016 11:52:48 +0300\" \"=?UTF-8?B?dGVzdCBpbWFnZXM6IGpwZWcsIGdpZiwgcG5n?=\" ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((NIL NIL \"testimap\" \"beeline.ru\")) NIL NIL NIL \"<1462524768.224805534@f341.i.mail.ru>\") INTERNALDATE \"06-May-2016 08:52:52 +0000\"RFC822.SIZE 1994919 BODY (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 50 1)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 108 2) \"alternative\")(\"image\" \"jpeg\"  (\"name\" \"=?UTF-8?B?MTUzOTc4NDE4OC5qcGc=?=\") NIL NIL \"base64\" 47910)(\"image\" \"jpeg\"  (\"name\" \"=?UTF-8?B?MTg5NDgzNzU1NC5qcGc=?=\") NIL NIL \"base64\" 63632)(\"image\" \"gif\"  (\"name\" \"=?UTF-8?B?UHV0aW4tQmFsbG9vbi1BbmltYWwuZ2lm?=\") NIL NIL \"base64\" 1779452)(\"image\" \"png\"  (\"name\" \"=?UTF-8?B?MzAgU2lyIE1vaGFtZWQgTWFjYW4gTWFya2FyIE1hd2F0aGEsIENvbG9tYm8g?= =?UTF-8?B?U3JpIExhbmthIChMS0EpLnBuZw==?=\") NIL NIL \"base64\" 98712) \"mixed\") FLAGS ())");

	//tests.emplace_back("* 317 FETCH (ENVELOPE (\"Thu, 19 May 2016 12:52:15 +0000\" \"RE: s vneghei mashini\" ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"testimapuatmobile@beeline.ru\" NIL \"testimapuatmobile\" \"beeline.ru\")) NIL NIL NIL \"<a6ec430438c046f58f9d8e9d353ea779@msk-ex03.bell-main.bellintegrator.ru>\") INTERNALDATE \"19-May-2016 12:52:21 +0000\" RFC822.SIZE 245786 BODY (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 30606 511)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 76420 1274) \"alternative\")(\"image\" \"png\"  (\"name\" \"=?koi8-r?B?yc7Gz8PFztTSLlBORw==?=\") NIL \"=?koi8-r?B?yc7Gz8PFztTSLlBORw==?=\" \"base64\" 28286)(\"message\" \"rfc822\"  (\"name\" \"RE s vneghei mashini\") NIL \"RE s vneghei mashini\" \"7BIT\" 104882 (\"Thu, 19 May 2016 12:31:01 +0000\" \"RE: s vneghei mashini\" ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\")) ((\"testimapuatmobile@beeline.ru\" NIL \"testimapuatmobile\" \"beeline.ru\")) NIL NIL NIL NIL) ((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 30048 501)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 73764 1230) \"alternative\") 1749) \"mixed\") FLAGS ())");
	//tests.emplace_back("* 23 FETCH (FLAGS (\\Unseen) INTERNALDATE \"23-May-2016 10:13:10 +0000\" RFC822.SIZE 74160 ENVELOPE (\"Mon, 23 May 2016 13:13:08 +0300\" \"=?utf-8?B?0JLQu9C+0LbQtdC90L3QvtC1INC/0LjRgdGM0LzQvg==?=\" ((\"Konstantin Musalov\" NIL \"kmusalov\" \"expertsolutions.ru\")) NIL NIL ((\"\" NIL \"seemslikebox\" \"mail.ru\")) NIL NIL NIL \"<004401d1b4db$b61e6da0$225b48e0$@expertsolutions.ru>\") BODY (((\"text\" \"plain\" (\"charset\" \"koi8-r\") NIL NIL \"base64\" 72 0)(\"text\" \"html\" (\"charset\" \"koi8-r\") NIL NIL \"quoted-printable\" 1999 0) \"alternative\")(\"message\" \"rfc822\" () NIL NIL \"7bit\" 69542) \"mixed\"))");

	//tests.emplace_back("* 481 FETCH (ENVELOPE (\"Wed, 25 May 2016 09:34:01 +0300\" {153}\n DELIVERY FAILURE: Error transferring to MX-VS2.VIMPELCOM.RU; SMTP Protocol Returned a Permanent Error 552 Message size exceeds fixed maximum message size \n ((NIL NIL \"Postmaster\" \"beeline.ru\")) ((NIL NIL \"Postmaster\" \"beeline.ru\")) ((NIL NIL \"Postmaster\" \"beeline.ru\")) ((NIL NIL \"testimapuatbb\" \"beeline.ru\")) NIL NIL NIL  \"<OF1C6FB016.3A84F7F5-ON43257FBE.002412E8-43257FBE.002415E1@LocalDomain>\") INTERNALDATE \"25-May-2016 09:27:21 +0000\" RFC822.SIZE 16817688 BODY ((\"text\" \"plain\"   (\"charset\" \"KOI8-R\") NIL NIL \"base64\" 3972 67)(\"application\" \"octet-stream\"  (\"name\" \"C.DTF\") NIL NIL \"base64\" 462)(\"application\" \"octet-stream\"  (\"name\" \"=?KOI8-R?B?7s/XwdEg0MHQy8EucmFy?=\") NIL NIL \"base64\" 16811482) \"mixed\") FLAGS ())");

	//for each(const std::string& str1 in tests)
	//{
	//	std::string error;

	//	bool ok = fetchparser::parse(str1.begin(), str1.end(),
	//		[](const fetchparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
	//		{
	//		},
	//		error);

	//	if (ok)
	//	{
	//		std::cout << "ok " /*<< str1.c_str()*/ <<  std::endl;
	//	}
	//	else
	//	{
	//		std::cout << "fail: " << error.c_str() << std::endl;
	//	}
	//}


	//tests.clear();

	//tests.emplace_back("=?UTF-8?B?MTUzOTc4NDE4OC5qcGc=?=");
	////tests.emplace_back("=?UTF-8?B?MzAgU2lyIE1vaGFtZWQgTWFjYW4gTWFya2FyIE1hd2F0aGEsIENvbG9tYm8g?= =?UTF-8?B?U3JpIExhbmthIChMS0EpLnBuZw==?=");

	//tests.emplace_back("=?UTF-8?Q?0JLQvtC/0YDQvtGB0Ysg0L/QviDQvNGD?= =?UTF-8?b?0LvRjNGC0LjQvNC10LTQuNCwLnR4dA==?=");
	//tests.emplace_back("=?UTF-8?q?0JzQvtC00LXQu9GMINC00LDQvdC90YvRhSBBQ1JNLmpwZw==?=");
	//tests.emplace_back(" =?utf-8?B?0KTRg9C90LrRhtC40L7QvdCw0Lsg0LfQvdCw0LrQvtGB0YfQuNGC0LDQu9C60Lggb25saW5lOiDQn9C+?= =?utf-8?B?0LTRgdGH0LXRgiDRgdC70L7QsiDQsiDRgtC10LrRgdGC0LUsINC/0YDQvtCy0LXRgNC60LAg0L7RgNGE?= =?utf-8?B?0L7Qs9GA0LDRhNC40Lgg0L7QvdC70LDQudC9LCDRg9C00LDQu9C10L3QuNC1INGB0YLQvtC/LdGB0Ls=?= =?utf-8?B?0L7QsiDQvdCwINGA0YPRgdGB0LrQvtC8INC4INCw0L3Qs9C70LjQudGB0LrQvtC8INGP0LfRi9C60LUs?= =?utf-8?B?INGD0LTQsNC70LXQvdC40LUg0LTRg9Cx0LvQtdC5LCDRgtGN0LPQvtCyLCDQstC+0LfQvNC+0LbQvdC+?= =?utf-8?B?0YHRgtGMINC+0LHRgNC10LfQsNGC0Ywg0YLQtdC60YHRgiDQtNC+INC+0L/RgNC10LTQtdC70LXQvdC9?= =?utf-8?B?0L7Qs9C+INC60L7Qu9C40YfQtdGB0YLQstCwINGB0LjQvNCy0L7Qu9C+0LIsINC/0YDQtdC+0LHRgNCw?= =?utf-8?B?0LfQvtCy0LDRgtGMINGC0LXQutGB0YIg0LIg0LrQsNGA0YLQuNC90LrRgyDRgSDQstC+0LTRj9C90Ys=?= =?utf-8?B?0Lwg0LfQvdCw0LrQvtC8Lg==?= ");
	//tests.emplace_back("‹ =?UTF-8?B?0L/Rg9GL0LrQv9C60L/Rg9C/0YjRg9C60L/Qs9GI0YM=?= =?UTF-8?B?0YDQutC/0YjRgNGD0L/RgNGE0YPQutC/0YDRg9C60L8=?= =?UTF-8?B?0YjRg9C60YjQv9GE0YPRiNC60L/RgNGD0LrQv9GA0YM=?= =?UTF-8?B?0L/RgNC60L/RgNC60YDRg9C/0YnRiNC60L/RgNC60YM=?= =?UTF-8?B?0YjQv9GA0YnRiNC60L/RgNGJ0YjQutC/0YDQv9GD0Ys=?= =?UTF-8?B?0LrQv9C60L/Rg9C/0YjRg9C60L/Qs9GI0YPRgNC60L8=?= =?UTF-8?B?0YjRgNGD0L/RgNGE0YPQutC/0YDRg9C60L/RiNGD0Lo=?= =?UTF-8?B?0YjQv9GE0YPRiNC60L/RgNGD0LrQv9GA0YPQv9GA0Lo=?= =?UTF-8?B?0L/RgNC60YDRg9C/0YnRiNC60L/RgNC60YPRiNC/0YA=?= =?UTF-8?B?0YnRiNC60L/RgNGJ0YjQutC/0YDQv9GD0YvQutC/0Lo=?= =?UTF-8?B?0L/Rg9C/0YjRg9C60L/Qs9GI0YPRgNC60L/RiNGA0YM=?= =?UTF-8?B?0L/RgNGE0YPQutC/0YDRg9C60LzQvNC80LzQvNC80L8=?= =?UTF-8?B?0YjRg9C60YjQv9GE0YPQvNC80LzQvNC80YjQutC/0YA=?= =?UTF-8?B?0YPQutC/0YDRg9C/0YDQutC/0YDQutGA0YPQv9GJ0Yg=?= =?UTF-8?B?0LrQv9GA0LrRg9GI0L/RgNGJ0YjQutC/0YDRidGI0Lo=?= =?UTF-8?B?0L/RgNC80LzQvNC80LzQvNC80LzQvDI1NNGCZ2c=?= ");
	//tests.emplace_back("=?UTF-8?B?0L/RgNC+0LLQtdGA0LrQsA==?=");
	//tests.emplace_back("RE: =?utf-8?Q?=D0=9F=D0=BE_=D0=92=D0=B0=D1=88=D0=B5=D0=BC=D1=83_=D0=B4=D0=BE=D0=B3=D0=BE=D0=B2=D0=BE=D1=80=D1=83_?=430466230 =?utf-8?Q?=D0=B5=D1=81=D1=82=D1=8C_=D0=B7=D0=B0=D0=B4=D0=BE=D0=BB=D0=B6=D0=B5=D0=BD=D0=BD=D0=BE=D1=81=D1=82=D1=8C?= ");
	for each(const std::string& str1 in tests)
	{
		std::wstring tempSubject;
		std::string sError;

		int pos = str1.find("=?");
		std::string tmp = str1.substr(pos, str1.size() - pos);


		auto lambda = [&tempSubject](const imapsubjectparser::TSubject& subject, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			std::string encoding = "base64";
			if (std::toupper(subject._coding[0]) == 'Q')
				encoding = "quoted-printable";
		};

		bool ok = imapsubjectparser::parse(
			tmp.begin(),
			tmp.end(),
			lambda,
			sError);

		if (ok)
		{
			std::cout << "ok " /*<< str1.c_str()*/ << std::endl;
		}
		else
		{
			std::cout << "fail: " << sError.c_str() << std::endl;
		}

	}


	//tests.clear();

	//tests.emplace_back("* 458 FETCH (BODYSTRUCTURE (((\"text\" \"plain\"  (\"charset\" \"UTF-8\") NIL NIL \"base64\" 344 6 NIL NIL NIL)(\"text\" \"html\"  (\"charset\" \"UTF-8\") NIL NIL \"quoted-printable\" 2712 46 NIL NIL NIL) \"alternative\" (\"boundary\" \"----=_Part_3029434_2052529119.1464095360489\") NIL NIL)(\"application\" \"vnd.openxmlformats-officedocument.wordprocessingml.document\"  NIL \"<2fd51af5-b9a8-90a5-e427-bff73f8723c9@yahoo.com>\" NIL \"base64\" 595540 NIL (\"attachment\" (\"filename\" \"E_mail.docx\")) NIL)(\"text\" \"plain\"  NIL \"<38413fab-10fb-753f-35bc-98174527e855@yahoo.com>\" NIL \"base64\" 54 1 NIL (\"attachment\" (\"filename\" \"E_mail.txt\")) NIL) \"mixed\" (\"boundary\" \"----=_Part_3029435_1918820444.1464095360491\") NIL NIL))");
	//tests.emplace_back("* 17 FETCH (BODYSTRUCTURE (((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 48 0 NIL NIL NIL NIL)(\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 102 0 NIL NIL NIL NIL) \"alternative\" (\"boundary\" NIL))(\"application\" \"x-rar-compressed\" (\"name\" \"geo.rar\") NIL NIL \"base64\" 39254 NIL (\"attachment\" (\"filename\" \"geo.rar\")) NIL NIL) \"mixed\" (\"boundary\" NIL)))");
	//tests.emplace_back("* 22 FETCH (BODYSTRUCTURE ((\"text\" \"plain\" (\"charset\" \"koi8-r\") NIL NIL \"base64\" 454 0 NIL NIL NIL NIL)(\"message\" \"delivery-status\" () NIL NIL \"7bit\" 715 NIL NIL NIL NIL)(\"message\" \"rfc822\" () NIL NIL \"7bit\" 811 NIL NIL NIL NIL) \"report\" (\"boundary\" NIL)))");
	//tests.emplace_back("* 23 FETCH (BODYSTRUCTURE (((\"text\" \"plain\" (\"charset\" \"koi8-r\") NIL NIL \"base64\" 72 0 NIL NIL NIL NIL)(\"text\" \"html\" (\"charset\" \"koi8-r\") NIL NIL \"quoted-printable\" 1999 0 NIL NIL NIL NIL) \"alternative\" (\"boundary\" NIL))(\"message\" \"rfc822\" () NIL NIL \"7bit\" 69542 NIL (\"attachment\" NIL) NIL NIL) \"mixed\" (\"boundary\" NIL)))");
	//tests.emplace_back("* 324 FETCH (BODYSTRUCTURE  (  (   (\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 31714 529 NIL NIL NIL)   (\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 81828 1364 NIL NIL NIL)    \"alternative\" (\"boundary\" \"_000_de043537cfa64fbf80d420db7d2328a1mskex03bellmainbellinte_\") NIL NIL)   (\"message\" \"rfc822\"  (\"name\" \"FW s vneghei mashini\") NIL \"FW s vneghei mashini\" \"7BIT\" 245522     (\"Thu, 19 May 2016 13:09:20 +0000\" \"FW: svneghei mashini\"      ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))      ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))      ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))      ((\"testimapuatmobile@beeline.ru\" NIL \"testimapuatmobile\" \"beeline.ru\"))      NIL NIL NIL NIL    )     (     (      (\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 31160 520 NIL NIL NIL)      (\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 79088 1319 NIL NIL NIL)       \"alternative\" (\"boundary\" \"_000_7174798071787278767676676871696973717180797378697967757_\") NIL NIL)      (\"image\" \"png\"  (\"name\" \"=?koi8-r?B?yc7Gz8PFztTSLlBORw==?=\") NIL \"=?koi8-r?B?yc7Gz8PFztTSLlBORw==?=\" \"base64\" 28286 NIL (\"attachment\" (\"filename\" \"=?koi8-r?B?yc7Gz8PFztTSLlBORw==?=\")) NIL)      (\"message\" \"rfc822\"  (\"name\" \"RE s vneghei mashini\") NIL \"RE s vneghei mashini\" \"7BIT\" 104882        (\"Thu, 19 May 2016 12:31:01 +0000\" \"RE: s vneghei mashini\"        ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))        ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))        ((\"oantonova@bellintegrator.ru\" NIL \"oantonova\" \"bellintegrator.ru\"))        ((\"testimapuatmobile@beeline.ru\" NIL \"testimapuatmobile\" \"beeline.ru\")) NIL NIL NIL NIL      )       (       (\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 30048 501 NIL NIL NIL)       (\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 73764 1230 NIL NIL NIL)        \"alternative\" (\"boundary\" \"_000_7468737277738071656867686966697875776972797371696877697_\") NIL (\"ru-RU\"))        1749 NIL (\"attachment\" (\"filename\" \"RE s vneghei mashini\")) NIL)        \"mixed\" (\"boundary\" \"_005_7174798071787278767676676871696973717180797378697967757_\") NIL (\"ru-RU\"))        4093 NIL (\"attachment\" (\"filename\" \"FW s vneghei mashini\")) NIL)        \"mixed\" (\"boundary\" \"_004_de043537cfa64fbf80d420db7d2328a1mskex03bellmainbellinte_\") NIL (\"ru-RU\")))");
	//tests.emplace_back("* 31 FETCH (BODYSTRUCTURE  (  (\"text\" \"plain\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 501 9 NIL NIL NIL)  (\"text\" \"html\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 2481 42 NIL NIL NIL)   \"alternative\" (\"boundary\" \"_000_12e441afbab24337b573ee3f6db8cdd1msmbx006beevimpelcomru_\") NIL (\"ru-RU\") ))");
	//tests.emplace_back("* 534 FETCH (BODYSTRUCTURE ((((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 506 9 NIL NIL NIL)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 8096 135 NIL NIL NIL) \"alternative\" (\"boundary\" \"_000_1d364cbfd27742cea7e8a07f6ea1e233mskex03bellmainbellinte_\") NIL NIL)(\"image\" \"gif\"  (\"name\" \"image001.gif\") \"<image001.gif@01D1BCE5.2438A600>\" \"image001.gif\" \"base64\" 3966 NIL (\"inline\" (\"filename\" \"image001.gif\")) NIL) \"related\" (\"boundary\" \"_005_1d364cbfd27742cea7e8a07f6ea1e233mskex03bellmainbellinte_\" \"type\" \"multipart/alternative\") NIL NIL)(\"application\" \"x-zip-compressed\"  (\"name\" \"=?koi8-r?B?xMzRINTF09TBLnppcA==?=\") NIL \"=?koi8-r?B?xMzRINTF09TBLnppcA==?=\" \"base64\" 15432580 NIL (\"attachment\" (\"filename\" \"=?koi8-r?B?xMzRINTF09TBLnppcA==?=\")) NIL) \"mixed\" (\"boundary\" \"_006_1d364cbfd27742cea7e8a07f6ea1e233mskex03bellmainbellinte_\") NIL (\"ru-RU\")))");
	//tests.emplace_back("* 604 FETCH (BODYSTRUCTURE (\"text\" \"plain\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 92751 1546 NIL NIL (\"en-US\")))");
	//tests.emplace_back("* 1542 FETCH (BODYSTRUCTURE (\"TEXT\" \"PLAIN\"  (\"charset\" \"KOI8-R\") NIL NIL \"BASE64\" 1174 17 NIL NIL NIL))");
	//tests.emplace_back("* 1661 FETCH (BODYSTRUCTURE ((\"text\" \"plain\"  (\"charset\" \"KOI8-R\") NIL NIL \"base64\" 3550 60 NIL NIL NIL)(\"application\" \"octet-stream\"  (\"name\" \"C.DTF\") NIL NIL \"base64\" 570 NIL (\"attachment\" (\"filename\" \"C.DTF\")) NIL)(\"application\" \"octet-stream\"  (\"name\" \"msg.eml\") NIL NIL \"base64\" 5882 NIL (\"attachment\" (\"filename\" \"msg.eml\")) NIL) \"mixed\" (\"Boundary\" \"0__=CCBBF52DDFABC1AC8f9e8a93df938690918cCCBBF52DDFABC1AC\") (\"inline\" NIL) NIL))");

	//tests.emplace_back("* 1438 FETCH (BODYSTRUCTURE ( \"alternative\" (\"boundary\" \"ixxoz----UJHaAncYMSWRKeYH-ueoCiILHRdrveeJAIbgLcKcnKb:892222649694878\") NIL NIL))");
	////tests.emplace_back("* 1445 FETCH (BODYSTRUCTURE ((\"text\" \"plain\"  (\"charset\" \"utf-8\" \"format\" \"flowed\") NIL NIL \"quoted-printable\" 2552 43 NIL NIL NIL)((\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"quoted-printable\" 16397 274 NIL NIL NIL)(\"image\" \"png\"  (\"name\" \" ↕  ↓↕   2☻\") \"<part1.08000508.02070403@avenir-s.ru>\" NIL \"base64\" 41706 NIL (\"inline\" NIL) NIL) \"related\" (\"boundary\" \"------------080907090301010009000701\") NIL NIL) \"alternative\"(\"boundary\" \"------------030006020108020704020008\") NIL NIL))");

	////tests.emplace_back("* 1445 FETCH (BODYSTRUCTURE ((\"text\" \"plain\"  (\"charset\" \"utf-8\" \"format\" \"flowed\") NIL NIL \"quoted-printable\" 2552 43 NIL NIL NIL)((\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"quoted-printable\" 16397 274 NIL NIL NIL)(\"image\" \"png\"  (\"name\" \" ↕  ↓↕   2☻\") \"<part1.08000508.02070403@avenir-s.ru>\" NIL \"base64\" 41706 NIL (\"inline\" NIL) NIL) \"related\" (\"boundary\" \"------------080907090301010009000701\") NIL NIL) \"alternative\" (\"boundary\" \"------------030006020108020704020008\") NIL NIL))");
	//tests.emplace_back("* 22 FETCH (UID 157 BODYSTRUCTURE ((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 68 0 NIL NIL NIL NIL)(\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 115772 0 NIL NIL NIL NIL) \"alternative\" (\"boundary\" NIL)))");
	//tests.emplace_back("* 170 FETCH (BODYSTRUCTURE (((\"Text\" \"Plain\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 1164 20 NIL NIL NIL)(\"Text\" \"HTML\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 25111 419 NIL NIL NIL) \"Alternative\" (\"boundary\" \"------------Boundary-00=_1TIZ12S0000000000000\") NIL NIL)(\"image\" \"png\"  (\"name\" \"SENDER_EMAILvladimir@@avdeenko59@yandex@@ru.png\") \"<PIC_GUID109056AE-5DE4-4EAE-91CB-6BB390A09A59>\" NIL \"base64\" 2964 NIL NIL NIL)(\"image\" \"png\"  (\"name\" \"clip_image001.png\") \"<F9CE7E43-2EAE-4F25-8988-CCF970FB5BDD>\" NIL \"base64\" 21066 NIL NIL NIL)(\"image\" \"png\"  (\"name\" \"clip_image003.png\") \"<3C94153E-4612-4FCD-B355-7F7149CEA98A>\" NIL \"base64\" 390 NIL NIL NIL)(\"image\" \"jpeg\"  (\"name\" \"608-1041a.jpg\") \"<B386D64D-8955-4E6A-9FDB-1F8ECC07E72D>\" NIL \"base64\" 14394 NIL NIL NIL)(\"image\" \"gif\"  (\"name\" \"butterfly_top.gif\") \"<096C99EB-DC4D-416F-9388-E931C362551D>\" NIL \"base64\" 17280 NIL NIL NIL)(\"image\" \"gif\"  (\"name\" \"butterfly_bottom.gif\") \"<57AAB80C-8974-4926-B982-B89A43F32072>\" NIL \"base64\" 28278 NIL NIL NIL)\"related\" (\"charset\" \"windows-1251\" \"type\" \"multipart/alternative\" \"boundary\" \"------------Boundary-00=_1TIZ6RO0000000000000\") NIL NIL) UID 8825)");

	//tests.emplace_back("* 3 FETCH (BODYSTRUCTURE ((\"text\" \"plain\"  (\"charset\" \"UTF-8\") NIL NIL \"base64\" 748 13 NIL NIL NIL)(\"message\" \"delivery-status\"  NIL NIL NIL \"7BIT\" 398 NIL NIL NIL)(\"message\" \"rfc822\"  NIL NIL NIL \"7BIT\" 5223 (\"Thu, 28 Jul 2016 12:35:27 +0300\" {332} =?UTF-8?B?w7DDrcOw4pSQw7DilpHDkMO8w7DCqcOw4paSw7DCpSDDsMOAw7DilpEgw7A=?= ((NIL NIL \"vopros\" \"ekb.beeline.ru\")) ((NIL NIL \"vopros\" \"ekb.beeline.ru\")) ((NIL NIL \"vopros\" \"ekb.beeline.ru\")) ((NIL NIL \"expo\" \"fmsender.ru\"))NIL NIL NIL \"<OF71746E81.73CF0D36-ON43257FFE.0034AF1F@beeline.ru>\") (\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"7BIT\" 856 15 NIL NIL NIL) 88 NIL NIL NIL) \"report\" (\"report-type\" \"delivery-status\" \"boundary\" \"==IFJRGLKFGIR-2147483647UHRUHIHD\") NIL NIL) UID 46904)");
	//
	//tests.emplace_back ("* 13 FETCH (BODYSTRUCTURE ((\"TEXT\" \"PLAIN\"  (\"charset\" \"UTF-8\") NIL NIL \"BASE64\" 3356 44 NIL NIL NIL)(\"APPLICATION\" \"MSWORD\"  (\"name\" \"=?UTF-8?B?0J7QntCeLmRvYw==?=\") NIL NIL \"BASE64\" 43442 NIL (\"attachment\" (\"filename\" \"=?UTF-8?B?0J7QntCeLmRvYw==?=\")) NIL) \"MIXED\" (\"Boundary\" \"0__=CCBB0A90DFB1893C8f9e8a93df938690918cCCBB0A90DFB1893C\") (\"inline\" NIL) NIL) UID 269635)");
	//tests.emplace_back("* 13 FETCH (BODYSTRUCTURE ((\"TEXT\" \"PLAIN\"  (\"charset\" \"UTF-8\") NIL NIL \"BASE64\" 3356 44 NIL NIL NIL)(\"APPLICATION\" \"MSWORD\"  (\"name\" \"=?UTF-8?B?0J7QntCeLmRvYw==?=\") NIL NIL \"BASE64\" 43442 NIL (\"attachment\" (\"filename\" \"=?UTF-8?B?0J7QntCeLmRvYw==?=\")) NIL) \"mixed\" (\"Boundary\" \"0__=CCBB0A90DFB1893C8f9e8a93df938690918cCCBB0A90DFB1893C\") (\"inline\" NIL) NIL) UID 269635)");

	//tests.emplace_back("* 12 FETCH (BODYSTRUCTURE (((\"Text\" \"Plain\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 2961 50 NIL NIL NIL)(\"Text\" \"HTML\"  (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 5374 90 NIL NIL NIL) \"Alternative\" (\"boundary\" \"------------Boundary-00=_DYR8KFN2QL8000000000\") NIL NIL)(\"unknown\" \"unknown\"  (\"name\" \"currencies.gif\") \"<416E8D6E-4070-4382-9716-55EA7C349B57>\" NIL \"base64\" 72692 NIL NIL NIL)(\"unknown\" \"unknown\"  (\"name\" \"SENDER_EMAILandreycofff@gmail@@com.png\") \"<PIC_GUID729F6772-06D9-461C-ACF0-E47A791787D6>\" NIL \"base64\" 9204 NIL NIL NIL) \"related\"(\"charset\" \"windows-1251\" \"type\" \"multipart/alternative\" \"boundary\" \"------------Boundary-00=_DYR8P4J1VA4000000000\") NIL NIL) UID 268947)");
	//tests.emplace_back("* 169 FETCH (BODYSTRUCTURE ((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"quoted-printable\" 10475 175 NIL NIL NIL)(\"application\" \"pkcs7-signature\"  (\"name\" \"smime.p7s\") NIL \"S/MIME Cryptographic Signature\" \"base64\" 6154 NIL (\"attachment\" (\"filename\" \"smime.p7s\")) NIL) \"signed\" (\"protocol\" \"application/pkcs7-signature\" \"micalg\" \"sha256\" \"boundary\" \"----------11E10119E3B58F0E2\") NIL NIL) UID 9190)");

	//tests.emplace_back("* 60 FETCH (BODYSTRUCTURE (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"quoted-printable\" 7757 130 NIL NIL NIL)(\"APPLICATION\" \"PKIX-CERT\"  (\"name\" \"account.cer\") NIL NIL \"base64\" 2140 NIL (\"attachment\" (\"filename\" \"account.cer\")) NIL) \"mixed\" (\"boundary\" \"----------10460118A14DFA2\") NIL NIL)(\"application\" \"pkcs7-signature\"  (\"name\" \"smime.p7s\") NIL \"S/MIME Cryptographic Signature\" \"base64\" 5010 NIL (\"attachment\" (\"filename\" \"smime.p7s\")) NIL) \"signed\" (\"protocol\" \"application/pkcs7-signature\" \"micalg\" \"sha1\" \"boundary\" \"----------381257A151EEF53\") NIL NIL) UID 40334)");

	//tests.emplace_back("* 10 FETCH (BODYSTRUCTURE (\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"quoted-printable\" 514 9 NIL NIL NIL) UID 272905)* 18 EXISTS* 0 RECENT");

	//tests.emplace_back("* 169 FETCH (BODYSTRUCTURE (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 2160 37 NIL NIL NIL)(\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 3428 58 NIL NIL NIL) \"alternative\" (\"boundary\" \"--ALT--dm1AO0ld1470241050\") NIL NIL)(\"image\" \"gif\" (\"name\" \"\") \"<GovNWKZ3BHR4ot7K>\" NIL \"base64\" 3018 NIL (\"inline\" (\"filename\" \"\")) NIL) \"related\" (\"boundary\" \"----dm1AO0ld-jvtzQp4sOJHz43y6-1470241050\") NIL NIL) UID 10451)");

	//tests.emplace_back("* 10 FETCH (BODYSTRUCTURE (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 78 2 NIL NIL NIL)((\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 546 10 NIL NIL NIL)(\"image\" \"jpeg\"  (\"name\" \"20160804_153216.jpg\") \"__storage_sdcard0__EmailTempImage_1_RotateImage_20160804_153216_jpg@sec.galaxytab\" NIL \"base64\" 124138 NIL (\"inline\" (\"filename\" \"20160804_153216.jpg\")) NIL) \"relative\" (\"boundary\" \"--_com.android.email_1214345537122482\") NIL NIL) \"alternative\" (\"boundary\" \"--_com.android.email_1214345304623741\") NIL NIL)(\"image\" \"jpeg\"  (\"name\" \"20160804_153216_resized.jpg\") NIL NIL \"base64\" 778608 NIL (\"attachment\" (\"filename\" \"20160804_153216_resized.jpg\")) NIL) \"mixed\" (\"boundary\" \"--_com.android.email_1214345288412070\")NIL NIL) UID 275387)");

	//tests.emplace_back("* 1 FETCH (BODYSTRUCTURE (\"text\" \"html\" (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 1347 37 NIL NIL NIL) UID 855)");
	//tests.emplace_back("* 1 FETCH (BODYSTRUCTURE (\"text\" \"html\" (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 1347 37 NIL NIL \"ru-RU\" NIL) UID 855)");

	//tests.emplace_back("* 3 FETCH (BODYSTRUCTURE ((\"text\" \"html\" (\"charset\" \"windows-1251\") NIL NIL \"quoted-printable\" 1378 37 NIL NIL NIL NIL)(\"image\" \"png\" (\"name\" \"asd.png\") NIL \"asd.png\" \"base64\" 54474 NIL (\"attachment\" (\"filename\" \"asd.png\" \"size\" \"39964\" \"creation-date\" \"Tue, 06 Oct 2020 06:35:09 GMT\" \"modification-date\" \"Tue, 06 Oct 2020 06:35:09 GMT\")) NIL NIL)(\"application\" \"octet-stream\" (\"name\" \"cti_environment_(2020-09-29)(5).log\") NIL \"cti_environment_(2020-09-29)(5).log\" \"base64\" 85702 NIL (\"attachment\" (\"filename\" \"cti_environment_(2020-09-29)(5).log\" \"size\" \"62882\" \"creation-date\" \"Tue, 06 Oct 2020 06:35:15 GMT\" \"modification-date\" \"Tue, 06 Oct 2020 06:35:15 GMT\")) NIL NIL) \"mixed\" (\"boundary\" \"_003_fcb137ec50ef491f9fc6c7061219e0b5kalugabeelineru_\") NIL \"ru-RU\") UID 859)");

	//tests.emplace_back("* 94 FETCH (BODYSTRUCTURE ((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 698 9 NIL NIL NIL NIL)((\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 5818 75 NIL NIL NIL NIL)(\"text\" \"plain\" (\"name\" NIL) \"<j8K0qlAaM5vcPl5o>\" NIL \"base64\" 148830 1909 NIL (\"inline\" (\"filename\" NIL)) NIL NIL)(\"text\" \"plain\" (\"name\" NIL) \"<H7vD5D1YRdevuVKb>\" NIL \"base64\" 64272 824 NIL (\"inline\" (\"filename\" NIL)) NIL NIL) \"related\" (\"boundary\" \"----88cBd5F5C1d5eD3121E1BbF757499eE9-LP4XQwScrmPPU4I3-1613560812\") NIL NIL) \"alternative\" (\"boundary\" \"--ALT--88cBd5F5C1d5eD3121E1BbF757499eE91613560812\") NIL NIL) UID 950)");

	//tests.emplace_back("* 27 FETCH (BODYSTRUCTURE (((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 234 3 NIL NIL NIL NIL)(\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 962 13 NIL NIL NIL NIL) \"alternative\" (\"boundary\" \"--_com.samsung.android.email_1381513126709931\") NIL NIL)(\"*\" \"*\" (\"name\" \"Doc 09.03.2021 20.05.pdf\") NIL NIL \"base64\" 6847626 NIL (\"attachment\" (\"filename\" \"Doc 09.03.2021 20.05.pdf\" \"size\" \"5004033\")) NIL NIL)(\"image\" \"jpeg\" (\"name\" \"20210309_201038.jpg\") NIL NIL \"base64\" 3638952 NIL (\"attachment\" (\"filename\" \"20210309_201038.jpg\" \"size\" \"2659231\")) NIL NIL) \"mixed\" (\"boundary\" \"--_com.samsung.android.email_1381513123416860\") NIL NIL) UID 4870)");

	//tests.emplace_back("* 27 FETCH (BODYSTRUCTURE (((\"text\" \"plain\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 234 3 NIL NIL NIL NIL)(\"text\" \"html\" (\"charset\" \"utf-8\") NIL NIL \"base64\" 962 13 NIL NIL NIL NIL) \"alternative\" (\"boundary\" \"--_com.samsung.android.email_1381513126709931\") NIL NIL)(\"*\" \"*\" (\"name\" \"Doc 09.03.2021 20.05.pdf\") NIL NIL \"base64\" 6847626 NIL (\"attachment\" (\"filename\" \"Doc 09.03.2021 20.05.pdf\" \"size\" \"5004033\")) NIL NIL)(\"image\" \"jpeg\" (\"name\" \"20210309_201038.jpg\") NIL NIL \"base64\" 3638952 NIL (\"attachment\" (\"filename\" \"20210309_201038.jpg\" \"size\" \"2659231\")) NIL NIL) \"mixed\" (\"boundary\" \"--_com.samsung.android.email_1381513123416860\") NIL NIL) UID 4870)");

	//"* 27 FETCH 
	//	(
	//		BODYSTRUCTURE 
	//		(
	//			(
	//				("text" "plain" ("charset" "utf-8") NIL NIL "base64" 234 3 NIL NIL NIL NIL)
	//				("text" "html" ("charset" "utf-8") NIL NIL "base64" 962 13 NIL NIL NIL NIL) 
	//				"alternative" ("boundary" "--_com.samsung.android.email_1381513126709931") NIL NIL
	//			)
	//			("*" "*" ("name" "Doc 09.03.2021 20.05.pdf") NIL NIL "base64" 6847626 NIL ("attachment" ("filename" "Doc 09.03.2021 20.05.pdf" "size" "5004033")) NIL NIL)
	//			("image" "jpeg" ("name" "20210309_201038.jpg") NIL NIL "base64" 3638952 NIL ("attachment" ("filename" "20210309_201038.jpg" "size" "2659231")) NIL NIL) 
	//			"mixed" ("boundary" "--_com.samsung.android.email_1381513123416860") NIL NIL
	//		) 
	//		UID 4870
	//	)"


	for each(const std::string& str1 in tests)
	{
		std::string error;

		bool ok = fetchbodystructureparser_parse(str1,
			[](const fetchbodystructureparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
		},
			error);

		if (ok)
		{
			std::cout << "ok " /*<< str1.c_str()*/ << std::endl;
		}
		else
		{
			std::cout << "fail: " << error.c_str() << std::endl;
		}
	}


	tests.clear();

	//tests.emplace_back("* 2 FETCH (ENVELOPE (\"Mon, 20 Apr 2015 08:40:56 GMT\" \"=?UTF-8?Q?=D0=91=D0=B5=D1=81=D0=B5=D0=B4=D0=B0_=D1=81_=D0=90=D0=BB=D0=B5=D0=BA=D1=81=D0=B0=D0=BD=D0=B4=D1=80=D0=BE=D0=BC_=D0=93=D1=83=D1=80=D0=BA=D0=BE=D0=B2=D1=8B=D0=BC?=\" ((\"=?UTF-8?Q?=D0=90=D0=BB=D0=B5=D0=BA=D1=81=D0=B0=D0=BD=D0=B4=D1=80_=D0=93=D1=83=D1=80=D0=BA=D0=BE=D0=B2_Alexander_Gurkov?=\" NIL \"a.s.gurkov-11bd174f1\" \"vkmessenger.com\")) ((\"=?UTF-8?Q?=D0=90=D0=BB=D0=B5=D0=BA=D1=81=D0=B0=D0=BD=D0=B4=D1=80_=D0=93=D1=83=D1=80=D0=BA=D0=BE=D0=B2_Alexander_Gurkov?=\" NIL \"a.s.gurkov-11bd174f1\" \"vkmessenger.com\")) ((\"=?UTF-8?Q?=D0=90=D0=BB=D0=B5=D0=BA=D1=81=D0=B0=D0=BD=D0=B4=D1=80_=D0=93=D1=83=D1=80=D0=BA=D0=BE=D0=B2_Alexander_Gurkov?=\" NIL \"a.s.gurkov-11bd174f1\" \"vkmessenger.com\")) ((NIL NIL \"pomogite\" \"beeline.ru\")) NIL NIL \"<526698151425620b@vk.com>\" \"<9a05007dfe13376f@vk.com>\") INTERNALDATE \"20-Apr-2015 08:41:10 +0000\" RFC822.SIZE 13154 FLAGS ())");
	//tests.emplace_back("* 3 FETCH (ENVELOPE (\"Wed, 20 May 2015 16:31:49 +0500\" \"=?UTF-8?Q?=D0=9F=D0=A0=D0=98=D0=93=D0=9B=D0=90=D0=A8=D0=90=D0=95=D0=9C=20?= =?UTF-8?Q?=D0=92=D0=90=D0=A1=20=D0=92=20=D0=9A=D0=A0=D0=AB=D0=9C=20=D0=9B?= =?UTF-8?Q?=D0=95=D0=A2=D0=9D=D0=98=D0=95=20=D0=9A=D0=90=D0=9D=D0=98=D0=9A?= =?UTF-8?Q?=D0=A3=D0=9B=D0=AB=20=20=D0=B8=20=D0=92=D0=A1=D0=95=20=D0=A7?= =?UTF-8?Q?=D0=B5=D1=80=D0=BD=D0=BE=D0=B5=20=D0=BC=D0=BE=D1=80=D0=B5=20=32?= =?UTF-8?Q?=30=31=35?=\" ((NIL NIL \"subscribe\" \"flamingo-tour.ru\")) ((NIL NIL \"subscribe\" \"flamingo-tour.ru\")) ((NIL NIL \"subscribe\"\"flamingo-tour.ru\")) ((NIL NIL NIL NIL)) NIL NIL NIL \"<03bfb093dd77beffca8d5751018d5eb9@ssl://85.12.197.61:993>\") INTERNALDATE \"20-May-2015 11:31:52 +0000\" RFC822.SIZE 621309 FLAGS ())");
	//tests.emplace_back("* 25 FETCH (ENVELOPE (\"Sat, 13 Jun 2015 23:14:45 +0000 (GMT)\" \"=?utf-8?B?0KLQsNGA0LjRhNC90YvQuSDQv9C70LDQvSA=?=\" ((\"=?utf-8?B?0JPQtdGA0LzQsNC9INCk0L7QuNC40YfQtdCy?=\" NIL \"fomichyov.german\" \"icloud.com\")) ((\"=?utf-8?B?0JPQtdGA0LzQsNC9INCk0L7QuNC40YfQtdCy?=\" NIL \"fomichyov.german\" \"icloud.com\")) ((\"=?utf-8?B?0JPQtdGA0LzQsNC9INCk0L7QuNC40YfQtdCy?=\" NIL \"fomichyov.german\" \"icloud.com\")) ((NIL NIL \"pomogite\" \"beeline.ru\")) NIL NIL NIL \"<1c211af2-18f4-499f-96d5-e1ad5cbe64d1@me.com>\") INTERNALDATE \"13-Jun-2015 23:14:53 +0000\" RFC822.SIZE 8978 FLAGS ())");
	//tests.emplace_back("* 603 FETCH (ENVELOPE (\"Sun, 28 Jun 2015 10:39:55 -0500 (EST)\" {80}pMy Complete Data From Mrs Younis for Gold And Money Investment In Your Country? ((\"younis\" NIL \"younis\" \"younniscom.ipage.com\")) ((\"younis\" NIL \"younis\" \"younniscom.ipage.com\")) ((\"younis\" NIL \"younis\" \"younniscom.ipage.com\")) ((NIL NIL NIL NIL)) NIL NIL NIL \"<1526912060.701378.1435505995831.JavaMail.open-xchange@bosoxweb04.eigbox.net>\") INTERNALDATE \"28-Jun-2015 16:13:49 +0000\" RFC822.SIZE 3466990 FLAGS ())");
	//tests.emplace_back("* 575 FETCH (ENVELOPE (\"Thu, 09 Jun 2016 17:27:20 +0300\" {780}=?utf-8?B?0KTRg9C90LrRhtC40L7QvdCw0Lsg0LfQvdCw0LrQvtGB0YfQuNGC0LDQu9C60Lggb25saW5lOiDQn9C+?= =?utf-8?B?0LTRgdGH0LXRgiDRgdC70L7QsiDQsiDRgtC10LrRgdGC0LUsINC/0YDQvtCy0LXRgNC60LAg0L7RgNGE?= =?utf-8?B?0L7Qs9GA0LDRhNC40Lgg0L7QvdC70LDQudC9LCDRg9C00LDQu9C10L3QuNC1INGB0YLQvtC/LdGB0Ls=?= =?utf-8?B?0L7QsiDQvdCwINGA0YPRgdGB0LrQvtC8INC4INCw0L3Qs9C70LjQudGB0LrQvtC8INGP0LfRi9C60LUs?= =?utf-8?B?INGD0LTQsNC70LXQvdC40LUg0LTRg9Cx0LvQtdC5LCDRgtGN0LPQvtCyLCDQstC+0LfQvNC+0LbQvdC+?= =?utf-8?B?0YHRgtGMINC+0LHRgNC10LfQsNGC0Ywg0YLQtdC60YHRgiDQtNC+INC+0L/RgNC10LTQtdC70LXQvdC9?= =?utf-8?B?0L7Qs9C+INC60L7Qu9C40YfQtdGB0YLQstCwINGB0LjQvNCy0L7Qu9C+0LIsINC/0YDQtdC+0LHRgNCw?= =?utf-8?B?0LfQvtCy0LDRgtGMINGC0LXQutGB0YIg0LIg0LrQsNGA0YLQuNC90LrRgyDRgSDQstC+0LTRj9C90Ys=?= =?utf-8?B?0Lwg0LfQvdCw0LrQvtC8Lg==?= ((\"=?utf-8?B?0JHQsNC70YvQutC+0LIg0JLQuNGC0LDQu9C40Lk=?=\" NIL \"vitaly.balykov\" \"yandex.ru\")) ((\"=?utf-8?B?0JHQsNC70YvQutC+0LIg0JLQuNGC0LDQu9C40Lk=?=\" NIL \"vitaly.balykov\" \"yandex.ru\")) ((\"=?utf-8?B?0JHQsNC70YvQutC+0LIg0JLQuNGC0LDQu9C40Lk=?=\" NIL \"vitaly.balykov\" \"yandex.ru\")) ((NIL NIL \"testimapuatbb\" \"beeline.ru\")) NIL NIL NIL \"<495261465482440@web4o.yandex.ru>\") INTERNALDATE \"09-Jun-2016 14:27:22 +0000\" RFC822.SIZE 5114 FLAGS ())");

	//tests.emplace_back("* 463 FETCH (ENVELOPE (\"Fri, 10 Jun 2016 11:15:38 +0000 (UTC)\" {907}=?UTF-8?B?0L/Rg9GL0LrQv9C60L/Rg9C/0YjRg9C60L/Qs9GI0YM=?= =?UTF-8?B?0YDQutC/0YjRgNGD0L/RgNGE0YPQutC/0YDRg9C60L8=?= =?UTF-8?B?0YjRg9C60YjQv9GE0YPRiNC60L/RgNGD0LrQv9GA0YM=?= =?UTF-8?B?0L/RgNC60L/RgNC60YDRg9C/0YnRiNC60L/RgNC60YM=?= =?UTF-8?B?0YjQv9GA0YnRiNC60L/RgNGJ0YjQutC/0YDQv9GD0Ys=?= =?UTF-8?B?0LrQv9C60L/Rg9C/0YjRg9C60L/Qs9GI0YPRgNC60L8=?= =?UTF-8?B?0YjRgNGD0L/RgNGE0YPQutC/0YDRg9C60L/RiNGD0Lo=?= =?UTF-8?B?0YjQv9GE0YPRiNC60L/RgNGD0LrQv9GA0YPQv9GA0Lo=?= =?UTF-8?B?0L/RgNC60YDRg9C/0YnRiNC60L/RgNC60YPRiNC/0YA=?= =?UTF-8?B?0YnRiNC60L/RgNGJ0YjQutC/0YDQv9GD0YvQutC/0Lo=?= =?UTF-8?B?0L/Rg9C/0YjRg9C60L/Qs9GI0YPRgNC60L/RiNGA0YM=?= =?UTF-8?B?0L/RgNGE0YPQutC/0YDRg9C60LzQvNC80LzQvNC80L8=?= =?UTF-8?B?0YjRg9C60YjQv9GE0YPQvNC80LzQvNC80YjQutC/0YA=?= =?UTF-8?B?0YPQutC/0YDRg9C/0YDQutC/0YDQutGA0YPQv9GJ0Yg=?= =?UTF-8?B?0LrQv9GA0LrRg9GI0L/RgNGJ0YjQutC/0YDRidGI0Lo=?= =?UTF-8?B?0L/RgNC80LzQvNC80LzQvNC80LzQvDI1NNGCZ2c=?= ((NIL NIL \"beemult1\" \"yahoo.com\")) ((NIL NIL \"beemult1\" \"yahoo.com\")) ((NIL NIL \"beemult1\" \"yahoo.com\")) ((\"testimapuatmobile\" NIL \"testimapuatmobile\" \"beeline.ru\")) NIL NIL NIL \"<237688110.1416297.1465557338172.JavaMail.yahoo@mail.yahoo.com>\") INTERNALDATE \"10-Jun-2016 11:18:37 +0000\" RFC822.SIZE 8225 FLAGS ())");

	////tests.emplace_back("* 612 FETCH (ENVELOPE (\"Tue, 14 Jun 2016 13:56 : 11 + 0300\" \" = ? UTF - 8 ? B ? 0L7Qv9GP0YLRjCDRgtC10YHRgiwgNSDQstC70L7QttC10L3QuNC5ISEh ? = \" ((\" = ? UTF - 8 ? B ? c2RmcyBzZGZz ? = \" NIL \"777vanhelsing777\" \"mail.ru\")) ((\" = ? UTF - 8 ? B ? c2RmcyBzZGZz ? = \" NIL \"777vanhelsing777\" \"mail.ru\")) ((\" = ? UTF - 8 ? B ? c2RmcyBzZGZz ? = \" NIL \"777vanhelsing777\" \"mail.ru\")) ((\" = ? UTF - 8 ? B ? dGVzdGltYXB1YXRiYg == ? = \" NIL \"testimapuatbb\" \"beeline.ru\")) NIL NIL NIL \"<1465901771.715370224@f411.i.mail.ru>\") INTERNALDATE \"14 - Jun - 2016 10 : 56 : 38 + 0000\" RFC822.SIZE 1645067 FLAGS (Junk))");
	//tests.emplace_back("* 1475 FETCH (ENVELOPE (\"Mon, 23 May 2016 17:07:44 +0400\" \"=?koi8-r?B?Rlc6IOvv7Ozl6/Tp9+7h8SDw8uX05e766fEg?=\" ((\"=?koi8-r?B?98/My8/Oxc7Lz9fBIO/L08HOwQ==?=\" NIL \"torg8\" \"trade-energo.com\")) ((\"=?koi8-r?B?98/My8/Oxc7Lz9fBIO/L08HOwQ==?=\" NIL \"torg8\" \"trade-energo.com\")) ((\"=?koi8-r?B?98/My8/Oxc7Lz9fBIO/L08HOwQ==?=\" NIL \"torg8\" \"trade-energo.com\")) ((NIL NIL \"pomogite\" \"beeline.ru\")) NIL NIL \"\" \"<!&!AAAAAAAAAAAYAAAAAAAAALlXxT+70slHpEfWLE8vxArCgAAAEAAAAIvgNCrrN6BJqDaFZ9BxPLUBAAAAAA==@trade-energo.com>\") INTERNALDATE \"23-May-2016 14:13:42 +0000\" RFC822.SIZE 642285 FLAGS ())");
	//tests.emplace_back("* 21 FETCH (UID 156 FLAGS (\\Unseen) INTERNALDATE \"05-Apr-2016 14:09:12 +0000\" RFC822.SIZE 68625 ENVELOPE (\"Tue, 05 Apr 2016 17:09:12 +0300\" \"Test image into html\" ((\"Andrey Alekseev\" NIL \"hopen\" \"inbox.ru\")) NIL ((\"=?UTF-8?B?QW5kcmV5IEFsZWtzZWV2?=\" NIL \"hopen\" \"inbox.ru\")) ((\"seemslikebox\" NIL \"seemslikebox\" \"mail.ru\")) NIL NIL NIL \"<1459865352.548439866@f402.i.mail.ru>\"))");

	//tests.emplace_back ("* 169 FETCH (ENVELOPE (\"Wed, 27 Jul 2016 17:00:05 +0300\" {577}[!! MassMail] =?UTF-8?Q??= =?UTF-8?Q?=D0=91=D0=B5=D0=B7=D0=BB=D0=B8=D0=BC=D0=B8=D1=82=D0=BD=D0=B0=D1=8F=20?= =?UTF-8?Q?=D0=AD=D0=BF=D0=B8=D0=BB=D1=8F=D1=86=D0=B8=D1=8F=20/=20?= =?UTF-8?Q?=D0=A0=D0=B5=D1=81=D1=82=D0=BE=D1=80=D0=B0=D0=BD=D1=8B=20?= =?UTF-8?Q?\"=D0=9D=D0=B8=D1=8F=D0=BC=D0=B0\"=20/=20?= =?UTF-8?Q?=D0=A2=D1=80=D0=B5=D0=BD=D0=B8=D0=BD=D0=B3=D0=B8=20?= =?UTF-8?Q?=D0=A1=D0=B5=D0=BA=D1=81=20=D0=A0=D0=A4=20/=20?= =?UTF-8?Q?=D0=9A=D0=BE=D1=80=D1=80=D0=B5=D0=BA=D1=86=D0=B8=D1=8F=20?= =?UTF-8?Q?=D1=84=D0=B8=D0=B3=D1=83=D1=80=D1=8B=20/=20=D0=A2=D0=B0=D0=BD=D1=86=D1=8B?= ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((NIL NIL \"internet\" \"beeline.ru\")) NIL NIL NIL \"<20160727140016.34B4B6021A7@r-2.eg.kingcoupon.ru>\") INTERNALDATE \"27-Jul-2016 14:00:24 +0000\" RFC822.SIZE 166749 FLAGS () UID 8779)");

	//tests.emplace_back("* 10 FETCH (ENVELOPE (\"Mon, 01 Aug 2016 13:46:54 +0300\" {148}\r\nDELIVERY FAILURE: 550 5.7.1 This message was not accepted due to domainowner DMARC policy (RFC 7489) https://help.mail.ru/mail-help/postmaster/dmarc ((\"Internet  Mail Delivery\" NIL \"postmaster\" \"pv33p00im-asmtp002.me.com\")) ((\"Internet Mail Delivery\" NIL \"postmaster\" \"pv33p00im-asmtp002.me.com\")) ((\"Internet Mail Delivery\" NIL \"postmaster\" \"pv33p00im-asmtp002.me.com\")) ((NIL NIL \"otvet\" \"beeline.ru\")) NIL NIL NIL \"<OFC1D4F715.E5A4A2BE-ON43258002.003B39C9@beeline.ru>\") INTERNALDATE \"01-Aug-2016 10:47:10 +0000\" RFC822.SIZE 23103 FLAGS () UID 267847)");
	////tests.emplace_back("* 169 FETCH (ENVELOPE (\"Wed, 27 Jul 2016 17:00:05 +0300\" \"subject\" ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((\"=?UTF-8?Q?=E2=99=95=20King=20Coupon?=\" NIL \"daily\" \"eg.kingcoupon.ru\")) ((NIL NIL \"internet\" \"beeline.ru\")) NIL NIL NIL \"<20160727140016.34B4B6021A7@r-2.eg.kingcoupon.ru>\") INTERNALDATE \"27-Jul-2016 14:00:24 +0000\" RFC822.SIZE 166749 FLAGS () UID 8779)");

	//tests.emplace_back("* 11 FETCH (ENVELOPE (\"Fri, 5 Aug 2016 09:45:40 +0300\" \"=?koi8-r?Q?Re:_=F7=CF=D0=D2=CF=D3_=D7_=D4=C5=C8=D0=CF=C4=C4=C5?= =?koi8-r?Q?=D2=D6=CB=D5_?=\" ((NIL NIL \"biopsy08\" \"inbox.ru\")) ((NIL NIL \"biopsy08\" \"inbox.ru\")) ((NIL NIL \"biopsy08\" \"inbox.ru\")) ((NIL NIL \"otvet\" \"beeline.ru\")) NIL NIL \"<OFC42F7724.22D1EDFC-ON43258006.00205954@beeline.ru>\" \"<CCB85D9E-E0FE-4643-AE59-5533392F5E11@inbox.ru>\") INTERNALDATE \"05-Aug-2016 06:45:43 +0000\" RFC822.SIZE 8800 FLAGS () UID 276584) * 15 EXISTS * 0 RECENT");
	//tests.emplace_back("* 10 FETCH (ENVELOPE (\"Thu, 24 Nov 2016 09:58:13 +0000\" \"reply test\" ((\"Ilya V Tolstoy\" NIL \"ITolstoy\" \"beeline.ru\")) ((\"Ilya V Tolstoy\" NIL \"ITolstoy\" \"beeline.ru\")) ((\"tolstoyilya@gmail.com\" NIL \"tolstoyilya\" \"gmail.com\")) ((\"testimap@beeline.ru\" NIL \"testimap\" \"beeline.ru\")) NIL NIL NIL \"<b7fccfc9f9a3494f80cbf1e60c37ee3b@DR-MBX001.bee.vimpelcom.ru>\") INTERNALDATE \"24-Nov-2016 09:58:14 +0000\" RFC822.SIZE 9522 FLAGS () UID 1288)");

	//tests.emplace_back("* 2 FETCH (FLAGS (\\Recent) INTERNALDATE \"05-Oct-2020 10:35:53 +0300\" RFC822.SIZE 9114 ENVELOPE (\"Mon, 5 Oct 2020 10:35:53 +0300\" \"asdasd\" ((\"=?windows-1251?B?xvPq7uIg0fLg7ejx6+DiIM7r5ePu4uj3?=\" NIL \"SOZhukov\" \"kaluga.beeline.ru\")) NIL NIL ((\"Info\" NIL \"info.beeline\" \"beeline.ru\")) NIL NIL NIL \"<fd17997d7d734168a7e07146c83f60bd@kaluga.beeline.ru>\") UID 858)");

	for each(const std::string& str1 in tests)
	{
		std::string error;

		std::string tmp = CutFuckingSubject(str1);

		bool ok = fetchallparser_parse(tmp,
			[](const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
		},
			error);

		if (ok)
		{
			std::cout << "ok " /*<< str1.c_str()*/ << std::endl;
		}
		else
		{
			std::cout << "fail: " << error.c_str() << std::endl;
		}
	}

	//tests.emplace_back("* 10 FETCH (BODYSTRUCTURE (((\"text\" \"plain\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 78 2 NIL NIL NIL)((\"text\" \"html\"  (\"charset\" \"utf-8\") NIL NIL \"base64\" 546 10 NIL NIL NIL)(\"image\" \"jpeg\"  (\"name\" \"20160804_153216.jpg\") \"__storage_sdcard0__EmailTempImage_1_RotateImage_20160804_153216_jpg@sec.galaxytab\" NIL \"base64\" 124138 NIL (\"inline\" (\"filename\" \"20160804_153216.jpg\")) NIL) \"relative\" (\"boundary\" \"--_com.android.email_1214345537122482\") NIL NIL) \"alternative\" (\"boundary\" \"--_com.android.email_1214345304623741\") NIL NIL)(\"image\" \"jpeg\"  (\"name\" \"20160804_153216_resized.jpg\") NIL NIL \"base64\" 778608 NIL (\"attachment\" (\"filename\" \"20160804_153216_resized.jpg\")) NIL) \"mixed\" (\"boundary\" \"--_com.android.email_1214345288412070\")NIL NIL) UID 275387)");


	for each(const std::string& str1 in tests)
	{
		std::string error;

		int pos = str1.find("UID");
		if (pos > 0)
		{
			std::string errorString = str1.substr(pos, str1.size() - sizeof("UID"));

			bool ok = fetchparser_parse2(
				errorString,
				[](const int& msgNum, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
				{
				}
				,
					error
				);

			if (ok)
			{
				std::cout << "ok " /*<< str1.c_str()*/ << std::endl;
			}
			else
			{
				std::cout << "fail: " << error.c_str() << std::endl;
			}

		}

	}



    return 0;
}

